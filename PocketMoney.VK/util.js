﻿; (function () {
    $.util = {
        token: null,
        ajax: function (action, type, success, data) {
            var date = new Date();
            $.ajax({
                url: "http://localhost:14210/API.svc/json/" + action,
                type: type,
                data: data,
                crossDomain: false,
                dataType: "json",
                contentType: "application/json",
                headers: {
                    'x-auth-token': this.token,
                    'x-date': date.getTime() - date.getTimezoneOffset() * 60 * 1000
                },
                success: success
            });
        },
        getParam: function (name) {
            var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
            if (results == null) {
                return null;
            }
            else {
                return results[1] || 0;
            }
        },
        getCookie: function (name) {
            var matches = document.cookie.match(new RegExp(
              "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
            ));
            return matches ? decodeURIComponent(matches[1]) : undefined;
        },

        setCookie: function (name, value, options) {
            options = options || {};

            var expires = options.expires;

            if (typeof expires == "number" && expires) {
                var d = new Date();
                d.setTime(d.getTime() + expires * 1000);
                expires = options.expires = d;
            }
            if (expires && expires.toUTCString) {
                options.expires = expires.toUTCString();
            }

            value = encodeURIComponent(value);

            var updatedCookie = name + "=" + value;

            for (var propName in options) {
                updatedCookie += "; " + propName;
                var propValue = options[propName];
                if (propValue !== true) {
                    updatedCookie += "=" + propValue;
                }
            }

            document.cookie = updatedCookie;
        },
        deleteCookie: function (name) {
            setCookie(name, "", {
                expires: -1
            });
        }
    };
})();