﻿var initVK = function () {
    $("#register").hide();
    $("#content").show();
    $.util.ajax("GetCurrentUser", "GET",
        function (result) {
            $("#btn-tasks").click();
            getTaskList();
            getGoalsList();
            getAttainmentsList();
        });
};

$(document).ready(function () {
    $.util.token = $.util.getCookie("PocketMoney_VK_Auth");
    if ($.util.token && $.util.token.length > 0) {
        initVK();
    }
    else {
        $.util.ajax("Authorizate", "GET",
            function (result) {
                $.util.setCookie("PocketMoney_VK_Auth", result.d.Data);
                $.util.token = result.d.Data;
                initVK();
            },
            {
                model: JSON.stringify({
                    Identity: $.util.getParam("viewer_id"),
                    ClientType: 1
                })
            });
    }
    $(".btn_day").click(function () {
        $("[data-dateType]").hide();
        $("[data-dateType=" + this.id + "]").show();
    });

    $("#menu > input").click(function () {
        $("#headlines > div").hide();
        $("#" + this.id.split("-")[1]).show();
    });

    $("#goalBack").click(function () {
        $("#tableGoals").show();
        $("#showGoal").hide();
    });

    $("#goalPost").click(postGoal);
});

var getTaskList = function () {

    $.util.ajax("GetTaskList", "GET",
       function (result) {
           $("#tableTasks > tbody").empty();
           for (var i = 0; i < result.d.TotalCount; i++)
               $("#tableTasks > tbody").append("<tr data-dateType=" + result.d.List[i].DateType + "><td>" + result.d.List[i].Text + "</td><td style='text-align: center;'>" + result.d.List[i].Reward + "</td><td><a href='#' class='showTask' data-task-id=" + result.d.List[i].Id + ">Просмотр</a></td></tr>");
           $("#0").click();
       });
};

var showGoal = function (eventObject) {
    var goal = $(eventObject.target);
    var goalId = goal.data("goal-id");
    $("#goalId").val(goalId);
    $("#tableGoals").hide();
    $("#showGoal").show();
    $("#goalNote").val("");
    $("#goalTitle").val(goal.data("goal-text"));
    $("#goalReward").val(goal.data("goal-reward"));
};

var getGoalsList = function () {
    $.util.ajax("GetGoalList", "GET",
         function (result) {
             $("#tableGoals > tbody").empty();
             for (var i = 0; i < result.d.List.length; i++)
                 $("#tableGoals > tbody").append("<tr><td>" + result.d.List[i].Text + "</td><td style='text-align: center;' '>" + result.d.List[i].Reward + "' </td><td><a href='#' class='showGoal' data-goal-id='" + result.d.List[i].Id + "' data-goal-text='" + result.d.List[i].Text + "' data-goal-reward='" + result.d.List[i].Reward + "'>Просмотр</a></td></tr>");
             $(".showGoal").click(showGoal);
         });
};

var postGoal = function () {
    alert(111);
    $.util.ajax("AchieveGoal", "POST",
        function (result) {
            $("goalBack").click();
        },
        JSON.stringify({
            model: {
                Id: $("#goalId").val(),
                Note: $("#goalNote").val()
            }})
        );
};

var getAttainmentsList = function () {
    $.util.ajax("GetGoodDeedList", "GET",
         function (result) {
             $("#attainmentList").empty();
             for (var i = 0; i < result.d.List.length; i++)
                 $("#attainmentList").append("<p data-attainment-id='" + result.d.List[i].Id + "'>" + result.d.List[i].Text + " " + result.d.List[i].Reward +  "</p>");
             $(".showGoal").click(showGoal);
         });
};

