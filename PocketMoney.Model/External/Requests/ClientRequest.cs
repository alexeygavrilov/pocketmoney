﻿using PocketMoney.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace PocketMoney.Model.External.Requests
{
    [DataContract]
    public class AddClientRequest : Request
    {
        [DataMember(IsRequired = true)]
        [DataType(DataType.Text)]
        [Required(ErrorMessage = "User Name is required field")]
        [Display(Name = "User Name")]
        [Details]
        public string UserName { get; set; }

        [DataMember(IsRequired = true)]
        [DataType(DataType.Text)]
        [Required(ErrorMessage = "Идентификатор пользователь обязателен")]
        [Display(Name = "Идентификатор")]
        [Details]
        public string Identity { get; set; }

        [DataMember(IsRequired = true)]
        [Required(ErrorMessage = "Тип подключения пользователя обязателен")]
        [Display(Name = "Тип подключения")]
        [Details]
        public eClientType ClientType { get; set; }

        public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (string.IsNullOrWhiteSpace(this.UserName))
            {
                //yield return new ValidationResult("User Name is required field");
                yield return new ValidationResult("Имя пользователя - обязательное поле");
            }

            if (string.IsNullOrWhiteSpace(this.Identity))
            {
                yield return new ValidationResult("Тип подключения пользователя обязателен");
            }

            if (ClientType != eClientType.VK)
            {
                yield return new ValidationResult("Система поддерживает пока только ВКонтакт.");
            }
        }
    }

    [DataContract]
    public class UpdateClientRequest : AddClientRequest
    {
        [DataMember, Details]
        public Guid UserId { get; set; }

        [DataMember, Details]
        [DataType(DataType.Text)]
        [Display(Name = "Additional User Name")]
        public string AdditionalName { get; set; }
    }
}
