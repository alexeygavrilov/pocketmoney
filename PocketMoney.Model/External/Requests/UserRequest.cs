﻿using Newtonsoft.Json;
using PocketMoney.Data;
using PocketMoney.Model.Internal;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;

namespace PocketMoney.Model.External.Requests
{
    [DataContract]
    public abstract class BaseUserRequest : Request
    {
        [DataMember(IsRequired = true)]
        [DataType(DataType.Text)]
        //[Required(ErrorMessage = "User Name is required field")]
        [Required(ErrorMessage = "Имя - необходимое поле")]
        //[Display(Name = "User Name")]
        [Display(Name = "Имя пользователя")]
        [Details]
        public string UserName { get; set; }

        [DataMember, Details]
        [DataType(DataType.Text)]
        //[Display(Name = "Additional User Name")]
        [Display(Name = "Дополнительная информация")]
        public string AdditionalName { get; set; }

        [DataMember, Details]
        [RegularExpression(@"^[\w-]+(?:\.[\w-]+)*@(?:[\w-]+\.)+[a-zA-Z]{2,7}$", ErrorMessage = "Некорректный Email")]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [DataMember, Details]
        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$",ErrorMessage = "Некоректный телефон")]
        //[Display(Name = "Phone Number")]
        [Display(Name = "Телефонный номер")]
        public string Phone { get; set; }

        public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (string.IsNullOrWhiteSpace(this.UserName))
            {
                //yield return new ValidationResult("User Name is required field");
                yield return new ValidationResult("Имя пользователя - обязательное поле");
            }

            if (!string.IsNullOrEmpty(Email) && !Regex.IsMatch(Email , @"^[\w-]+(?:\.[\w-]+)*@(?:[\w-]+\.)+[a-zA-Z]{2,7}$"))
            {
                //yield return new ValidationResult("Email is incorrect");
                yield return new ValidationResult("Некорректный email");
            }

            if (!string.IsNullOrEmpty(Phone) && !Regex.IsMatch(Phone , @"^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$"))
            {
                //yield return new ValidationResult("Phone is incorrect");
                yield return new ValidationResult("Некорректный телефон");
            }
        }
    }

    [DataContract]
    public class AddUserRequest : BaseUserRequest
    {
        [DataMember(IsRequired = true)]
        //[Required(ErrorMessage = "Password is required field")]
        [Required(ErrorMessage = "Пароль-необходимое поле")]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataMember(IsRequired = true)]
        [DataType(DataType.Password)]
        //[Display(Name = "Confirm Password")]
        [Display(Name = "Подтверждение пароля")]
        //[StringLength(100, ErrorMessage = "{0} should be more than {2} symbols.", MinimumLength = User.MIN_REQUIRED_PASSWORD_LENGTH)]
        [StringLength(100, ErrorMessage = "{0} должен быть длиннее {2} символов.", MinimumLength = User.MIN_REQUIRED_PASSWORD_LENGTH)]
        //[Compare("Password", ErrorMessage = "Passwords are not match. Please try again.")]
        [Compare("Password", ErrorMessage = "Пароли не совпадают.")]
        public string ConfirmPassword { get; set; }

        [DataMember, Details]
        public int RoleId { get; set; }

        [DataMember, Details]
        public bool SendNotification { get; set; }

        public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            foreach (var val in base.Validate(validationContext))
                yield return val;

            if (this.SendNotification && string.IsNullOrWhiteSpace(this.Email))
                //yield return new ValidationResult("Cannot send notification if email is not exist.");
                yield return new ValidationResult("Невозможно отправить извещение, если email не введен");

            if (string.IsNullOrEmpty(this.Password))
                //yield return new ValidationResult("Password cannot be empty");
                yield return new ValidationResult("Пароль не может быть пустым");

            if (this.Password.Length < User.MIN_REQUIRED_PASSWORD_LENGTH)
                yield return new ValidationResult(string.Format(
                    /*"Password should be more than {0} symbols"*/
                    "Пароль должен быть длиннее {0} символов",
                    User.MIN_REQUIRED_PASSWORD_LENGTH));

            if (this.Password != this.ConfirmPassword)
                //yield return new ValidationResult("Passwords are not match. Please try again.");
                yield return new ValidationResult("Пароли не совпадают");

            if (this.RoleId == 0)
                //yield return new ValidationResult("Role is not defined");
                yield return new ValidationResult("Роли не совпадают");
        }
    }

    [DataContract]
    public class UpdateUserRequest : BaseUserRequest
    {
        [DataMember, Details]
        public Guid UserId { get; set; }
    }
}
