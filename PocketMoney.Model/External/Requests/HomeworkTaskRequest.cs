﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using PocketMoney.Data;
using Newtonsoft.Json;

namespace PocketMoney.Model.External.Requests
{
    [DataContract]
    public class AddHomeworkTaskRequest : BaseTaskRequest
    {

        [DataMember]
        [DataType(DataType.Text)]
        [Display(Name = "Lesson")]
        [Details]
        public string Lesson { get; set; }

        [DataMember , Details]
        [JsonConverter(typeof(ConcreteTypeConverter<HomeworkForm>))]
        public HomeworkForm Form { get; set; }

        public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (this.Form == null)
            {
                //yield return new ValidationResult("Invalid form");
                yield return new ValidationResult("Неправильная форма");
            }
            else
            {
                if (this.Form.DateRangeFrom >= this.Form.DateRangeTo)
                    //yield return new ValidationResult("Invalid date range");
                    yield return new ValidationResult("Неправильный промежуток времени");

                if (this.Form.DaysOfWeek == null || this.Form.DaysOfWeek.Length == 0)
                    //yield return new ValidationResult("Days of Week are required value");
                    yield return new ValidationResult("Поле дни недели - обязательно для заполнения");
            }

            foreach (var val in base.Validate(validationContext))
                yield return val;
        }
    }

    [DataContract]
    public class UpdateHomeworkTaskRequest : AddHomeworkTaskRequest , IIdentity
    {
        [DataMember , Details]
        public Guid Id { get; set; }

        public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (this.Id == Guid.Empty)
                //yield return new ValidationResult("Task identifier is required");
                yield return new ValidationResult("Требуется идентификатор задания");

            foreach (var val in base.Validate(validationContext))
                yield return val;

        }
    }

}
