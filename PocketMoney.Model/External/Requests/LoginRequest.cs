﻿using PocketMoney.Data;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace PocketMoney.Model.External.Requests
{

    [DataContract]
    public class AuthorizateRequest : Request
    {
        [DataMember(IsRequired = true)]
        [Required(ErrorMessage = "Тип подключения пользователя обязателен")]
        [Display(Name = "Тип подключения")]
        [Details]
        public eClientType ClientType { get; set; }

        [DataMember(IsRequired = true)]
        [DataType(DataType.Text)]
        [Required(ErrorMessage = "Идентификатор пользователь обязателен")]
        [Display(Name = "Идентификатор")]
        [Details]
        public string Identity { get; set; }

        public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (ClientType != eClientType.VK)
            {
                yield return new ValidationResult("Система поддерживает пока только ВКонтакт.");
            }
            if (string.IsNullOrWhiteSpace(this.Identity))
            {
                yield return new ValidationResult("Идентификатор пользователь обязателен");
            }
        }
    }

    [DataContract]
    public class LoginRequest : Request
    {
        [DataMember(IsRequired = true)]
        [DataType(DataType.Text)]
        [Required(ErrorMessage = "Email/Имя - это обязательное поле")]
        [Display(Name = "Email или Имя пользователя")]
        public string UserName { get; set; }

        [DataMember(IsRequired = true)]
        [Required(ErrorMessage = "Пароль - это обязательное поле")]
        [DataType(DataType.Password)]
        [Display(Name = "Пароль")]
        public string Password { get; set; }



        [Display(Name = "Запомнить меня")]
        public bool RememberMe { get; set; }

        public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (string.IsNullOrWhiteSpace(this.UserName))
                yield return new ValidationResult("Имя - это обязательное поле");

            if (string.IsNullOrWhiteSpace(this.Password))
                yield return new ValidationResult("Пароль - это обязательное поле");
        }
    }
}
