﻿using PocketMoney.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;

namespace PocketMoney.Model.External.Requests
{
    [DataContract]
    public abstract class BaseTaskRequest : RewardRequest
    {
        [DataMember]
        [DataType(DataType.MultilineText)]
        //[Required(ErrorMessage = "Task details is required field")]
        [Display(Name = "Task details")]
        [Details]
        public string Text { get; set; }

        [DataType(DataType.Time)]
        [Display(Name = "Reminder")]
        [Details]
        public TimeSpan? ReminderTime { get; set; }

        [DataMember , Details]
        [Display(Name = "Assigned To")]
        public Guid[] AssignedTo { get; set; }


        public IEnumerable<Guid> GetAssignedTo()
        {
            return (this.AssignedTo ?? new Guid[0]).AsEnumerable();
        }
    }

}
