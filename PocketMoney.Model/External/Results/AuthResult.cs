﻿using System.Runtime.Serialization;
using PocketMoney.Data;

namespace PocketMoney.Model.External.Results
{

    [DataContract]
    public class TokenResult : ResultData<IUser>
    {
        public TokenResult() { }

        public TokenResult(IUser user)
            : base(user)
        {
        }

        [DataMember, Details("******")]
        public string Token { get; set; }

        protected override void ClearData()
        {
            base.ClearData();
            this.Token = null;
        }

    }

    [DataContract]
    public class AuthResult : TokenResult
    {
        public AuthResult() { }

        public AuthResult(IUser user) : base(user) { }

        [DataMember, Details]
        public string Login { get; set; }

        [DataMember, Details("******")]
        public string Password { get; set; }

        protected override void ClearData()
        {
            base.ClearData();
            this.Password = this.Login = null;
        }
    }
}
