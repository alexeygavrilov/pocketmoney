﻿using PocketMoney.Data;
using PocketMoney.Model.Internal;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace PocketMoney.Model.External.Results.Clients
{
    [DataContract]
    public class GoalView : ObjectBase
    {
        public GoalView() { }

        public GoalView(Goal goal)
        {
            this.Id = goal.Id;
            this.Text = goal.Details;
            this.Reward = goal.Reward.ToString();
        }

        protected GoalView(Guid id, string text, Reward reward)
        {
            this.Id = id;
            this.Text = text;
            this.Reward = reward.ToString();
        }

        [DataMember, Details]
        public Guid Id { get; set; }

        [DataMember, Details]
        public string Text { get; set; }

        [DataMember, Details]
        public string Reward { get; set; }
    }

    [DataContract]
    public class TaskView : GoalView
    {
        public TaskView() { }


        public TaskView(string custom,
            Guid taskId,
            TaskType type,
            string text,
            Reward reward,
            eDateType dateType)
            : base(taskId, text, reward)
        {
            switch (type.Id)
            {
                case Internal.TaskType.CLEAN_TYPE:
                    Title = CleanTask.FormatTitle(custom, text);
                    CustomName = "Команата";
                    break;
                case Internal.TaskType.HOMEWORK_TYPE:
                    Title = HomeworkTask.FormatTitle(custom, text);
                    CustomName = "Урок(и)";
                    break;
                case Internal.TaskType.ONE_TIME_TYPE:
                    Title = OneTimeTask.FormatTitle(custom, text);
                    CustomName = "Название";
                    break;
                case Internal.TaskType.REPEAT_TYPE:
                    Title = RepeatTask.FormatTitle(custom, text);
                    CustomName = "Название";
                    break;
                case Internal.TaskType.SHOPPING_TYPE:
                    Title = ShopTask.FormatTitle(custom, text);
                    CustomName = "Магазин";
                    break;

            }
            this.CustomValue = custom ?? string.Empty;
            this.TaskType = type.Id;
            this.DateType = dateType;
        }

        [DataMember, Details]
        public int TaskType { get; set; }

        [DataMember, Details]
        public eDateType DateType { get; set; }

        [DataMember, Details]
        public string Title { get; set; }

        [DataMember, Details]
        public string CustomName { get; set; }
        [DataMember, Details]
        public string CustomValue { get; set; }
    }

    [DataContract]
    public class TaskListResult : ResultList<TaskView>
    {
        public TaskListResult() { }

        public TaskListResult(TaskView[] taskList, int count) : base(taskList, count) { }
    }

    [DataContract]
    public class GoalListResult : ResultList<GoalView>
    {
        public GoalListResult() { }

        public GoalListResult(GoalView[] goalList, int count) : base(goalList, count) { }
    }

    [DataContract]
    public class TaskViewInQuery : ObjectBase
    {
        public Guid Id { get; set; }
        public string Desctiption { get; set; }
        public Reward Reward { get; set; }
        public TaskType TaskType { get; set; }
        public bool HasDates { get; set; }

        // clean
        public string Clean_RoomName { get; set; }
        public eDaysOfWeek Clean_DaysOfWeek { get; set; }

        // shop
        public string Shop_Name { get; set; }
        public DateTime? Shop_DeadlineDate { get; set; }

        // repeat
        public string Repeat_Name { get; set; }

        // single
        public string OneTime_Name { get; set; }
        public DateTime? OneTime_DeadlineDate { get; set; }

        // homework
        public string Homework_LessonName { get; set; }

        public TaskView[] Create(Func<DayOfOne, bool> actionExists, Func<DayOfOne, bool> dateExists, CurrentDates dates)
        {
            var result = new List<TaskView>();

            if (this.TaskType == TaskType.CleanTask)
            {
                if ((this.Clean_DaysOfWeek == eDaysOfWeek.None || this.Clean_DaysOfWeek.HasFlag(dates.YesterdayDate.DayOfWeek.To()))
                    && !actionExists(dates.Yesterday))
                {
                    result.Add(new TaskView(this.Clean_RoomName, this.Id, this.TaskType, this.Desctiption, this.Reward, eDateType.Yesterday));
                }
                if ((this.Clean_DaysOfWeek == eDaysOfWeek.None || this.Clean_DaysOfWeek.HasFlag(dates.TodayDate.DayOfWeek.To()))
                    && !actionExists(dates.Today))
                {
                    result.Add(new TaskView(this.Clean_RoomName, this.Id, this.TaskType, this.Desctiption, this.Reward, eDateType.Today));
                }
                if ((this.Clean_DaysOfWeek == eDaysOfWeek.None || this.Clean_DaysOfWeek.HasFlag(dates.TomorrowDate.DayOfWeek.To()))
                    && !actionExists(dates.Tomorrow))
                {
                    result.Add(new TaskView(this.Clean_RoomName, this.Id, this.TaskType, this.Desctiption, this.Reward, eDateType.Tomorrow));
                }
            }
            else if (this.TaskType == TaskType.HomeworkTask)
            {
                if (dateExists(dates.Yesterday) && !actionExists(dates.Yesterday))
                {
                    result.Add(new TaskView(this.Homework_LessonName, this.Id, this.TaskType, this.Desctiption, this.Reward, eDateType.Yesterday));
                }
                if (dateExists(dates.Today) && !actionExists(dates.Today))
                {
                    result.Add(new TaskView(this.Homework_LessonName, this.Id, this.TaskType, this.Desctiption, this.Reward, eDateType.Today));
                }
                if (dateExists(dates.Tomorrow) && !actionExists(dates.Tomorrow))
                {
                    result.Add(new TaskView(this.Homework_LessonName, this.Id, this.TaskType, this.Desctiption, this.Reward, eDateType.Tomorrow));
                }
            }
            else if (this.TaskType == TaskType.OneTimeTask)
            {
                if ((!this.OneTime_DeadlineDate.HasValue || this.OneTime_DeadlineDate.Value >= dates.YesterdayDate) && !actionExists(dates.Yesterday))
                {
                    result.Add(new TaskView(this.OneTime_Name, this.Id, this.TaskType, this.Desctiption, this.Reward, eDateType.Yesterday));
                }
                if ((!this.OneTime_DeadlineDate.HasValue || this.OneTime_DeadlineDate.Value >= dates.TodayDate) && !actionExists(dates.Today))
                {
                    result.Add(new TaskView(this.OneTime_Name, this.Id, this.TaskType, this.Desctiption, this.Reward, eDateType.Today));
                }
                if ((!this.OneTime_DeadlineDate.HasValue || this.OneTime_DeadlineDate.Value >= dates.TomorrowDate) && !actionExists(dates.Tomorrow))
                {
                    result.Add(new TaskView(this.OneTime_Name, this.Id, this.TaskType, this.Desctiption, this.Reward, eDateType.Tomorrow));
                }
            }
            else if (this.TaskType == TaskType.RepeatTask)
            {
                if (dateExists(dates.Yesterday) && !actionExists(dates.Yesterday))
                {
                    result.Add(new TaskView(this.Repeat_Name, this.Id, this.TaskType, this.Desctiption, this.Reward, eDateType.Yesterday));
                }
                if (dateExists(dates.Today) && !actionExists(dates.Today))
                {
                    result.Add(new TaskView(this.Repeat_Name, this.Id, this.TaskType, this.Desctiption, this.Reward, eDateType.Today));
                }
                if (dateExists(dates.Tomorrow) && !actionExists(dates.Tomorrow))
                {
                    result.Add(new TaskView(this.Repeat_Name, this.Id, this.TaskType, this.Desctiption, this.Reward, eDateType.Tomorrow));
                }
            }
            else if (this.TaskType == TaskType.ShoppingTask)
            {
                if ((!this.Shop_DeadlineDate.HasValue || this.Shop_DeadlineDate.Value >= dates.YesterdayDate) && !actionExists(dates.Yesterday))
                {
                    result.Add(new TaskView(this.Shop_Name, this.Id, this.TaskType, this.Desctiption, this.Reward, eDateType.Yesterday));
                }
                if ((!this.Shop_DeadlineDate.HasValue || this.Shop_DeadlineDate.Value >= dates.TodayDate) && !actionExists(dates.Today))
                {
                    result.Add(new TaskView(this.Shop_Name, this.Id, this.TaskType, this.Desctiption, this.Reward, eDateType.Today));
                }
                if ((!this.Shop_DeadlineDate.HasValue || this.Shop_DeadlineDate.Value >= dates.TomorrowDate) && !actionExists(dates.Tomorrow))
                {
                    result.Add(new TaskView(this.Shop_Name, this.Id, this.TaskType, this.Desctiption, this.Reward, eDateType.Tomorrow));
                }
            }
            return result.ToArray();
        }
    }
}
