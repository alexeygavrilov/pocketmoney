﻿using PocketMoney.Data;
using PocketMoney.Model.Internal;
using System;
using System.Runtime.Serialization;

namespace PocketMoney.Model.External.Results
{
    [DataContract]
    public class TaskStatusResult : ResultList<StatusView>
    {
        public TaskStatusResult() { }

        public TaskStatusResult(StatusView[] statusList, int count) : base(statusList, count) { }

    }

    [DataContract]
    public class StatusView : ObjectBase
    {
        public StatusView() { }


        [DataMember, Details]
        public string User { get; set; }

        [DataMember, Details]
        public string Date
        {
            get
            {
                return DateOfOne > 0
                    ? new DayOfOne(DateOfOne).ToString()
                    : DateOfAction.HasValue
                        ? DateOfAction.Value.ToString("dd.MM.yyyy")
                        : string.Empty;
            }
        }

        [IgnoreDataMember]
        public DateTime? DateOfAction { get; set; }

        [IgnoreDataMember]
        public int DateOfOne { get; set; }

        [DataMember, Details]
        public string Comment { get; set; }

        [DataMember, Details]
        public Guid TaskId { get; set; }

        [DataMember, Details]
        public Guid TaskActionId { get; set; }
    }
}
