﻿using PocketMoney.Data;
using PocketMoney.Model.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace PocketMoney.Model.External.Results
{
    [DataContract]
    public class UserInfoView : ObjectBase, IEqualityComparer<UserInfoView>
    {
        public UserInfoView(Guid userId, string userName)
        {
            UserId = userId;
            UserName = userName;
        }

        [DataMember, Details]
        public Guid UserId { get; set; }

        [DataMember, Details]
        public string UserName { get; set; }

        public bool Equals(UserInfoView x, UserInfoView y)
        {
            return x.UserId == y.UserId;
        }

        public int GetHashCode(UserInfoView obj)
        {
            return obj.UserId.GetHashCode();
        }
    }

    [DataContract]
    public class UserAssignedView : UserInfoView, IEqualityComparer<UserAssignedView>
    {
        public UserAssignedView(Guid userId, string userName, eTaskStatus status)
            : base(userId, userName)
        {
            Status = status;
        }

        [DataMember, Details]
        public eTaskStatus Status { get; set; }


        public bool Equals(UserAssignedView x, UserAssignedView y)
        {
            return base.Equals(x, y);
        }

        public int GetHashCode(UserAssignedView obj)
        {
            return base.GetHashCode(obj);
        }
    }

    [DataContract]
    public class UserView : UserInfoView
    {
        public UserView(User user)
            : base(user.Id, user.UserName)
        {
            this.Points = user.Points.Points;
            this.CompletedTaskCount = user.Counts.CompletedTasks;
            this.GoalsCount = user.Counts.CompletedGoals;
            this.GoodDeedCount = user.Counts.GoodWorks;
            this.GrabbedTaskCount = user.Counts.GrabbedTasks;
            this.RoleName = string.Join(", ", user.Roles.Select(r => r.Name).ToArray());
        }

        [DataMember, Details]
        public string RoleName { get; set; }

        [DataMember, Details]
        public int Points { get; set; }

        [DataMember, Details]
        public int CompletedTaskCount { get; set; }

        [DataMember, Details]
        public int GrabbedTaskCount { get; set; }

        [DataMember, Details]
        public int GoalsCount { get; set; }

        [DataMember, Details]
        public int GoodDeedCount { get; set; }

        public override string ToString()
        {
            return this.UserName;
        }
    }

    [DataContract]
    public class UserFullView : UserView
    {
        public UserFullView(User user, Func<string> historyLog)
            : base(user)
        {
            this.AdditionalName = user.AdditionalName;
            this.Email = user.Email != null ? user.Email.Address : string.Empty;
            this.Phone = user.Phone != null ? user.Phone.Number : string.Empty;
            this.LastLoginDate = user.LastLoginDate.HasValue ? user.LastLoginDate.Value.ToString() : string.Empty;
            this.HistoryLog = historyLog();
            this.Connections = user.Connections.Select(x => new KeyValuePair
            {
                Key = ((int)x.ClientType).ToString(),
                Value = x.Identity
            }).ToArray();
        }


        [DataMember, Details]
        public KeyValuePair[] Connections { get; set; }


        [DataMember, Details]
        public string AdditionalName { get; set; }

        [DataMember, Details]
        public string Email { get; set; }

        [DataMember, Details]
        public string Phone { get; set; }

        [DataMember, Details]
        public string LastLoginDate { get; set; }

        [DataMember, Details]
        public string HistoryLog { get; set; }

        public override string ToString()
        {
            return this.UserName;
        }
    }
}
