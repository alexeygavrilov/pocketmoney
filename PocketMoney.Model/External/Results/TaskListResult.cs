﻿using PocketMoney.Data;
using PocketMoney.Model.Internal;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace PocketMoney.Model.External.Results
{
    [DataContract]
    public class TaskListResult : ResultList<TaskView>
    {
        public TaskListResult() { }

        public TaskListResult(TaskView[] taskList, int count) : base(taskList, count) { }
    }

    [DataContract]
    public abstract class TaskView : RewardView
    {
        protected TaskView(Guid taskId, TaskType type, string text, Reward reward, int? reminderTime, IEnumerable<UserAssignedView> assignedTo, bool hasDates, bool hasActions)
            : base(reward)
        {
            this.TaskId = taskId;
            this.TaskType = type.Id;
            this.HasDates = hasDates;
            this.HasActions = hasActions;
            this.Text = text;
            this.ReminderTime = reminderTime.HasValue ? new TimeSpan?(TimeSpan.FromMinutes(reminderTime.Value)) : null;
            this.AssignedTo = assignedTo.ToArray();
        }

        protected TaskView(Task task)
            : base(task.Reward)
        {
            this.HasDates = task.HasDates;
            this.TaskId = task.Id;
            this.TaskType = task.Type.Id;
            this.Text = task.Details;
            this.ReminderTime = task.Reminder.HasValue ? new TimeSpan?(TimeSpan.FromMinutes(task.Reminder.Value)) : null;
            this.AssignedTo = task.AssignedTo.Select(x => new UserAssignedView(x.User.Id, x.User.FullName(), x.Status)).ToArray();
            this.HasActions = task.AssignedTo.Any(x => x.Actions.Any());
        }

        [DataMember, Details]
        public Guid TaskId { get; set; }

        [DataMember, Details]
        public int TaskType { get; set; }

        [DataMember, Details]
        public string Title { get { return this.GetTitle(); } }

        [DataMember, Details]
        public string Text { get; set; }

        [DataMember, Details]
        public UserAssignedView[] AssignedTo { get; set; }

        [DataMember, Details]
        public TimeSpan? ReminderTime { get; set; }

        [DataMember, Details]
        public bool HasDates { get; set; }

        [DataMember, Details]
        public bool HasActions { get; set; }

        [DataMember, Details]
        public bool HasStatus
        {
            get
            {
                if (AssignedTo.Any(x => x.UserId != Guid.Empty))
                {
                    if (HasDates)
                    {
                        return HasActions;
                    }
                    else
                    {
                        return AssignedTo.Any(x => x.Status != eTaskStatus.New);
                    }
                }
                return false;
            }
        }

        [DataMember, Details]
        public string StatusName
        {
            get
            {
                if (AssignedTo.Any(x => x.UserId != Guid.Empty))
                {
                    if (HasDates)
                    {
                        if (HasActions)
                        {
                            return "Выполняется";
                        }
                        else
                        {
                            return "Ждет исполненния";
                        }
                    }
                    else
                    {
                        if (AssignedTo.All(x => x.Status == eTaskStatus.Closed))
                        {
                            return "Завершен";
                        }
                        if (AssignedTo.Any(x => x.Status == eTaskStatus.Closed))
                        {
                            return string.Join(" и ", this.AssignedTo.Where(x => x.Status == eTaskStatus.Closed).Select(x => x.UserName).ToArray()) + " завершил";
                        }
                        if (AssignedTo.Any(x => x.Status == eTaskStatus.Reopen))
                        {
                            return "Переоткрыт";
                        }
                        return "Ждет исполненния";
                    }
                }
                else
                {
                    return "Ждет исполнителя";
                }
            }
        }

        public string Responsibility
        {
            get
            {
                return string.Join(", ", this.AssignedTo.Select(x => x.UserName).ToArray());
            }
        }

        public abstract string GetTitle();


    }
}
