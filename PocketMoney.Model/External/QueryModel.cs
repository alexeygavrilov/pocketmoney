﻿using PocketMoney.Data;
using PocketMoney.Model.External.Results;
using PocketMoney.Model.Internal;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PocketMoney.Model.External
{
    public class UserViewInQuery : ObjectBase
    {
        public Guid UserId { get; set; }
        public string UserName { get; set; }
        public string AdditionalName { get; set; }
        public eTaskStatus Status { get; set; }
        public bool HasActions { get; set; }
    }

    public class GoalViewInQuery : UserViewInQuery
    {
        public Guid Id { get; set; }
        public string Details { get; set; }
        public Reward Reward { get; set; }

        public GoalView CreateGoal(IList<UserViewInQuery> assignedTo)
        {
            return new GoalView(this.Id, this.Details, this.Reward,
                assignedTo.ToDictionary(k => k.UserId, u => User.FullName(u.UserName, u.AdditionalName), EqualityComparer<Guid>.Default));
        }
    }

    public class TaskViewInQuery : GoalViewInQuery
    {
        public TaskType TaskType { get; set; }
        public int? Reminder { get; set; }
        public string RepeatName { get; set; }
        public string OneTimeName { get; set; }
        public string RoomName { get; set; }
        public string ShopName { get; set; }
        public string LessonName { get; set; }
        public bool HasDates { get; set; }

        public TaskView CreateTask(IList<UserViewInQuery> assignedTo)
        {
            var user = assignedTo
                .GroupBy(x => x.UserId)
                .Select(u => new UserAssignedView(u.First().UserId, User.FullName(u.First().UserName, u.First().AdditionalName), u.First().Status));

            var hasActions = assignedTo.Any(x => x.HasActions);

            if (this.TaskType == TaskType.CleanTask)
            {
                return new CleanTaskView(this.RoomName, false, null, this.Id, this.TaskType, this.Details, this.Reward, this.Reminder, user, this.HasDates, hasActions);
            }
            else if (this.TaskType == TaskType.HomeworkTask)
            {
                return new HomeworkTaskView(this.LessonName, null, this.Id, this.TaskType, this.Details, this.Reward, this.Reminder, user, this.HasDates, hasActions);
            }
            else if (this.TaskType == TaskType.OneTimeTask)
            {
                return new OneTimeTaskView(this.OneTimeName, null, this.Id, this.TaskType, this.Details, this.Reward, this.Reminder, user, this.HasDates, hasActions);
            }
            else if (this.TaskType == TaskType.RepeatTask)
            {
                return new RepeatTaskView(this.RepeatName, null, this.Id, this.TaskType, this.Details, this.Reward, this.Reminder, user, this.HasDates, hasActions);
            }
            else if (this.TaskType == TaskType.ShoppingTask)
            {
                return new ShoppingTaskView(this.ShopName, null, null, this.Id, this.TaskType, this.Details, this.Reward, this.Reminder, user, this.HasDates, hasActions);
            }
            else
            {
                return null;
            }
        }
    }

}
