﻿using PocketMoney.Data;

namespace PocketMoney.Model.Internal
{
    public static class Roles
    {
        public static Role Empty = new Role();
        public static Role Children = new Role(0x1, "Ребенок");
        public static Role Parent = new Role(0x2, "Родитель");
        public static Role FamilyAdmin = new Role(0x4, "Глава семьи");
        public static Role Admin = new Role(0x8, "Администратор");
        public static Role Define(int roleId)
        {
            switch (roleId)
            {
                case 1:
                    return Children;
                case 2:
                    return Parent;
                case 4:
                    return FamilyAdmin;
                case 8:
                    return Admin;
                default:
                    return Empty;
            }
        }
        public static Role Define(string roleName)
        {
            switch (roleName)
            {
                case "Ребенок":
                    return Children;
                case "Родитель":
                    return Parent;
                case "Глава семьи":
                    return FamilyAdmin;
                case "Администратор":
                    return Admin;
                default:
                    return Empty;
            }
        }
    }
}
