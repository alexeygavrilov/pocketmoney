﻿using PocketMoney.Data;
using System;

namespace PocketMoney.Model.Internal
{
    public class OneTimeTask : Task
    {
        protected OneTimeTask() : base() { }

        public OneTimeTask(string name, string details, Reward reward, DateTime? deadlineDate, User creator)
            : base(TaskType.OneTimeTask, details, reward, creator)
        {
            this.OneTimeName = name;
            this.DeadlineDate = deadlineDate;
        }

        [Details]
        public virtual string OneTimeName { get; set; }

        [Details]
        public virtual DateTime? DeadlineDate { get; set; }

        [Details]
        public override string Name
        {
            get { return FormatTitle(this.OneTimeName, this.Details); }
        }

        public static string FormatTitle(string name, string text)
        {
            return Task.FormatTitle(name, text);
            //return string.Format(TITLE_FORMAT, name);
        }

    }
}
