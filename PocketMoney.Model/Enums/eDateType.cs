﻿
namespace PocketMoney.Model
{
    public enum eDateType
    {
        Yesterday = 0,
        Today = 1,
        Tomorrow = 2
    }
}
