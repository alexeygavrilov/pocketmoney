﻿namespace PocketMoney.Model
{
    public enum eClientType : int
    {
        FB = 0,
        VK = 1,
        Phone = 2
    }
}
