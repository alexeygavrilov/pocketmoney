﻿using System;
using Newtonsoft.Json;
using PocketMoney.Data;
using PocketMoney.FileSystem;
using PocketMoney.Model;
using PocketMoney.Util.ExtensionMethods;
using Newtonsoft.Json.Linq;
using System.Globalization;

namespace PocketMoney.Model
{
    public class ConcreteTypeConverter<TConcrete> : JsonConverter
        where TConcrete : class, new()
    {
        protected virtual TConcrete Create(Type objectType , JObject jObject)
        {
            return new TConcrete();
        }
        public override bool CanConvert(Type objectType)
        {
            return typeof(TConcrete).IsAssignableFrom(objectType);
        }

        public override bool CanRead
        {
            get
            {
                return true;
            }
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            // Load JObject from stream
            JObject jObject = JObject.Load(reader);

           // Create target object based on JObject
            TConcrete target = Create(objectType , jObject);

            // Populate the object properties
            serializer.Culture = CultureInfo.CurrentCulture;
            var jReader = jObject.CreateReader();
            jReader.Culture = CultureInfo.CurrentCulture;
            serializer.Populate(jReader, target);

            return target;
            //return serializer.Deserialize<TConcrete>(reader);
        }

        private dynamic GetObject(object value)
        {
            if (value is IFile)
                return ((IFile)value).From();
            else if (value is IUser)
                return ((IUser)value).From();
            else if (value is IFamily)
                return ((IFamily)value).From();

            try
            {
                return (TConcrete)Convert.ChangeType(value, typeof(TConcrete));
            }
            catch (Exception ex)
            {
                ex.LogError();
                return null;
            }
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            serializer.Culture = CultureInfo.CurrentCulture;
            
            if (value.GetType().IsArray)
            {
                object[] array = (object[])value;
                dynamic[] newarr = new dynamic[array.Length];
                for (int i = 0; i < array.Length; i++)
                {
                    newarr[i] = this.GetObject(array[i]);
                }
                serializer.Serialize(writer, newarr);
            }
            else
                serializer.Serialize(writer, this.GetObject(value));
        }
    }
}
