﻿var PocketMoneyApp = {
    elements: {
        tableTasks: function () { return $("#tableTasks"); },
        tableGoals: function () { return $("#tableGoals"); },
        shoppingList: function () { return $("#shoppingList"); },
        showGoal: function () { return $("#showGoal"); },
        showTask: function () { return $("#showTask"); }
    },
    getTaskList: function () {
        var _this = PocketMoneyApp;
        $.util.ajax("GetTaskList", "GET",
           function (result) {
               var tbody = _this.elements.tableTasks();
               $("#task-title").text("Задачи на сегодня");
               tbody.empty();
               tbody.append("<div class='list-item empty-item' style='display:none'>Нет задач</div>");
               for (var i = 0; i < result.d.TotalCount; i++) {
                   tbody.append("<div class='list-item' data-date-type='" + result.d.List[i].DateType + "'" + (result.d.List[i].DateType != 1 ? " style='display:none'" : "") + " data-task-id='" + result.d.List[i].Id + "' data-task-title='" + result.d.List[i].CustomName + ": " + result.d.List[i].CustomValue + "' data-task-text='" + result.d.List[i].Text + "' data-task-reward='" + result.d.List[i].Reward + "' data-task-type='" + result.d.List[i].TaskType + "'><span>" + result.d.List[i].Title + "</span><div class='list-item-reward'>" + result.d.List[i].Reward + "</div></div>");
               }
               $.util.ajax("GetFloatingTaskList", "GET",
                    function (result) {
                        for (var i = 0; i < result.d.TotalCount; i++) {
                            tbody.append("<div class='list-item' data-date-type='10' style='display:none' data-task-id='" + result.d.List[i].Id + "' data-task-title='" + result.d.List[i].CustomName + ": " + result.d.List[i].CustomValue + "' data-task-text='" + result.d.List[i].Text + "' data-task-reward='" + result.d.List[i].Reward + "' data-task-type='" + result.d.List[i].TaskType + "'><span>" + result.d.List[i].Title + "</span><div class='list-item-reward'>" + result.d.List[i].Reward + "</div></div>");
                        }
                        $(".list-item[data-date-type]").click(_this.showTask)
                        _this.showTaskList();
                    });
           });
    },
    showTask: function (eventObject) {
        var _this = PocketMoneyApp;
        var task = $(eventObject.target).closest(".list-item");
        var taskId = task.data("task-id");
        var dateType = task.data("date-type");

        $("#taskTitle").text(task.data("task-title"));
        $("#taskText").text("Описание: " + task.data("task-text"));
        $("#taskReward").text("Награда: " + task.data("task-reward"));
        $("#taskNote").val("");
        if (dateType == 10) {
            $("#taskPost").hide();
            $("#taskGrab").show();
            $("#taskGrab").data("task-id", taskId);
        }
        else {
            $("#taskGrab").hide();
            $("#taskPost").data("task-id", taskId);
            $("#taskPost").show();
            $("#taskPost").data("date-type", dateType);
        }

        _this.elements.tableTasks().hide();
        _this.elements.showTask().show();
        _this.elements.shoppingList().empty();

        if (task.data("task-type") == "5") {
            $.util.ajax("GetShoppingList", "GET",
                function (result) {
                    var shoppingList = _this.elements.shoppingList();
                    shoppingList.empty();
                    for (var i = 0; i < result.d.List.length; i++) {
                        var checked = (result.d.List[i].Processed === true) ? " checked" : "";
                        var disabled = dateType == 10 ? " disabled='disabled'" : "";
                        shoppingList.append("<input type='checkbox' id='checkbox" + i + "' " + checked + disabled + " data-order-number='" + result.d.List[i].OrderNumber + "'/><label for='checkbox" + i + "'>" + result.d.List[i].ItemName + "</label><br />");
                    }
                    if (dateType != 10) {
                        _this.elements.shoppingList().find("input[type=checkbox]").change(function (eventObject) {
                            $.util.ajax("CheckShopItem", "POST",
                                function (result) {
                                },
                                JSON.stringify({
                                    model:
                                        {
                                            TaskId: taskId,
                                            OrderNumber: $(eventObject.target).data("order-number"),
                                            Checked: $(eventObject.target).prop("checked")
                                        }
                                })
                            );
                        });
                    }
                },
                { taskId: JSON.stringify({ Data: taskId }) });
        }
    },
    postTask: function (eventObject) {
        var _this = PocketMoneyApp;
        $.util.ajax("DoneTask", "POST",
            function (result) {
                _this.getTaskList();
                _this.elements.tableTasks().show();
                _this.elements.showTask().hide();
                _this.getPoints();
            },
            JSON.stringify({
                model: {
                    Id: $(eventObject.target).data("task-id"),
                    Note: $("#taskNote").val(),
                    DateType: $(eventObject.target).data("date-type")
                }
            })
        )
    },
    grabTask: function (eventObject) {
        var _this = PocketMoneyApp;
        $.util.ajax("GrabbTask", "POST",
            function (result) {
                _this.getTaskList();
                _this.elements.tableTasks().show();
                _this.elements.showTask().hide();
                _this.getPoints();
            },
            JSON.stringify({
                model: {
                    Id: $(eventObject.target).data("task-id"),
                    Note: $("#taskNote").val()
                }
            })
        )
    },
    getGoalsList: function () {
        var _this = PocketMoneyApp;
        $.util.ajax("GetGoalList", "GET", function (result) {
            var tbody = _this.elements.tableGoals();
            tbody.empty();
            tbody.append("<div class='list-item empty-item' style='display:none'>Нет достижений</div>");
            for (var i = 0; i < result.d.TotalCount; i++) {
                tbody.append("<div class='list-item' data-goal-id='" + result.d.List[i].Id + "' data-goal-text='" + result.d.List[i].Text + "' data-goal-reward='" + result.d.List[i].Reward + "'><span>" + result.d.List[i].Text + "</span><div class='list-item-reward'>" + result.d.List[i].Reward + "</div></div>");
            }
            $(".list-item[data-goal-id]").click(_this.showGoal);
            _this.showEmpty(tbody);
        });
    },
    showGoal: function (eventObject) {
        var goal = $(eventObject.target).closest(".list-item");

        $("#goalPost").data("goal-id", goal.data("goal-id"));
        $("#goalNote").val("");
        $("#goalTitle").text(goal.data("goal-text"));
        $("#goalReward").text("Награда: " + goal.data("goal-reward"));

        PocketMoneyApp.elements.tableGoals().hide();
        PocketMoneyApp.elements.showGoal().show();
    },
    postGoal: function (eventObject) {
        var _this = PocketMoneyApp;
        $.util.ajax("AchieveGoal", "POST",
            function (result) {
                _this.getGoalsList();
                _this.elements.tableGoals().show();
                _this.elements.showGoal().hide();
                _this.getPoints();
            },
            JSON.stringify({
                model: {
                    Id: $(eventObject.target).data("goal-id"),
                    Note: $("#goalNote").val(),
                }
            })
            );
    },
    getAttainmentsList: function () {
        var _this = PocketMoneyApp;
        $.util.ajax("GetGoodDeedList", "GET",
             function (result) {
                 var tbody = $("#tableAttainments");
                 tbody.empty();
                 tbody.append("<div class='list-item empty-item' style='display:none'>Нет поступков</div>");
                 for (var i = 0; i < result.d.TotalCount; i++) {
                     tbody.append("<div class='list-item' style='cursor:default'><span>" + result.d.List[i].Text + "</span><div class='list-item-reward'>" + result.d.List[i].Reward + "</div></div>");
                 }
                 _this.showEmpty(tbody);
             });
    },
    postAttainment: function () {
        var _this = PocketMoneyApp;
        $.util.ajax("AddGoodDeed", "POST",
            function (result) {
                _this.getAttainmentsList();
                $("#tableAttainments").show();
                $("#attainmentArea").hide();
            },
            JSON.stringify({
                model: {
                    Text: $("#attainmentDesciption").val()
                }
            }));
    },
    getPoints: function () {
        $.util.ajax("GetCurrentUser", "GET",
             function (result) {
                 $("#user-points").text(result.d.Data.Points + " баллов");
             }
        );
    },
    showday: function (e) {
        var btn = $(e.target).closest(".btn_day");
        $("#task-title").text(btn.text());
        var tbody = PocketMoneyApp.elements.tableTasks();
        tbody.find(".list-item[data-date-type]").hide();
        tbody.find(".list-item[data-date-type=" + btn.data("id") + "]").show();
        PocketMoneyApp.showEmpty(tbody);
        btn.closest(".submenu").slideUp(100);
    },
    showEmpty: function (table) {
        if (table.find('.list-item:visible:not(.empty-item)').length > 0) {
            table.find(".empty-item").hide();
        }
        else {
            table.find(".empty-item").show();
        }
    },
    hideGoal: function () {
        PocketMoneyApp.elements.showGoal().hide();
        PocketMoneyApp.elements.tableGoals().show();
    },
    addAttainment: function (e) {
        $("#headlines > div").hide();
        $("#attainments").show();
        $("#tableAttainments").hide();
        $("#attainmentArea").show();
        $("#attainmentDesciption").val('');
        $(e.target).closest(".submenu").slideUp(100);
    },
    hideTask: function () {
        PocketMoneyApp.elements.showTask().hide();
        PocketMoneyApp.elements.tableTasks().show();
    },
    hideAttainment: function () {
        $("#attainmentArea").hide();
        $("#tableAttainments").show();
    },
    showTaskList: function () {
        $("#headlines > div").hide();
        $("#tasks").show();
        PocketMoneyApp.hideTask();
        PocketMoneyApp.showEmpty(PocketMoneyApp.elements.tableTasks());
    },
    showGoalList: function () {
        $("#headlines > div").hide();
        $("#goals").show();
        PocketMoneyApp.hideGoal();
        PocketMoneyApp.showEmpty(PocketMoneyApp.elements.tableGoals());
    },
    showAttainmentList: function () {
        $("#headlines > div").hide();
        $("#attainments").show();
        PocketMoneyApp.hideAttainment();
        PocketMoneyApp.showEmpty($("#tableAttainments"));

    },
    showSubmenu: function (e) {
        $(e.target).children(".submenu").slideDown(100);
    },
    load: function () {
        $("#register").hide();
        $("#content").show();
        $.util.ajax("GetCurrentUser", "GET",
            function (result) {
                $("#user-points").text(result.d.Data.Points + " баллов");
                PocketMoneyApp.getTaskList();
                PocketMoneyApp.getGoalsList();
                PocketMoneyApp.getAttainmentsList();
                $.util.hideLoad();
            });
    },
    noAccess: function () {
        $("#register").show();
        $("#content").hide();
        $.util.hideLoad();
    },
    init: function () {
        $(".btn_day").bind("click", this.showday);

        $("#btn-tasks").bind("click", this.showTaskList);

        $("#btn-goals").bind("click", this.showGoalList);

        $("#btn-attainments > a").bind("click", this.showAttainmentList);

        $("#goalBack").bind("click", this.hideGoal);

        $("#addAttainment").bind("click", this.addAttainment);

        $("#goalPost").bind("click", this.postGoal);

        $("#taskPost").bind("click", this.postTask);

        $("#taskGrab").bind("click", this.grabTask);

        $("#taskBack").bind("click", this.hideTask);

        $("#postAttainment").bind("click", this.postAttainment);

        $("#hideAttainment").bind("click", this.hideAttainment);

        $(".topmenu").bind("mouseover", this.showSubmenu);
    }
};

$(document).ready(function () {
    $.util.showLoad();
    PocketMoneyApp.init();
    $.util.token = $.util.getCookie("PocketMoney_VK_Auth");
    if ($.util.token && $.util.token.length > 0) {
        PocketMoneyApp.load();
    }
    else {
        var viewerId = $.util.getParam("viewer_id");
        if (viewerId && viewerId.length > 0) {
            var link = $("#registrationLink").attr("href");
            link = link + "?client_type=VK&identity=" + $.util.getParam("viewer_id")
            $("#registrationLink").attr("href", link);

            $.get($.util.url + "Authorizate?_=" + new Date().getTime(),
                {
                    model: JSON.stringify({
                        Identity: viewerId,
                        ClientType: 1
                    })
                },
                function (result) {
                    if (result.d.Success === true) {
                        $.util.setCookie("PocketMoney_VK_Auth", result.d.Data);
                        $.util.token = result.d.Data;
                        PocketMoneyApp.load();
                    }
                    else {
                        PocketMoneyApp.noAccess();
                    }
                });
        }
        else {
            PocketMoneyApp.noAccess();
        }
    }

});

