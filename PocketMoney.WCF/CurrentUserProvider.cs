﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using PocketMoney.Data;
using PocketMoney.Data.Wrappers;
using PocketMoney.Util.ExtensionMethods;
using System;
using System.Globalization;
using System.Web;

namespace PocketMoney.WCF
{
    public class CurrentUserProvider : ICurrentUserProvider
    {
        public void AddCurrentUser(IUser user, bool persist = false)
        {
            throw new NotImplementedException();
        }

        public IUser GetCurrentUser()
        {
            var token = HttpContext.Current.Request.Headers.Get("x-auth-token");
            if (!string.IsNullOrEmpty(token))
            {
                string[] ids = token.Split(new char[1] { '-' });

                return new WrapperUser("Wrapper User",
                    ids[0].FromBase32Url(),
                    ids[1].FromBase32Url());
            }
            return null;
        }

        public void RemoveCurrentUser()
        {
            throw new NotImplementedException();
        }

        public DateTime GetCurrentDate()
        {
            DateTime result = DateTime.Now;
            var dateString = HttpContext.Current.Request.Headers.Get("x-date");
            if (!string.IsNullOrEmpty(dateString))
            {
                long msSinceEpoch;
                if (long.TryParse(dateString, out msSinceEpoch))
                {
                    result = new DateTime(1970, 1, 1).AddTicks(msSinceEpoch * 10000);
                }
            }
            return result;
        }

        public void SetData(string key, object value)
        {
            throw new NotImplementedException();
        }

        public object GetDate(string key)
        {
            throw new NotImplementedException();
        }
    }

    public class SiteInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(
                Component.For<ICurrentUserProvider>()
                    .ImplementedBy<CurrentUserProvider>()
                    .LifeStyle
                    .PerWebRequest);
        }
    }
}