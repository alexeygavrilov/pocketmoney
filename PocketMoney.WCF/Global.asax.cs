﻿using Castle.MicroKernel.Lifestyle;
using PocketMoney.Configuration.Web;
using PocketMoney.Util.ExtensionMethods;
using System;
using System.Linq;

namespace PocketMoney.WCF
{
    public class Global : System.Web.HttpApplication
    {
        public Global()
        {
            //new PerWebRequestLifestyleModule().Init(this);
        }

        protected void Application_Start(object sender, EventArgs e)
        {
            ContainerRegistration.Register(BuildManager.Instance);

        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

            ;
            //if (Request.Headers.AllKeys.Contains("Origin") && Request.HttpMethod == "OPTIONS")
            //{
            //    Response.Clear();
            //    Response.End();
            //    //Response.Flush();
            //}
        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {
            Exception exception = this.Server.GetLastError();
            if (exception != null)
                exception.LogFatal();
        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}