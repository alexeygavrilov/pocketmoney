﻿; (function () {
    $.util = {
        token: null,
        url: "API.svc/json/",
        showLoad: function () {
            $("#headlines").hide();
            $(".cssload-coffee").show();
        },
        hideLoad: function () {
            $(".cssload-coffee").hide();
            $("#headlines").show();
        },
        ajax: function (action, type, success, data) {
            this.showLoad();
            var _this = this;
            var date = new Date();
            $.ajax({
                url: this.url + action + "?_=" + date.getTime(),
                type: type,
                data: data,
                crossDomain: false,
                dataType: "json",
                contentType: "application/json",
                headers: {
                    'x-auth-token': this.token,
                    'x-date': date.getTime() - date.getTimezoneOffset() * 60 * 1000
                },
                success: function (result) {
                    if (result && result.d.Success === true) {
                        success(result);
                    }
                    else if (result && result.d) {
                        alert("Ошибка: " + result.d.Message);
                    }
                    else {
                        alert("Ошибка подключения");
                    }
                },
                complete: function () {
                    _this.hideLoad();
                }
            });
        },
        getParam: function (name) {
            var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
            if (results == null) {
                return null;
            }
            else {
                return results[1] || 0;
            }
        },
        getCookie: function (name) {
            var matches = document.cookie.match(new RegExp(
              "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
            ));
            return matches ? decodeURIComponent(matches[1]) : undefined;
        },

        setCookie: function (name, value, options) {
            options = options || {};

            var expires = options.expires;

            if (typeof expires == "number" && expires) {
                var d = new Date();
                d.setTime(d.getTime() + expires * 1000);
                expires = options.expires = d;
            }
            if (expires && expires.toUTCString) {
                options.expires = expires.toUTCString();
            }

            value = encodeURIComponent(value);

            var updatedCookie = name + "=" + value;

            for (var propName in options) {
                updatedCookie += "; " + propName;
                var propValue = options[propName];
                if (propValue !== true) {
                    updatedCookie += "=" + propValue;
                }
            }

            document.cookie = updatedCookie;
        },
        deleteCookie: function (name) {
            setCookie(name, "", {
                expires: -1
            });
        }
    };
})();