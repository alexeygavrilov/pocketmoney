(function() {
  $.showErrorMessage = function(message) {
    var box;
    box = $('#error');
    box.text("Ошибка: " + message);
    box.fadeIn(700);
    return setTimeout((function() {
      return box.fadeOut(700);
    }), 10000);
  };

  $.getResult = function(result, success) {
    if (result.Success === false && result.Message) {
      return $.showErrorMessage(result.Message);
    } else {
      return success(result);
    }
  };

  $.getDate = function(date) {
    var day, month;
    day = ("0" + date.getDate()).slice(-2);
    month = ("0" + (date.getMonth() + 1)).slice(-2);
    return day + "." + month + "." + date.getFullYear();
  };

  $.getFormatDate = function(Data) {
    if (Data) {
      return $.getDate(new Date(parseInt(Data.substr(6))));
    }
  };

  $.getFormatTime = function(Data) {
    var hours, minutes;
    if (Data) {
      hours = ("0" + Data.Hours).slice(-2);
      minutes = ("0" + Data.Minutes).slice(-2);
      return hours + ":" + minutes;
    }
  };

}).call(this);
