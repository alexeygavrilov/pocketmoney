exports = @
$ = jQuery
ops =
    elements: 
        '#shoppingAssignedToChooseBox': 'AssignedToChooseBox'
        '#shoppingList': 'shoppingList'
        '#shoppingName': 'Name'
        '#shoppingDescription': 'Description'
        '#shoppingReward': 'Reward'
        '#shoppingRewardRadioPoints': 'RewardRadioPoints'
        '#shoppingRewardRadioGift': 'RewardRadioGift'
        '#shoppingRewardLabelPoints': 'RewardLabelPoints'
        '#shoppingRewardPoints': 'RewardPoints'
        '#shoppingRewardLabelGift': 'RewardLabelGift'
        '#shoppingRewardGift': 'RewardGift'
        '#shoppingReminderTime': 'ReminderTime'
        'div.validation-summary-errors > ul': 'validationSummary'
        '#actionPostShoppingTask': 'postTaskBtn'
        '#shoppingReminderTime': 'ReminderTime'
        '#shoppingDeadlineDate': 'DeadlineDate'

    events:
        'click #btnAddRow': 'addRow'
        'click #shoppingRewardRadioPoints': 'selectPoints'
        'click #shoppingRewardRadioGift': 'selectGift'
        'click #actionPostShoppingTask': 'postTask'

    proxied: ['showTask']

    init: ->
       @App.bind 'ShowShoppingTask', @showTask

    showTask: (taskId) ->
        @AssignedToChooseBox.empty()
        $('#assignedToBox').clone(true).show().appendTo @AssignedToChooseBox
        @clearForm()
        @grid = @shoppingList.grid
                    dataSource: []
                    dataKey: "OrderNumber",
                    columns: [ { field: 'OrderNumber', hidden: true}, { field: 'ItemName', editor: @editorText, title: '�������', cssClass: 'Column' }, { field: 'Qty', editor: @editorNumber, title: '����������', cssClass: "Column" }, { field: 'Processed', hidden: true, cssClass: "processed" } ]
                            
        @loadTask(taskId)

    clearForm: ->
        @validationSummary.empty()
        @Name.val "" 
        @DeadlineDate.val ""
        @Description.val ""
        $('input[data-user-id]', @AssignedToChooseBox).each -> $(@).prop "checked", false
        @RewardRadioGift.removeAttr "checked"
        @RewardRadioPoints.attr "checked", "checked"
        @selectPoints()
        @ReminderTime.val ""
        @RewardGift.val ""
        @RewardPoints.val "0"

    selectPoints: ->
        @RewardLabelGift.hide()
        @RewardGift.hide()
        @RewardLabelPoints.show()
        @RewardPoints.show()

    selectGift: ->
        @RewardLabelPoints.hide()
        @RewardPoints.hide()
        @RewardLabelGift.show()
        @RewardGift.show()

    addRow: ->
        @grid.addRow { 'OrderNumber': @grid.count() + 1, 'ItemName': '', 'Qty': '', 'Processed': 'false' } 
        

    loadTask: (taskId) ->
        if taskId
            $.get @settings.GetShoppingTaskUrl + "?_=" + new Date().getTime(), 
                taskId: taskId 
                (result) =>
                    $.getResult result, =>
                        @Name.val result.Data.ShopName
                        @DeadlineDate.val $.getFormatDate(result.Data.DeadlineDate)
                        @Description.val result.Data.Text
                        @ReminderTime.val $.getFormatTime(result.Data.ReminderTime)
                        @RewardPoints.val result.Data.Points
                        @RewardGift.val result.Data.Gift
                        if result.Data.Gift and result.Data.Gift.length > 0
                            @RewardRadioPoints.removeAttr "checked"
                            @RewardRadioGift.attr "checked", "checked"
                            @selectGift()

                        @App.trigger 'FillUserList', result.Data.AssignedTo, @AssignedToChooseBox
                        @grid.data().dataSource = result.Data.ShoppingList
                        @grid.reload()
                        
                            
    editorText: ($container, currentValue) ->
        if $container.closest("tr[data-role=row]").find(".processed > div").text() is "false"
            $container.append("<input type='text' value='#{currentValue}' />")
        else 
            $container.append(currentValue)

    editorNumber:($container, currentValue) -> 
        if $container.closest("tr[data-role=row]").find(".processed > div").text() is "false"
            $container.append("<input type='number' value='#{currentValue}' min='1'/>")    
        else 
            $container.append(currentValue)

    getSelectedUsersId: ->
        result = []
        result.push $(checkBox).data("user-id") for checkBox in @AssignedToChooseBox.find("input[data-user-id]:checked")
        result


    validate: -> 
        @validationSummary.empty()
        @Name.removeClass 'input-validation-error'
        if  @Name.val() is ''
            @validationSummary.append '<li>��� - ��� ������������ ����</li>'
            @Name.addClass 'input-validation-error'
        if  @RewardPoints.val() <= 0 and @RewardGift.val() is ""
            @validationSummary.append '<li>�� ������ ������� ������ ���� �������</li>'

    postTask:(eventObject) ->
        false if !@validate()

        taskId = @postTaskBtn.data("task-id");

        url = if taskId and taskId.length > 0 then @postTaskBtn.data "action-url-update" else @postTaskBtn.data "action-url-add"

        
        gridData = @grid.getAll()
        $.ajax 
            type: "POST"
            url: url + "?_=" + new Date().getTime()
            datatype: "json"
            traditional: true
            data:
                Id: taskId
                ShopName: @Name.val()
                DeadlineDate: @DeadlineDate.val()
                Text: @Description.val()
                ReminderTime: @ReminderTime.val()
                AssignedTo: @getSelectedUsersId()
                Points: @RewardPoints.val()
                Gift: @RewardGift.val()
                ShoppingList: JSON.stringify(gridData)
            success: (result) =>
                    $.getResult result, =>
                        @App.trigger 'ShowTaskList'
            

exports.ShoppingTaskController = Spine.Controller.create(ops);
