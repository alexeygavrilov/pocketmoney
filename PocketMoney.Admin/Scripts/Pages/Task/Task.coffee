exports = @
$ = jQuery
ops =
    elements: 
        '#taskRowTemplate': 'rowTemplate'
        '#areaList': 'areaList'
        '#tableTasks > tbody': 'table'
        '#HomeworkTask': 'homework'
        '#OneTimeTask': 'onetime'
        '#CleanTask': 'clean'
        '#RepeatTask': 'repeat'
        '#ShoppingTask': 'shop'
        '.add-task-list': 'addTaskList'
        '#checkboxUsersTemplate': 'checkboxUsersTemplate'
        '#assignedToBox': 'AssignedToBox'
        '#assignedToChooseBox': 'AssignedToChooseBox'
        '#areaTaskStatus' : 'statusTaskList'
        '#operationRowTemplate': 'operationRowTemplate'
        '#tableOperations > tbody' : 'tableOperations'

    events:
        'click .actionBack': 'hideEditTask'
        'click #actionAdd': 'showCreateTask'
        'click li.add-task-list-choose': 'showEditTask'
        'blur .add-task-area': 'hideCreateTask'
        'click .js-actionEdit': 'showEditTask'
        'click .js-actionRemove': 'deleteTask'
        'click .js-task-status': 'showStatusTask'
        'click .js-task-status-back': 'hideStatusTask'
        'click #assignedToFloating': 'chooseAll'
        'click .js-action-�ancel': 'cancelAction'

    proxied: ['showTaskList']

    init: ->
        @App.bind 'ShowTaskList', @showTaskList
        @App.bind 'FillUserList', @fillUserList

        exports.HomeworkTaskController.init
                el: $("#HomeworkTask")
                settings: @settings

        exports.OneTimeTaskController.init
                el: $("#OneTimeTask")
                settings: @settings

        exports.CleanTaskController.init
                el: $("#CleanTask")
                settings: @settings

        exports.ShoppingTaskController.init
                el: $("#ShoppingTask")
                settings: @settings

        exports.RepeatTaskController.init
                el: $("#RepeatTask")
                settings: @settings
        
        @loadData()
        @loadChooseBox()
    
    loadData: ->
        $.get @settings.GetTasksUrl + "?_=" + new Date().getTime(), (result) =>
            $.getResult result, =>
                @table.empty()
                @table.append @rowTemplate.tmpl(row) for row in result.List

    loadChooseBox: ->
        $.get @settings.GetFamilyMembersUrl + "?_=" + new Date().getTime(), (result) =>
            $.getResult result, =>
                @AssignedToChooseBox.empty()
                @AssignedToChooseBox.append @checkboxUsersTemplate.tmpl(user) for user in result.List
                rightBox = @AssignedToChooseBox.find('label').filter(':odd')
                leftBox = @AssignedToChooseBox.find('label').filter(':even')
                rightBox.wrapAll "<div class='right-part' />"
                leftBox.wrapAll "<div class='left-part' />"
                @AssignedToChooseBox.append '<div style="clear:both;"></div>'

    loadTaskStatus: (taskId) ->
        $.get "#{@settings.GetTaskStatusUrl}?taskId=#{taskId}&_=#{new Date().getTime()}", (result) =>
            $.getResult result, =>
                @tableOperations.empty()
                @tableOperations.append @operationRowTemplate.tmpl(row) for row in result.List
                @areaList.hide 'slide', { direction: 'left' }, 400, =>
                    @statusTaskList.show "slide", { direction: "right" }, 400

    chooseAll: (eventObject)->
        assignedToCheckBoxes = $(eventObject.target).parent().siblings("#assignedToChooseBox").find("input[data-user-id]")
        if eventObject.target.checked is true
            assignedToCheckBoxes.each -> 
                $(@).prop "checked", false
                $(@).attr "disabled", "disabled"
        else
            assignedToCheckBoxes.each -> 
                $(@).removeAttr "disabled"

    showTaskList: ->
        @hideEditTask()
        @loadData()

    fillUserList: (assignedTo, chooseBox) ->
        if assignedTo and assignedTo.length > 0
            $('#assignedToFloating', chooseBox).prop "checked", false
            $('input[data-user-id]', chooseBox).each -> $(@).removeAttr "disabled"
            $("input[data-user-id=#{user.UserId}]", chooseBox).prop "checked", true for user in assignedTo
        else
            $('#assignedToFloating', chooseBox).prop "checked", true
            $('input[data-user-id]', chooseBox).each -> $(@).attr "disabled", "disabled"

    hideCreateTask: ->    
        @addTaskList.slideUp 400
    
    showCreateTask: ->
        @addTaskList.slideDown 400

    hideEditTask: ->
        $(".task:visible").each (indx, element) =>
            $(element).hide 'slide', { direction: 'right' }, 400, =>
                @areaList.show 'slide', { direction: 'left' }, 400         

    deleteTask:(eventObject) ->
        if confirm "�� ����� ������ ������� �������?"
            $.post @settings.DeleteTaskUrl,
                taskId: $(eventObject.target).data("task-id"),
                (result)=> 
                    $.getResult result, => 
                        @loadData()
        

    showEditTask: (eventObject) ->
        
        taskName = $(eventObject.target).data "task-type"
        taskId = $(eventObject.target).data "task-id"

        postTaskBtn = $ "#actionPost#{taskName}"

        if $(eventObject.target).hasClass "add-task-list-choose"  
            postTaskBtn.data "task-id", ""
            postTaskBtn.val postTaskBtn.data("text-add")
        else
            postTaskBtn.data "task-id", taskId
            postTaskBtn.val postTaskBtn.data("text-update")

        @addTaskList.hide()

        @areaList.hide 'slide', { direction: 'left' }, 400, =>
            @App.trigger "Show#{taskName}", taskId
            $("##{taskName}").show "slide", { direction: "right" }, 400

    showStatusTask: (eventObject) ->
        return false if $(eventObject.target).data("has-status") is false

        @loadTaskStatus $(eventObject.target).data("task-id")

    hideStatusTask: ->
        @statusTaskList.hide 'slide', { direction: 'right' }, 400, =>
            @areaList.show "slide", { direction: "left" }, 400

    cancelAction: (eventObject) ->
        if window.confirm($(eventObject.target).data("confirm")) is true
            taskId = $(eventObject.target).data "task-id"

            $.post @settings.CancelTaskActionUrl,
                taskActionId:  $(eventObject.target).data("task-action-id"),
                (result)=> 
                    $.getResult result, => 
                        @loadData()
                        @hideStatusTask()

exports.TaskController = Spine.Controller.create(ops);
