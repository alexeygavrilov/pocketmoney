exports = @
$ = jQuery
ops =
    elements: 
        '#repeatName': 'Name'
        '#repeatDescription': 'Description'
        '#repeatAssignedToChooseBox': 'AssignedToChooseBox'
        '#repeatReward': 'Reward'
        '#repeatRewardRadioPoints': 'RewardRadioPoints'
        '#repeatRewardRadioGift': 'RewardRadioGift'
        '#repeatRewardLabelPoints': 'RewardLabelPoints'
        '#repeatRewardPoints': 'RewardPoints'
        '#repeatRewardLabelGift': 'RewardLabelGift'
        '#repeatRewardGift': 'RewardGift'
        '#repeatReminderTime': 'ReminderTime'
        'div.validation-summary-errors > ul': 'validationSummary'
        '#actionPostRepeatTask': 'postTaskBtn'
        '#daySelector': 'daySelector'
        '#daySelectorRadioBox': 'daySelectorRadioBox'
        '#everyday': 'everyday'
        '#everyWeekday': 'everyWeekday'
        '#everyWeek': 'everyWeek'
        '#everyMonth': 'everyMonth'
        '#selectedInput': 'selectedInput'
        '#everyDayInput': 'everyDayInput'
        '#daysOfWeekInput': 'daysOfWeekInput'
        '#evetyMonthInput': 'everyMonthInput'
        '#dateRange': 'dateRange'
        '#startDate': 'startDate'
        '#selectEndAfter': 'selectEndAfter'
        '#EndAfter': 'EndAfter'
        '#selectEndBy': 'selectEndBy'
        '#EndBy': 'EndBy'
        '#everyWeekNum': 'everyWeekNum'
        


    events:
        'click #repeatRewardRadioPoints': 'selectPoints'
        'click #repeatRewardRadioGift': 'selectGift'
        'click #actionPostRepeatTask': 'postTask'
        'click #everyday': 'selectEveryday'
        'click #everyWeekday': 'selectEveryWeekday'
        'click #everyWeek': 'selectEveryWeek'
        'click #everyMonth': 'selectEveryMonth'
        'click #selectEndAfter': 'clickSelectEndAfter'
        'click #selectEndBy': 'clickSelectEndBy'

    proxied: ['showTask']
        
    init: ->
       @App.bind 'ShowRepeatTask', @showTask

    showTask: (taskId) ->
        @AssignedToChooseBox.empty()
        $('#assignedToBox').clone(true).show().appendTo @AssignedToChooseBox
        @clearForm()
        @loadTask(taskId)

    clearForm: ->
        @validationSummary.empty()
        @Name.val "" 
        @Description.val ""
        $('input[data-user-id]', @AssignedToChooseBox).each -> $(@).prop "checked", false
        @RewardRadioGift.removeAttr "checked"
        @RewardRadioPoints.attr "checked", "checked"
        @selectPoints()
        @ReminderTime.val ""
        @startDate.val ""
        @everyday.click()
        @everyDayInput.find("input").val "1"
        @everyWeekNum.val "1"
        @everyMonthInput.children('input[type=number]:first').val "1"
        @everyMonthInput.children('input[type=number]:last').val "1"
        @EndAfter.val ""
        @daysOfWeekInput.find("input[data-day-of-week]").prop "checked", false
        @RewardGift.val ""
        @RewardPoints.val "0"
        @selectEndBy.attr "checked", "checked"
        
    hideAll:->
        @everyDayInput.hide()
        @daysOfWeekInput.hide()
        @everyMonthInput.hide()
        @daysOfWeekInput.children('input[type=checkbox]').each -> $(@).prop "checked", false

    selectEveryday:->
        @hideAll()
        @everyDayInput.show()
        @everyDayInput.find("input").focus()

    selectEveryWeekday:->
        @hideAll()

    selectEveryWeek:->
        @hideAll()
        @daysOfWeekInput.show()
        @everyWeekNum.focus()

    selectEveryMonth:->
        @hideAll()
        @everyMonthInput.show()
        @everyMonthInput.children('input[type=number]:first').focus()

    clickSelectEndAfter:->
        @EndAfter.focus()

    clickSelectEndBy:->
        ;#@EndBy.focus()

     getSelectedDaysOfWeek: ->
        result = []
        result.push $(checkBox).data("day-of-week") for checkBox in  @daysOfWeekInput.find("input[data-day-of-week]:checked")
        result

    loadTask: (taskId) ->
        if taskId
            $.get @settings.GetRepeatTaskUrl + "?_=" + new Date().getTime(), 
                taskId: taskId 
                (result) =>
                    $.getResult result, =>
                        @Name.val result.Data.Name
                        @Description.val result.Data.Text
                        @ReminderTime.val $.getFormatTime(result.Data.ReminderTime)
                        @RewardPoints.val result.Data.Points
                        @RewardGift.val result.Data.Gift
                        if result.Data.Gift and result.Data.Gift.length > 0
                            @RewardRadioPoints.removeAttr "checked"
                            @RewardRadioGift.attr "checked", "checked"
                            @selectGift()
                        @App.trigger 'FillUserList', result.Data.AssignedTo, @AssignedToChooseBox
                        @daysOfWeekInput.find("input[data-day-of-week=#{day}]").prop "checked", true for day in result.Data.Form.DaysOfWeek
                        @daySelectorRadioBox.find("input[data-occurrence-type=#{result.Data.Form.OccurrenceType}]").prop "checked", true
                        @daySelectorRadioBox.find("input[data-occurrence-type=#{result.Data.Form.OccurrenceType}]").click()
                        @everyDayInput.children('input[type=number]').val result.Data.Form.EveryDay
                        @everyMonthInput.children('input[type=number]:first').val result.Data.Form.DayOfMonth
                        @everyMonthInput.children('input[type=number]:last').val result.Data.Form.EveryMonth
                        @startDate.val $.getFormatDate(result.Data.Form.DateRangeFrom)
                        @EndAfter.val result.Data.Form.OccurrenceNumber
                        @EndBy.val $.getFormatDate(result.Data.Form.DateRangeTo)
                        @everyWeekNum.val result.Data.Form.EveryWeek

                        if not result.Data.Form.OccurrenceNumber
                            @selectEndBy.click()
                        else
                            @selectEndAfter.click()
        else  
            startDate = new Date()
            endDate = new Date startDate
            endDate.setMonth startDate.getMonth() + 1
            @startDate.val $.getDate(startDate)
            @EndBy.val $.getDate(endDate) 

     postTask:(eventObject) ->
        false if !@validate()
        url = if @postTaskBtn.data("task-id").length > 0 then @postTaskBtn.data "action-url-update" else @postTaskBtn.data "action-url-add"
        $.ajax 
            type: "POST"
            url: url + "?_=" + new Date().getTime()
            datatype: "json"
            traditional: true
            data:
                Id: @postTaskBtn.data "task-id"
                Name: @Name.val()
                Text: @Description.val()
                ReminderTime: @ReminderTime.val()
                AssignedTo: @getSelectedUsersId()
                Points: @RewardPoints.val()
                Gift: @RewardGift.val()
                Form: JSON.stringify(
                    EveryWeek: @everyWeekNum.val()
                    OccurrenceType: @daySelectorRadioBox.find("input:checked:first").data "occurrence-type"
                    DaysOfWeek: @getSelectedDaysOfWeek()
                    EveryDay: @everyDayInput.children('input[type=number]').val()
                    DayOfMonth: @everyMonthInput.children('input[type=number]:first').val()
                    EveryMonth: @everyMonthInput.children('input[type=number]:last').val()
                    DateRangeFrom: @startDate.val()
                    DateRangeTo:  if @selectEndBy.attr("checked") is 'checked' then @EndBy.val() else null
                    OccurrenceNumber: if @selectEndAfter.attr("checked") is "checked" then @EndAfter.val() else null
                    DaysOfWeek: @getSelectedDaysOfWeek()
                    )
            success: (result) =>
                  $.getResult result, =>
                      @App.trigger 'ShowTaskList'

    selectPoints: ->
        @RewardLabelGift.hide()
        @RewardGift.hide()
        @RewardLabelPoints.show()
        @RewardPoints.show()

    selectGift: ->
        @RewardLabelPoints.hide()
        @RewardPoints.hide()
        @RewardLabelGift.show()
        @RewardGift.show()
    
    getSelectedUsersId: ->
        result = []
        result.push $(checkBox).data("user-id") for checkBox in @AssignedToChooseBox.find("input[data-user-id]:checked")
        result

    validate: ->
        @validationSummary.empty()
        @Name.removeClass 'input-validation-error'
        if  @Name.val() is ''
            @validationSummary.append '<li>��� - ��� ������������ ����</li>'
            @Name.addClass 'input-validation-error'
        if  @RewardPoints.val() <= 0 and @RewardGift.val() is ""
            @validationSummary.append '<li>�� ������ ������� ������ ���� �������</li>'
    
exports.RepeatTaskController = Spine.Controller.create(ops);
