exports = @
$ = jQuery
ops =
    elements:
        '#cleanRoomName': 'RoomName'
        '#cleanDescription': 'Description'
        '#cleanDaySelector': 'daySelector'
        '#CleanSelectHollidays': 'includeHolidays'
        '#cleanAssignedToChooseBox': 'AssignedToChooseBox'
        '#cleanReward': 'Reward'  
        '#cleanRewardRadioPoints': 'RewardRadioPoints'   
        '#cleanRewardRadioGift': 'RewardRadioGift'  
        '#cleanChooseReward': 'ChooseReward'
        '#cleanRewardLabelPoints': 'RewardLabelPoints'
        '#cleanRewardPoints': 'RewardPoints'
        '#cleanRewardLabelGift': 'RewardLabelGift'
        '#cleanRewardGift': 'RewardGift'
        '#cleanReminderTime': 'ReminderTime'
        'div.validation-summary-errors > ul': 'validationSummary'
        '#actionPostCleanTask': 'postTaskBtn'

    events:
        'click #cleanRewardRadioPoints': 'selectPoints'
        'click #cleanRewardRadioGift': 'selectGift'
        'click #actionPostCleanTask': 'postTask'

    proxied: ['showTask']
        
    init: ->
        @App.bind 'ShowCleanTask', @showTask
        @RoomName.autocomplete 
            source: @RoomName.data "url"
            minLength: 2
    
    showTask: (taskId) ->
        @AssignedToChooseBox.empty()
        $('#assignedToBox').clone(true).show().appendTo @AssignedToChooseBox
        @clearForm()
        @loadTask(taskId)

    clearForm: ->
        @validationSummary.empty()
        @RoomName.val "" 
        @Description.val ""
        $('input[data-user-id]', @AssignedToChooseBox).each -> $(@).prop "checked", false
        $('input[data-day-of-week]', @daySelector).each -> $(@).prop "checked", false
        @RewardRadioGift.removeAttr "checked"
        @RewardRadioPoints.attr "checked", "checked"
        @selectPoints()
        @ReminderTime.val ""
        @RewardGift.val ""
        @RewardPoints.val "0"

    selectPoints: ->
        @RewardLabelGift.hide()
        @RewardGift.hide()
        @RewardLabelPoints.show()
        @RewardPoints.show()

    selectGift: ->
        @RewardLabelPoints.hide()
        @RewardPoints.hide()
        @RewardLabelGift.show()
        @RewardGift.show()

    loadTask: (taskId) ->

        if taskId
            $.get @settings.GetCleanTaskUrl + "?_=" + new Date().getTime(), 
                taskId: taskId 
                (result) =>
                    $.getResult result, =>
                        @RoomName.val result.Data.RoomName
                        @Description.val result.Data.Text
                        @ReminderTime.val $.getFormatTime(result.Data.ReminderTime)
                        @RewardPoints.val result.Data.Points
                        @RewardGift.val result.Data.Gift
                        if result.Data.Gift and result.Data.Gift.length > 0
                            @RewardRadioPoints.removeAttr "checked"
                            @RewardRadioGift.attr "checked", "checked"
                            @selectGift()

                        @App.trigger 'FillUserList', result.Data.AssignedTo, @AssignedToChooseBox
                        $("input[data-day-of-week=#{day}]", @daySelector).prop "checked", true for day in result.Data.DaysOfWeek
                            
    getSelectedUsersId: ->
        result = []
        result.push $(checkBox).data("user-id") for checkBox in @AssignedToChooseBox.find("input[data-user-id]:checked")
        result
    
    getSelectedDaysOfWeek: ->
        result = []
        result.push $(checkBox).data("day-of-week") for checkBox in @daySelector.find("input[data-day-of-week]:checked")
        result

    validate: ->
        @validationSummary.empty()
        @RoomName.removeClass 'input-validation-error'
        if  @RoomName.val() is ''
            @validationSummary.append '<li>������� - ��� ������������ ����</li>'
            @RoomName.addClass 'input-validation-error'

        if  not @daySelector.find("input[day-of-week]:checked")
            @validationSummary.append '<li>�� �� ������� �� ������ ���</li>'
        
        if  @RewardPoints.val() <= 0 and @RewardGift.val() is ""
            @validationSummary.append '<li>�� ������ ������� ������ ���� �������</li>'

    postTask:(eventObject) ->
        false if !@validate()
        url = if @postTaskBtn.data("task-id").length > 0 then @postTaskBtn.data "action-url-update" else @postTaskBtn.data "action-url-add"
        $.ajax 
            type: "POST"
            url: url + "?_=" + new Date().getTime()
            datatype: "json"
            traditional: true
            data:
                Id: @postTaskBtn.data "task-id"
                RoomName: @RoomName.val()
                Text: @Description.val()
                ReminderTime: @ReminderTime.val()
                AssignedTo: @getSelectedUsersId()
                Points: @RewardPoints.val()
                Gift: @RewardGift.val()
                DaysOfWeek: @getSelectedDaysOfWeek()
            success: (result) =>
                  $.getResult result, =>
                      @App.trigger 'ShowTaskList'

exports.CleanTaskController = Spine.Controller.create(ops);
