(function() {
  var $, exports, ops;

  exports = this;

  $ = jQuery;

  ops = {
    elements: {
      '#taskRowTemplate': 'rowTemplate',
      '#areaList': 'areaList',
      '#tableTasks > tbody': 'table',
      '#HomeworkTask': 'homework',
      '#OneTimeTask': 'onetime',
      '#CleanTask': 'clean',
      '#RepeatTask': 'repeat',
      '#ShoppingTask': 'shop',
      '.add-task-list': 'addTaskList',
      '#checkboxUsersTemplate': 'checkboxUsersTemplate',
      '#assignedToBox': 'AssignedToBox',
      '#assignedToChooseBox': 'AssignedToChooseBox',
      '#areaTaskStatus': 'statusTaskList',
      '#operationRowTemplate': 'operationRowTemplate',
      '#tableOperations > tbody': 'tableOperations'
    },
    events: {
      'click .actionBack': 'hideEditTask',
      'click #actionAdd': 'showCreateTask',
      'click li.add-task-list-choose': 'showEditTask',
      'blur .add-task-area': 'hideCreateTask',
      'click .js-actionEdit': 'showEditTask',
      'click .js-actionRemove': 'deleteTask',
      'click .js-task-status': 'showStatusTask',
      'click .js-task-status-back': 'hideStatusTask',
      'click #assignedToFloating': 'chooseAll',
      'click .js-action-сancel': 'cancelAction'
    },
    proxied: ['showTaskList'],
    init: function() {
      this.App.bind('ShowTaskList', this.showTaskList);
      this.App.bind('FillUserList', this.fillUserList);
      exports.HomeworkTaskController.init({
        el: $("#HomeworkTask"),
        settings: this.settings
      });
      exports.OneTimeTaskController.init({
        el: $("#OneTimeTask"),
        settings: this.settings
      });
      exports.CleanTaskController.init({
        el: $("#CleanTask"),
        settings: this.settings
      });
      exports.ShoppingTaskController.init({
        el: $("#ShoppingTask"),
        settings: this.settings
      });
      exports.RepeatTaskController.init({
        el: $("#RepeatTask"),
        settings: this.settings
      });
      this.loadData();
      return this.loadChooseBox();
    },
    loadData: function() {
      return $.get(this.settings.GetTasksUrl + "?_=" + new Date().getTime(), (function(_this) {
        return function(result) {
          return $.getResult(result, function() {
            var row, _i, _len, _ref, _results;
            _this.table.empty();
            _ref = result.List;
            _results = [];
            for (_i = 0, _len = _ref.length; _i < _len; _i++) {
              row = _ref[_i];
              _results.push(_this.table.append(_this.rowTemplate.tmpl(row)));
            }
            return _results;
          });
        };
      })(this));
    },
    loadChooseBox: function() {
      return $.get(this.settings.GetFamilyMembersUrl + "?_=" + new Date().getTime(), (function(_this) {
        return function(result) {
          return $.getResult(result, function() {
            var leftBox, rightBox, user, _i, _len, _ref;
            _this.AssignedToChooseBox.empty();
            _ref = result.List;
            for (_i = 0, _len = _ref.length; _i < _len; _i++) {
              user = _ref[_i];
              _this.AssignedToChooseBox.append(_this.checkboxUsersTemplate.tmpl(user));
            }
            rightBox = _this.AssignedToChooseBox.find('label').filter(':odd');
            leftBox = _this.AssignedToChooseBox.find('label').filter(':even');
            rightBox.wrapAll("<div class='right-part' />");
            leftBox.wrapAll("<div class='left-part' />");
            return _this.AssignedToChooseBox.append('<div style="clear:both;"></div>');
          });
        };
      })(this));
    },
    loadTaskStatus: function(taskId) {
      return $.get("" + this.settings.GetTaskStatusUrl + "?taskId=" + taskId + "&_=" + (new Date().getTime()), (function(_this) {
        return function(result) {
          return $.getResult(result, function() {
            var row, _i, _len, _ref;
            _this.tableOperations.empty();
            _ref = result.List;
            for (_i = 0, _len = _ref.length; _i < _len; _i++) {
              row = _ref[_i];
              _this.tableOperations.append(_this.operationRowTemplate.tmpl(row));
            }
            return _this.areaList.hide('slide', {
              direction: 'left'
            }, 400, function() {
              return _this.statusTaskList.show("slide", {
                direction: "right"
              }, 400);
            });
          });
        };
      })(this));
    },
    chooseAll: function(eventObject) {
      var assignedToCheckBoxes;
      assignedToCheckBoxes = $(eventObject.target).parent().siblings("#assignedToChooseBox").find("input[data-user-id]");
      if (eventObject.target.checked === true) {
        return assignedToCheckBoxes.each(function() {
          $(this).prop("checked", false);
          return $(this).attr("disabled", "disabled");
        });
      } else {
        return assignedToCheckBoxes.each(function() {
          return $(this).removeAttr("disabled");
        });
      }
    },
    showTaskList: function() {
      this.hideEditTask();
      return this.loadData();
    },
    fillUserList: function(assignedTo, chooseBox) {
      var user, _i, _len, _results;
      if (assignedTo && assignedTo.length > 0) {
        $('#assignedToFloating', chooseBox).prop("checked", false);
        $('input[data-user-id]', chooseBox).each(function() {
          return $(this).removeAttr("disabled");
        });
        _results = [];
        for (_i = 0, _len = assignedTo.length; _i < _len; _i++) {
          user = assignedTo[_i];
          _results.push($("input[data-user-id=" + user.UserId + "]", chooseBox).prop("checked", true));
        }
        return _results;
      } else {
        $('#assignedToFloating', chooseBox).prop("checked", true);
        return $('input[data-user-id]', chooseBox).each(function() {
          return $(this).attr("disabled", "disabled");
        });
      }
    },
    hideCreateTask: function() {
      return this.addTaskList.slideUp(400);
    },
    showCreateTask: function() {
      return this.addTaskList.slideDown(400);
    },
    hideEditTask: function() {
      return $(".task:visible").each((function(_this) {
        return function(indx, element) {
          return $(element).hide('slide', {
            direction: 'right'
          }, 400, function() {
            return _this.areaList.show('slide', {
              direction: 'left'
            }, 400);
          });
        };
      })(this));
    },
    deleteTask: function(eventObject) {
      if (confirm("вы точно хотите удалить задание?")) {
        return $.post(this.settings.DeleteTaskUrl, {
          taskId: $(eventObject.target).data("task-id")
        }, (function(_this) {
          return function(result) {
            return $.getResult(result, function() {
              return _this.loadData();
            });
          };
        })(this));
      }
    },
    showEditTask: function(eventObject) {
      var postTaskBtn, taskId, taskName;
      taskName = $(eventObject.target).data("task-type");
      taskId = $(eventObject.target).data("task-id");
      postTaskBtn = $("#actionPost" + taskName);
      if ($(eventObject.target).hasClass("add-task-list-choose")) {
        postTaskBtn.data("task-id", "");
        postTaskBtn.val(postTaskBtn.data("text-add"));
      } else {
        postTaskBtn.data("task-id", taskId);
        postTaskBtn.val(postTaskBtn.data("text-update"));
      }
      this.addTaskList.hide();
      return this.areaList.hide('slide', {
        direction: 'left'
      }, 400, (function(_this) {
        return function() {
          _this.App.trigger("Show" + taskName, taskId);
          return $("#" + taskName).show("slide", {
            direction: "right"
          }, 400);
        };
      })(this));
    },
    showStatusTask: function(eventObject) {
      if ($(eventObject.target).data("has-status") === false) {
        return false;
      }
      return this.loadTaskStatus($(eventObject.target).data("task-id"));
    },
    hideStatusTask: function() {
      return this.statusTaskList.hide('slide', {
        direction: 'right'
      }, 400, (function(_this) {
        return function() {
          return _this.areaList.show("slide", {
            direction: "left"
          }, 400);
        };
      })(this));
    },
    cancelAction: function(eventObject) {
      var taskId;
      if (window.confirm($(eventObject.target).data("confirm")) === true) {
        taskId = $(eventObject.target).data("task-id");
        return $.post(this.settings.CancelTaskActionUrl, {
          taskActionId: $(eventObject.target).data("task-action-id")
        }, (function(_this) {
          return function(result) {
            return $.getResult(result, function() {
              _this.loadData();
              return _this.hideStatusTask();
            });
          };
        })(this));
      }
    }
  };

  exports.TaskController = Spine.Controller.create(ops);

}).call(this);
