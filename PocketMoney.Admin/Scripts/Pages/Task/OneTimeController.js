(function() {
  var $, exports, ops;

  exports = this;

  $ = jQuery;

  ops = {
    elements: '',
    events: '',
    init: function() {
      return this.loadData();
    }
  };

  exports.OneTimeTaskController = Spine.Controller.create(ops);

}).call(this);
