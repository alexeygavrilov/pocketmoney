(function() {
  var $, exports, ops;

  exports = this;

  $ = jQuery;

  ops = {
    elements: {
      '#OneTimeRewardPoints': 'OneTimeRewardPoints',
      '#oneTimeName': 'Name',
      '#oneTimeDeadlineDate': 'DeadlineDate',
      '#oneTimeDescription': 'Description',
      '#oneTimeAssignedToChooseBox': 'AssignedToChooseBox',
      '#oneTimeReward': 'Reward',
      '#oneTimeRewardRadioPoints': 'RewardRadioPoints',
      '#oneTimeRewardRadioGift': 'RewardRadioGift',
      '#oneTimeRewardLabelPoints': 'RewardLabelPoints',
      '#oneTimeRewardPoints': 'RewardPoints',
      '#oneTimeRewardLabelGift': 'RewardLabelGift',
      '#oneTimeRewardGift': 'RewardGift',
      '#oneTimeReminderTime': 'ReminderTime',
      'div.validation-summary-errors > ul': 'validationSummary',
      '#actionPostOneTimeTask': 'postTaskBtn'
    },
    events: {
      'click #oneTimeRewardRadioPoints': 'selectPoints',
      'click #oneTimeRewardRadioGift': 'selectGift',
      'click #actionPostOneTimeTask': 'postTask'
    },
    proxied: ['showTask'],
    init: function() {
      return this.App.bind('ShowOneTimeTask', this.showTask);
    },
    showTask: function(taskId) {
      this.AssignedToChooseBox.empty();
      $('#assignedToBox').clone(true).show().appendTo(this.AssignedToChooseBox);
      this.clearForm();
      return this.loadTask(taskId);
    },
    clearForm: function() {
      this.validationSummary.empty();
      this.Name.val("");
      this.DeadlineDate.val("");
      this.Description.val("");
      $('input[data-user-id]', this.AssignedToChooseBox).each(function() {
        return $(this).prop("checked", false);
      });
      this.RewardRadioGift.removeAttr("checked");
      this.RewardRadioPoints.attr("checked", "checked");
      this.selectPoints();
      this.ReminderTime.val("");
      this.RewardGift.val("");
      return this.RewardPoints.val("0");
    },
    loadTask: function(taskId) {
      if (taskId) {
        return $.get(this.settings.GetOneTimeTaskUrl + "?_=" + new Date().getTime(), {
          taskId: taskId
        }, (function(_this) {
          return function(result) {
            return $.getResult(result, function() {
              _this.Name.val(result.Data.Name);
              _this.DeadlineDate.val($.getFormatDate(result.Data.DeadlineDate));
              _this.Description.val(result.Data.Text);
              _this.ReminderTime.val($.getFormatTime(result.Data.ReminderTime));
              _this.RewardPoints.val(result.Data.Points);
              _this.RewardGift.val(result.Data.Gift);
              if (result.Data.Gift && result.Data.Gift.length > 0) {
                _this.RewardRadioPoints.removeAttr("checked");
                _this.RewardRadioGift.attr("checked", "checked");
                _this.selectGift();
              }
              return _this.App.trigger('FillUserList', result.Data.AssignedTo, _this.AssignedToChooseBox);
            });
          };
        })(this));
      }
    },
    selectPoints: function() {
      this.RewardLabelGift.hide();
      this.RewardGift.hide();
      this.RewardLabelPoints.show();
      return this.RewardPoints.show();
    },
    selectGift: function() {
      this.RewardLabelPoints.hide();
      this.RewardPoints.hide();
      this.RewardLabelGift.show();
      return this.RewardGift.show();
    },
    getSelectedUsersId: function() {
      var checkBox, result, _i, _len, _ref;
      result = [];
      _ref = this.AssignedToChooseBox.find("input[data-user-id]:checked");
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        checkBox = _ref[_i];
        result.push($(checkBox).data("user-id"));
      }
      return result;
    },
    validate: function() {
      this.validationSummary.empty();
      this.Name.removeClass('input-validation-error');
      if (this.Name.val() === '') {
        this.validationSummary.append('<li>Имя - это обязательное поле</li>');
        this.Name.addClass('input-validation-error');
      }
      if (this.RewardPoints.val() <= 0 && this.RewardGift.val() === "") {
        return this.validationSummary.append('<li>Вы должны выбрать хотябы одну награду</li>');
      }
    },
    postTask: function(eventObject) {
      var taskId, url;
      if (!this.validate()) {
        false;
      }
      taskId = this.postTaskBtn.data("task-id");
      url = taskId && taskId.length > 0 ? this.postTaskBtn.data("action-url-update") : this.postTaskBtn.data("action-url-add");
      return $.ajax({
        type: "POST",
        url: url + "?_=" + new Date().getTime(),
        datatype: "json",
        traditional: true,
        data: {
          Id: taskId,
          Name: this.Name.val(),
          DeadlineDate: this.DeadlineDate.val(),
          Text: this.Description.val(),
          ReminderTime: this.ReminderTime.val(),
          AssignedTo: this.getSelectedUsersId(),
          Points: this.RewardPoints.val(),
          Gift: this.RewardGift.val()
        },
        success: (function(_this) {
          return function(result) {
            return $.getResult(result, function() {
              return _this.App.trigger('ShowTaskList');
            });
          };
        })(this)
      });
    }
  };

  exports.OneTimeTaskController = Spine.Controller.create(ops);

}).call(this);
