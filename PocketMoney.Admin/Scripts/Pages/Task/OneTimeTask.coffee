exports = @
$ = jQuery
ops =
    elements: 
        '#OneTimeRewardPoints': 'OneTimeRewardPoints'
        '#oneTimeName': 'Name'
        '#oneTimeDeadlineDate': 'DeadlineDate'
        '#oneTimeDescription': 'Description'
        '#oneTimeAssignedToChooseBox': 'AssignedToChooseBox'
        '#oneTimeReward': 'Reward'
        '#oneTimeRewardRadioPoints': 'RewardRadioPoints'
        '#oneTimeRewardRadioGift': 'RewardRadioGift'
        '#oneTimeRewardLabelPoints': 'RewardLabelPoints'
        '#oneTimeRewardPoints': 'RewardPoints'
        '#oneTimeRewardLabelGift': 'RewardLabelGift'
        '#oneTimeRewardGift': 'RewardGift'
        '#oneTimeReminderTime': 'ReminderTime'
        'div.validation-summary-errors > ul': 'validationSummary'
        '#actionPostOneTimeTask': 'postTaskBtn'


    events:
        'click #oneTimeRewardRadioPoints': 'selectPoints'
        'click #oneTimeRewardRadioGift': 'selectGift'
        'click #actionPostOneTimeTask': 'postTask'

    proxied: ['showTask']
        
    init: ->
       @App.bind 'ShowOneTimeTask', @showTask

    showTask: (taskId) ->
        @AssignedToChooseBox.empty()
        $('#assignedToBox').clone(true).show().appendTo @AssignedToChooseBox
        @clearForm()
        @loadTask(taskId)

    clearForm: ->
        @validationSummary.empty()
        @Name.val "" 
        @DeadlineDate.val ""
        @Description.val ""
        $('input[data-user-id]', @AssignedToChooseBox).each -> $(@).prop "checked", false
        @RewardRadioGift.removeAttr "checked"
        @RewardRadioPoints.attr "checked", "checked"
        @selectPoints()
        @ReminderTime.val ""
        @RewardGift.val ""
        @RewardPoints.val "0"

    loadTask: (taskId) ->
        if taskId
            $.get @settings.GetOneTimeTaskUrl + "?_=" + new Date().getTime(), 
                taskId: taskId 
                (result) =>
                    $.getResult result, =>
                        @Name.val result.Data.Name
                        @DeadlineDate.val $.getFormatDate(result.Data.DeadlineDate)
                        @Description.val result.Data.Text
                        @ReminderTime.val $.getFormatTime(result.Data.ReminderTime)
                        @RewardPoints.val result.Data.Points
                        @RewardGift.val result.Data.Gift
                        if result.Data.Gift and result.Data.Gift.length > 0
                            @RewardRadioPoints.removeAttr "checked"
                            @RewardRadioGift.attr "checked", "checked"
                            @selectGift()
                        @App.trigger 'FillUserList', result.Data.AssignedTo, @AssignedToChooseBox

    selectPoints: ->
        @RewardLabelGift.hide()
        @RewardGift.hide()
        @RewardLabelPoints.show()
        @RewardPoints.show()

    selectGift: ->
        @RewardLabelPoints.hide()
        @RewardPoints.hide()
        @RewardLabelGift.show()
        @RewardGift.show()
    
    getSelectedUsersId: ->
        result = []
        result.push $(checkBox).data("user-id") for checkBox in @AssignedToChooseBox.find("input[data-user-id]:checked")
        result

    validate: ->
        @validationSummary.empty()
        @Name.removeClass 'input-validation-error'
        if  @Name.val() is ''
            @validationSummary.append '<li>��� - ��� ������������ ����</li>'
            @Name.addClass 'input-validation-error'
        if  @RewardPoints.val() <= 0 and @RewardGift.val() is ""
            @validationSummary.append '<li>�� ������ ������� ������ ���� �������</li>'
         

    postTask:(eventObject) ->
        false if !@validate()

        taskId = @postTaskBtn.data("task-id");

        url = if taskId and taskId.length > 0 then @postTaskBtn.data "action-url-update" else @postTaskBtn.data "action-url-add"

        $.ajax 
            type: "POST"
            url: url + "?_=" + new Date().getTime()
            datatype: "json"
            traditional: true
            data:
                Id: taskId
                Name: @Name.val()
                DeadlineDate: @DeadlineDate.val()
                Text: @Description.val()
                ReminderTime: @ReminderTime.val()
                AssignedTo: @getSelectedUsersId()
                Points: @RewardPoints.val()
                Gift: @RewardGift.val()
            success: (result) =>
                  $.getResult result, =>
                      @App.trigger 'ShowTaskList'
    
exports.OneTimeTaskController = Spine.Controller.create(ops);
