(function() {
  var $, exports, ops;

  exports = this;

  $ = jQuery;

  ops = {
    elements: '',
    events: '',
    init: function() {}
  };

  exports.ShopTaskController = Spine.Controller.create(ops);

}).call(this);
