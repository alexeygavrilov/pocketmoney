(function() {
  var $, exports, ops;

  exports = this;

  $ = jQuery;

  ops = {
    elements: {
      '#shoppingAssignedToChooseBox': 'AssignedToChooseBox',
      '#shoppingList': 'shoppingList',
      '#shoppingName': 'Name',
      '#shoppingDescription': 'Description',
      '#shoppingReward': 'Reward',
      '#shoppingRewardRadioPoints': 'RewardRadioPoints',
      '#shoppingRewardRadioGift': 'RewardRadioGift',
      '#shoppingRewardLabelPoints': 'RewardLabelPoints',
      '#shoppingRewardPoints': 'RewardPoints',
      '#shoppingRewardLabelGift': 'RewardLabelGift',
      '#shoppingRewardGift': 'RewardGift',
      '#shoppingReminderTime': 'ReminderTime',
      'div.validation-summary-errors > ul': 'validationSummary',
      '#actionPostShoppingTask': 'postTaskBtn',
      '#shoppingReminderTime': 'ReminderTime',
      '#shoppingDeadlineDate': 'DeadlineDate'
    },
    events: {
      'click #btnAddRow': 'addRow',
      'click #shoppingRewardRadioPoints': 'selectPoints',
      'click #shoppingRewardRadioGift': 'selectGift',
      'click #actionPostShoppingTask': 'postTask'
    },
    proxied: ['showTask'],
    init: function() {
      return this.App.bind('ShowShoppingTask', this.showTask);
    },
    showTask: function(taskId) {
      this.AssignedToChooseBox.empty();
      $('#assignedToBox').clone(true).show().appendTo(this.AssignedToChooseBox);
      this.clearForm();
      this.grid = this.shoppingList.grid({
        dataSource: [],
        dataKey: "OrderNumber",
        columns: [
          {
            field: 'OrderNumber',
            hidden: true
          }, {
            field: 'ItemName',
            editor: this.editorText,
            title: 'Продукт',
            cssClass: 'Column'
          }, {
            field: 'Qty',
            editor: this.editorNumber,
            title: 'Количество',
            cssClass: "Column"
          }, {
            field: 'Processed',
            hidden: true,
            cssClass: "processed"
          }
        ]
      });
      return this.loadTask(taskId);
    },
    clearForm: function() {
      this.validationSummary.empty();
      this.Name.val("");
      this.DeadlineDate.val("");
      this.Description.val("");
      $('input[data-user-id]', this.AssignedToChooseBox).each(function() {
        return $(this).prop("checked", false);
      });
      this.RewardRadioGift.removeAttr("checked");
      this.RewardRadioPoints.attr("checked", "checked");
      this.selectPoints();
      this.ReminderTime.val("");
      this.RewardGift.val("");
      return this.RewardPoints.val("0");
    },
    selectPoints: function() {
      this.RewardLabelGift.hide();
      this.RewardGift.hide();
      this.RewardLabelPoints.show();
      return this.RewardPoints.show();
    },
    selectGift: function() {
      this.RewardLabelPoints.hide();
      this.RewardPoints.hide();
      this.RewardLabelGift.show();
      return this.RewardGift.show();
    },
    addRow: function() {
      return this.grid.addRow({
        'OrderNumber': this.grid.count() + 1,
        'ItemName': '',
        'Qty': '',
        'Processed': 'false'
      });
    },
    loadTask: function(taskId) {
      if (taskId) {
        return $.get(this.settings.GetShoppingTaskUrl + "?_=" + new Date().getTime(), {
          taskId: taskId
        }, (function(_this) {
          return function(result) {
            return $.getResult(result, function() {
              _this.Name.val(result.Data.ShopName);
              _this.DeadlineDate.val($.getFormatDate(result.Data.DeadlineDate));
              _this.Description.val(result.Data.Text);
              _this.ReminderTime.val($.getFormatTime(result.Data.ReminderTime));
              _this.RewardPoints.val(result.Data.Points);
              _this.RewardGift.val(result.Data.Gift);
              if (result.Data.Gift && result.Data.Gift.length > 0) {
                _this.RewardRadioPoints.removeAttr("checked");
                _this.RewardRadioGift.attr("checked", "checked");
                _this.selectGift();
              }
              _this.App.trigger('FillUserList', result.Data.AssignedTo, _this.AssignedToChooseBox);
              _this.grid.data().dataSource = result.Data.ShoppingList;
              return _this.grid.reload();
            });
          };
        })(this));
      }
    },
    editorText: function($container, currentValue) {
      if ($container.closest("tr[data-role=row]").find(".processed > div").text() === "false") {
        return $container.append("<input type='text' value='" + currentValue + "' />");
      } else {
        return $container.append(currentValue);
      }
    },
    editorNumber: function($container, currentValue) {
      if ($container.closest("tr[data-role=row]").find(".processed > div").text() === "false") {
        return $container.append("<input type='number' value='" + currentValue + "' min='1'/>");
      } else {
        return $container.append(currentValue);
      }
    },
    getSelectedUsersId: function() {
      var checkBox, result, _i, _len, _ref;
      result = [];
      _ref = this.AssignedToChooseBox.find("input[data-user-id]:checked");
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        checkBox = _ref[_i];
        result.push($(checkBox).data("user-id"));
      }
      return result;
    },
    validate: function() {
      this.validationSummary.empty();
      this.Name.removeClass('input-validation-error');
      if (this.Name.val() === '') {
        this.validationSummary.append('<li>Имя - это обязательное поле</li>');
        this.Name.addClass('input-validation-error');
      }
      if (this.RewardPoints.val() <= 0 && this.RewardGift.val() === "") {
        return this.validationSummary.append('<li>Вы должны выбрать хотябы одну награду</li>');
      }
    },
    postTask: function(eventObject) {
      var gridData, taskId, url;
      if (!this.validate()) {
        false;
      }
      taskId = this.postTaskBtn.data("task-id");
      url = taskId && taskId.length > 0 ? this.postTaskBtn.data("action-url-update") : this.postTaskBtn.data("action-url-add");
      gridData = this.grid.getAll();
      return $.ajax({
        type: "POST",
        url: url + "?_=" + new Date().getTime(),
        datatype: "json",
        traditional: true,
        data: {
          Id: taskId,
          ShopName: this.Name.val(),
          DeadlineDate: this.DeadlineDate.val(),
          Text: this.Description.val(),
          ReminderTime: this.ReminderTime.val(),
          AssignedTo: this.getSelectedUsersId(),
          Points: this.RewardPoints.val(),
          Gift: this.RewardGift.val(),
          ShoppingList: JSON.stringify(gridData)
        },
        success: (function(_this) {
          return function(result) {
            return $.getResult(result, function() {
              return _this.App.trigger('ShowTaskList');
            });
          };
        })(this)
      });
    }
  };

  exports.ShoppingTaskController = Spine.Controller.create(ops);

}).call(this);
