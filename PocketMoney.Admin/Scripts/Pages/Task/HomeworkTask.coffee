exports = @
$ = jQuery
ops =
    elements: 
        '#lessonChecked': 'lessonChecked'
        '#homeworlkLesson': 'LessonName'
        '#homeworkDescription': 'Description'
        '#js-day-selector': 'daySelector'
        '#js-select-hollidays': 'includeHolidays'
        '#homeworkAssignedToChooseBox': 'AssignedToChooseBox'
        '#homeworkStartDate': 'StartDate'
        '#homeworkEndDate': 'EndDate'
        '#homeworkReward': 'Reward'  
        '#homeworkRewardRadioPoints': 'RewardRadioPoints'   
        '#homeworkRewardRadioGift': 'RewardRadioGift'  
        '#homeworkChooseReward': 'ChooseReward'
        '#homeworkRewardLabelPoints': 'RewardLabelPoints'
        '#homeworkRewardPoints': 'RewardPoints'
        '#homeworkRewardLabelGift': 'RewardLabelGift'
        '#homeworkRewardGift': 'RewardGift'
        '#homeworkReminderTime': 'ReminderTime'
        'div.validation-summary-errors > ul': 'validationSummary'
        '#actionPostHomeworkTask': 'postTaskBtn'

    events:
        'click #homeworkRewardRadioPoints': 'selectPoints'
        'click #homeworkRewardRadioGift': 'selectGift'
        'click #actionPostHomeworkTask': 'postTask'

    proxied: ['showTask']
        
    init: ->
        @App.bind 'ShowHomeworkTask', @showTask
        @LessonName.autocomplete 
            source: @LessonName.data "url"
            minLength: 2
    
    showTask: (taskId) ->
        @AssignedToChooseBox.empty()
        $('#assignedToBox').clone(true).show().appendTo @AssignedToChooseBox
        @clearForm()
        @loadTask(taskId)

    clearForm: ->
        @validationSummary.empty()
        @LessonName.val "" 
        @StartDate.val ""
        @EndDate.val ""
        @Description.val ""
        $('input[data-user-id]', @AssignedToChooseBox).each -> $(@).prop "checked", false
        $('input[data-day-of-week]', @daySelector).each -> $(@).prop "checked", false
        @RewardRadioGift.removeAttr "checked"
        @RewardRadioPoints.attr "checked", "checked"
        @selectPoints()
        @ReminderTime.val ""
        @includeHolidays.removeAttr "checked"
        @RewardGift.val ""
        @RewardPoints.val "0"

    selectPoints: ->
        @RewardLabelGift.hide()
        @RewardGift.hide()
        @RewardLabelPoints.show()
        @RewardPoints.show()

    selectGift: ->
        @RewardLabelPoints.hide()
        @RewardPoints.hide()
        @RewardLabelGift.show()
        @RewardGift.show()

    loadTask: (taskId) ->

        if taskId
            $.get @settings.GetHomeworkTaskUrl + "?_=" + new Date().getTime(), 
                taskId: taskId 
                (result) =>
                    $.getResult result, =>
                        @LessonName.val result.Data.Lesson
                        @Description.val result.Data.Text
                        @ReminderTime.val $.getFormatTime(result.Data.ReminderTime)
                        @RewardPoints.val result.Data.Points
                        @RewardGift.val result.Data.Gift
                        @App.trigger 'FillUserList', result.Data.AssignedTo, @AssignedToChooseBox
                        @includeHolidays.prop "checked", result.Data.Form.IncludeHolidays
                        @StartDate.val $.getFormatDate(result.Data.Form.DateRangeFrom)
                        @EndDate.val $.getFormatDate(result.Data.Form.DateRangeTo)
                        if result.Data.Gift and result.Data.Gift.length > 0
                            @RewardRadioPoints.removeAttr "checked"
                            @RewardRadioGift.attr "checked", "checked"
                            @selectGift()

                        $("input[data-day-of-week=#{day}]", @daySelector).prop "checked", true for day in result.Data.Form.DaysOfWeek
        else
            startDate = new Date()
            endDate = new Date startDate
            endDate.setMonth startDate.getMonth() + 1
            @StartDate.val $.getDate(startDate)
            @EndDate.val $.getDate(endDate)
                            
    getSelectedUsersId: ->
        result = []
        result.push $(checkBox).data("user-id") for checkBox in @AssignedToChooseBox.find("input[data-user-id]:checked")
        result
    
    getSelectedDaysOfWeek: ->
        result = []
        result.push $(checkBox).data("day-of-week") for checkBox in @daySelector.find("input[data-day-of-week]:checked")
        result

    validate: ->
        @validationSummary.empty()
        if  not @daySelector.find("input[day-of-week]:checked")
            @validationSummary.append '<li>�� �� ������� �� ������ ���</li>'
        
        if  @RewardPoints.val() <= 0 and @RewardGift.val() is ""
            @validationSummary.append '<li>�� ������ ������� ������ ���� �������</li>'

    postTask:(eventObject) ->
        false if !@validate()
        url = if @postTaskBtn.data("task-id").length > 0 then @postTaskBtn.data "action-url-update" else @postTaskBtn.data "action-url-add"
        $.ajax 
            type: "POST"
            url: url + "?_=" + new Date().getTime()
            datatype: "json"
            traditional: true
            data:
                Id: @postTaskBtn.data "task-id"
                Lesson: @LessonName.val()
                Text: @Description.val()
                ReminderTime: @ReminderTime.val()
                AssignedTo: @getSelectedUsersId()
                Points: @RewardPoints.val()
                Gift: @RewardGift.val()
                Form: JSON.stringify(
                    DateRangeFrom: @StartDate.val()
                    DateRangeTo: @EndDate.val()
                    IncludeHolidays: @includeHolidays.prop "checked"
                    DaysOfWeek: @getSelectedDaysOfWeek()
                    )
            success: (result) =>
                  $.getResult result, =>
                      @App.trigger 'ShowTaskList'

exports.HomeworkTaskController = Spine.Controller.create(ops);
