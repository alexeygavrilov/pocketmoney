(function() {
  var $, exports, ops;

  exports = this;

  $ = jQuery;

  ops = {
    elements: {
      '#lessonChecked': 'lessonChecked',
      '#homeworlkLesson': 'LessonName',
      '#homeworkDescription': 'Description',
      '#js-day-selector': 'daySelector',
      '#js-select-hollidays': 'includeHolidays',
      '#homeworkAssignedToChooseBox': 'AssignedToChooseBox',
      '#homeworkStartDate': 'StartDate',
      '#homeworkEndDate': 'EndDate',
      '#homeworkReward': 'Reward',
      '#homeworkRewardRadioPoints': 'RewardRadioPoints',
      '#homeworkRewardRadioGift': 'RewardRadioGift',
      '#homeworkChooseReward': 'ChooseReward',
      '#homeworkRewardLabelPoints': 'RewardLabelPoints',
      '#homeworkRewardPoints': 'RewardPoints',
      '#homeworkRewardLabelGift': 'RewardLabelGift',
      '#homeworkRewardGift': 'RewardGift',
      '#homeworkReminderTime': 'ReminderTime',
      'div.validation-summary-errors > ul': 'validationSummary',
      '#actionPostHomeworkTask': 'postTaskBtn'
    },
    events: {
      'click #homeworkRewardRadioPoints': 'selectPoints',
      'click #homeworkRewardRadioGift': 'selectGift',
      'click #actionPostHomeworkTask': 'postTask'
    },
    proxied: ['showTask'],
    init: function() {
      this.App.bind('ShowHomeworkTask', this.showTask);
      return this.LessonName.autocomplete({
        source: this.LessonName.data("url"),
        minLength: 2
      });
    },
    showTask: function(taskId) {
      this.AssignedToChooseBox.empty();
      $('#assignedToBox').clone(true).show().appendTo(this.AssignedToChooseBox);
      this.clearForm();
      return this.loadTask(taskId);
    },
    clearForm: function() {
      this.validationSummary.empty();
      this.LessonName.val("");
      this.StartDate.val("");
      this.EndDate.val("");
      this.Description.val("");
      $('input[data-user-id]', this.AssignedToChooseBox).each(function() {
        return $(this).prop("checked", false);
      });
      $('input[data-day-of-week]', this.daySelector).each(function() {
        return $(this).prop("checked", false);
      });
      this.RewardRadioGift.removeAttr("checked");
      this.RewardRadioPoints.attr("checked", "checked");
      this.selectPoints();
      this.ReminderTime.val("");
      this.includeHolidays.removeAttr("checked");
      this.RewardGift.val("");
      return this.RewardPoints.val("0");
    },
    selectPoints: function() {
      this.RewardLabelGift.hide();
      this.RewardGift.hide();
      this.RewardLabelPoints.show();
      return this.RewardPoints.show();
    },
    selectGift: function() {
      this.RewardLabelPoints.hide();
      this.RewardPoints.hide();
      this.RewardLabelGift.show();
      return this.RewardGift.show();
    },
    loadTask: function(taskId) {
      var endDate, startDate;
      if (taskId) {
        return $.get(this.settings.GetHomeworkTaskUrl + "?_=" + new Date().getTime(), {
          taskId: taskId
        }, (function(_this) {
          return function(result) {
            return $.getResult(result, function() {
              var day, _i, _len, _ref, _results;
              _this.LessonName.val(result.Data.Lesson);
              _this.Description.val(result.Data.Text);
              _this.ReminderTime.val($.getFormatTime(result.Data.ReminderTime));
              _this.RewardPoints.val(result.Data.Points);
              _this.RewardGift.val(result.Data.Gift);
              _this.App.trigger('FillUserList', result.Data.AssignedTo, _this.AssignedToChooseBox);
              _this.includeHolidays.prop("checked", result.Data.Form.IncludeHolidays);
              _this.StartDate.val($.getFormatDate(result.Data.Form.DateRangeFrom));
              _this.EndDate.val($.getFormatDate(result.Data.Form.DateRangeTo));
              if (result.Data.Gift && result.Data.Gift.length > 0) {
                _this.RewardRadioPoints.removeAttr("checked");
                _this.RewardRadioGift.attr("checked", "checked");
                _this.selectGift();
              }
              _ref = result.Data.Form.DaysOfWeek;
              _results = [];
              for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                day = _ref[_i];
                _results.push($("input[data-day-of-week=" + day + "]", _this.daySelector).prop("checked", true));
              }
              return _results;
            });
          };
        })(this));
      } else {
        startDate = new Date();
        endDate = new Date(startDate);
        endDate.setMonth(startDate.getMonth() + 1);
        this.StartDate.val($.getDate(startDate));
        return this.EndDate.val($.getDate(endDate));
      }
    },
    getSelectedUsersId: function() {
      var checkBox, result, _i, _len, _ref;
      result = [];
      _ref = this.AssignedToChooseBox.find("input[data-user-id]:checked");
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        checkBox = _ref[_i];
        result.push($(checkBox).data("user-id"));
      }
      return result;
    },
    getSelectedDaysOfWeek: function() {
      var checkBox, result, _i, _len, _ref;
      result = [];
      _ref = this.daySelector.find("input[data-day-of-week]:checked");
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        checkBox = _ref[_i];
        result.push($(checkBox).data("day-of-week"));
      }
      return result;
    },
    validate: function() {
      this.validationSummary.empty();
      if (!this.daySelector.find("input[day-of-week]:checked")) {
        this.validationSummary.append('<li>Вы не выбрали ни одного дня</li>');
      }
      if (this.RewardPoints.val() <= 0 && this.RewardGift.val() === "") {
        return this.validationSummary.append('<li>Вы должны выбрать хотябы одну награду</li>');
      }
    },
    postTask: function(eventObject) {
      var url;
      if (!this.validate()) {
        false;
      }
      url = this.postTaskBtn.data("task-id").length > 0 ? this.postTaskBtn.data("action-url-update") : this.postTaskBtn.data("action-url-add");
      return $.ajax({
        type: "POST",
        url: url + "?_=" + new Date().getTime(),
        datatype: "json",
        traditional: true,
        data: {
          Id: this.postTaskBtn.data("task-id"),
          Lesson: this.LessonName.val(),
          Text: this.Description.val(),
          ReminderTime: this.ReminderTime.val(),
          AssignedTo: this.getSelectedUsersId(),
          Points: this.RewardPoints.val(),
          Gift: this.RewardGift.val(),
          Form: JSON.stringify({
            DateRangeFrom: this.StartDate.val(),
            DateRangeTo: this.EndDate.val(),
            IncludeHolidays: this.includeHolidays.prop("checked"),
            DaysOfWeek: this.getSelectedDaysOfWeek()
          })
        },
        success: (function(_this) {
          return function(result) {
            return $.getResult(result, function() {
              return _this.App.trigger('ShowTaskList');
            });
          };
        })(this)
      });
    }
  };

  exports.HomeworkTaskController = Spine.Controller.create(ops);

}).call(this);
