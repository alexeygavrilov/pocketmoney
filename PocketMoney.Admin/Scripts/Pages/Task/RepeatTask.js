(function() {
  var $, exports, ops;

  exports = this;

  $ = jQuery;

  ops = {
    elements: {
      '#repeatName': 'Name',
      '#repeatDescription': 'Description',
      '#repeatAssignedToChooseBox': 'AssignedToChooseBox',
      '#repeatReward': 'Reward',
      '#repeatRewardRadioPoints': 'RewardRadioPoints',
      '#repeatRewardRadioGift': 'RewardRadioGift',
      '#repeatRewardLabelPoints': 'RewardLabelPoints',
      '#repeatRewardPoints': 'RewardPoints',
      '#repeatRewardLabelGift': 'RewardLabelGift',
      '#repeatRewardGift': 'RewardGift',
      '#repeatReminderTime': 'ReminderTime',
      'div.validation-summary-errors > ul': 'validationSummary',
      '#actionPostRepeatTask': 'postTaskBtn',
      '#daySelector': 'daySelector',
      '#daySelectorRadioBox': 'daySelectorRadioBox',
      '#everyday': 'everyday',
      '#everyWeekday': 'everyWeekday',
      '#everyWeek': 'everyWeek',
      '#everyMonth': 'everyMonth',
      '#selectedInput': 'selectedInput',
      '#everyDayInput': 'everyDayInput',
      '#daysOfWeekInput': 'daysOfWeekInput',
      '#evetyMonthInput': 'everyMonthInput',
      '#dateRange': 'dateRange',
      '#startDate': 'startDate',
      '#selectEndAfter': 'selectEndAfter',
      '#EndAfter': 'EndAfter',
      '#selectEndBy': 'selectEndBy',
      '#EndBy': 'EndBy',
      '#everyWeekNum': 'everyWeekNum'
    },
    events: {
      'click #repeatRewardRadioPoints': 'selectPoints',
      'click #repeatRewardRadioGift': 'selectGift',
      'click #actionPostRepeatTask': 'postTask',
      'click #everyday': 'selectEveryday',
      'click #everyWeekday': 'selectEveryWeekday',
      'click #everyWeek': 'selectEveryWeek',
      'click #everyMonth': 'selectEveryMonth',
      'click #selectEndAfter': 'clickSelectEndAfter',
      'click #selectEndBy': 'clickSelectEndBy'
    },
    proxied: ['showTask'],
    init: function() {
      return this.App.bind('ShowRepeatTask', this.showTask);
    },
    showTask: function(taskId) {
      this.AssignedToChooseBox.empty();
      $('#assignedToBox').clone(true).show().appendTo(this.AssignedToChooseBox);
      this.clearForm();
      return this.loadTask(taskId);
    },
    clearForm: function() {
      this.validationSummary.empty();
      this.Name.val("");
      this.Description.val("");
      $('input[data-user-id]', this.AssignedToChooseBox).each(function() {
        return $(this).prop("checked", false);
      });
      this.RewardRadioGift.removeAttr("checked");
      this.RewardRadioPoints.attr("checked", "checked");
      this.selectPoints();
      this.ReminderTime.val("");
      this.startDate.val("");
      this.everyday.click();
      this.everyDayInput.find("input").val("1");
      this.everyWeekNum.val("1");
      this.everyMonthInput.children('input[type=number]:first').val("1");
      this.everyMonthInput.children('input[type=number]:last').val("1");
      this.EndAfter.val("");
      this.daysOfWeekInput.find("input[data-day-of-week]").prop("checked", false);
      this.RewardGift.val("");
      this.RewardPoints.val("0");
      return this.selectEndBy.attr("checked", "checked");
    },
    hideAll: function() {
      this.everyDayInput.hide();
      this.daysOfWeekInput.hide();
      this.everyMonthInput.hide();
      return this.daysOfWeekInput.children('input[type=checkbox]').each(function() {
        return $(this).prop("checked", false);
      });
    },
    selectEveryday: function() {
      this.hideAll();
      this.everyDayInput.show();
      return this.everyDayInput.find("input").focus();
    },
    selectEveryWeekday: function() {
      return this.hideAll();
    },
    selectEveryWeek: function() {
      this.hideAll();
      this.daysOfWeekInput.show();
      return this.everyWeekNum.focus();
    },
    selectEveryMonth: function() {
      this.hideAll();
      this.everyMonthInput.show();
      return this.everyMonthInput.children('input[type=number]:first').focus();
    },
    clickSelectEndAfter: function() {
      return this.EndAfter.focus();
    },
    clickSelectEndBy: function() {},
    getSelectedDaysOfWeek: function() {
      var checkBox, result, _i, _len, _ref;
      result = [];
      _ref = this.daysOfWeekInput.find("input[data-day-of-week]:checked");
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        checkBox = _ref[_i];
        result.push($(checkBox).data("day-of-week"));
      }
      return result;
    },
    loadTask: function(taskId) {
      var endDate, startDate;
      if (taskId) {
        return $.get(this.settings.GetRepeatTaskUrl + "?_=" + new Date().getTime(), {
          taskId: taskId
        }, (function(_this) {
          return function(result) {
            return $.getResult(result, function() {
              var day, _i, _len, _ref;
              _this.Name.val(result.Data.Name);
              _this.Description.val(result.Data.Text);
              _this.ReminderTime.val($.getFormatTime(result.Data.ReminderTime));
              _this.RewardPoints.val(result.Data.Points);
              _this.RewardGift.val(result.Data.Gift);
              if (result.Data.Gift && result.Data.Gift.length > 0) {
                _this.RewardRadioPoints.removeAttr("checked");
                _this.RewardRadioGift.attr("checked", "checked");
                _this.selectGift();
              }
              _this.App.trigger('FillUserList', result.Data.AssignedTo, _this.AssignedToChooseBox);
              _ref = result.Data.Form.DaysOfWeek;
              for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                day = _ref[_i];
                _this.daysOfWeekInput.find("input[data-day-of-week=" + day + "]").prop("checked", true);
              }
              _this.daySelectorRadioBox.find("input[data-occurrence-type=" + result.Data.Form.OccurrenceType + "]").prop("checked", true);
              _this.daySelectorRadioBox.find("input[data-occurrence-type=" + result.Data.Form.OccurrenceType + "]").click();
              _this.everyDayInput.children('input[type=number]').val(result.Data.Form.EveryDay);
              _this.everyMonthInput.children('input[type=number]:first').val(result.Data.Form.DayOfMonth);
              _this.everyMonthInput.children('input[type=number]:last').val(result.Data.Form.EveryMonth);
              _this.startDate.val($.getFormatDate(result.Data.Form.DateRangeFrom));
              _this.EndAfter.val(result.Data.Form.OccurrenceNumber);
              _this.EndBy.val($.getFormatDate(result.Data.Form.DateRangeTo));
              _this.everyWeekNum.val(result.Data.Form.EveryWeek);
              if (!result.Data.Form.OccurrenceNumber) {
                return _this.selectEndBy.click();
              } else {
                return _this.selectEndAfter.click();
              }
            });
          };
        })(this));
      } else {
        startDate = new Date();
        endDate = new Date(startDate);
        endDate.setMonth(startDate.getMonth() + 1);
        this.startDate.val($.getDate(startDate));
        return this.EndBy.val($.getDate(endDate));
      }
    },
    postTask: function(eventObject) {
      var url;
      if (!this.validate()) {
        false;
      }
      url = this.postTaskBtn.data("task-id").length > 0 ? this.postTaskBtn.data("action-url-update") : this.postTaskBtn.data("action-url-add");
      return $.ajax({
        type: "POST",
        url: url + "?_=" + new Date().getTime(),
        datatype: "json",
        traditional: true,
        data: {
          Id: this.postTaskBtn.data("task-id"),
          Name: this.Name.val(),
          Text: this.Description.val(),
          ReminderTime: this.ReminderTime.val(),
          AssignedTo: this.getSelectedUsersId(),
          Points: this.RewardPoints.val(),
          Gift: this.RewardGift.val(),
          Form: JSON.stringify({
            EveryWeek: this.everyWeekNum.val(),
            OccurrenceType: this.daySelectorRadioBox.find("input:checked:first").data("occurrence-type"),
            DaysOfWeek: this.getSelectedDaysOfWeek(),
            EveryDay: this.everyDayInput.children('input[type=number]').val(),
            DayOfMonth: this.everyMonthInput.children('input[type=number]:first').val(),
            EveryMonth: this.everyMonthInput.children('input[type=number]:last').val(),
            DateRangeFrom: this.startDate.val(),
            DateRangeTo: this.selectEndBy.attr("checked") === 'checked' ? this.EndBy.val() : null,
            OccurrenceNumber: this.selectEndAfter.attr("checked") === "checked" ? this.EndAfter.val() : null,
            DaysOfWeek: this.getSelectedDaysOfWeek()
          })
        },
        success: (function(_this) {
          return function(result) {
            return $.getResult(result, function() {
              return _this.App.trigger('ShowTaskList');
            });
          };
        })(this)
      });
    },
    selectPoints: function() {
      this.RewardLabelGift.hide();
      this.RewardGift.hide();
      this.RewardLabelPoints.show();
      return this.RewardPoints.show();
    },
    selectGift: function() {
      this.RewardLabelPoints.hide();
      this.RewardPoints.hide();
      this.RewardLabelGift.show();
      return this.RewardGift.show();
    },
    getSelectedUsersId: function() {
      var checkBox, result, _i, _len, _ref;
      result = [];
      _ref = this.AssignedToChooseBox.find("input[data-user-id]:checked");
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        checkBox = _ref[_i];
        result.push($(checkBox).data("user-id"));
      }
      return result;
    },
    validate: function() {
      this.validationSummary.empty();
      this.Name.removeClass('input-validation-error');
      if (this.Name.val() === '') {
        this.validationSummary.append('<li>Имя - это обязательное поле</li>');
        this.Name.addClass('input-validation-error');
      }
      if (this.RewardPoints.val() <= 0 && this.RewardGift.val() === "") {
        return this.validationSummary.append('<li>Вы должны выбрать хотябы одну награду</li>');
      }
    }
  };

  exports.RepeatTaskController = Spine.Controller.create(ops);

}).call(this);
