(function() {
  var $, exports, ops;

  exports = this;

  $ = jQuery;

  ops = {
    elements: {
      '#goalsRowTemplate': 'rowTamplate',
      '#tableGoals > tbody': 'table'
    },
    events: '',
    init: function() {
      return this.loadData();
    },
    loadData: function() {
      return $.get(this.settings.GetAppointsUrl, (function(_this) {
        return function(result) {
          return $.getResult(result, function() {
            var row, _i, _len, _ref, _results;
            _this.table.empty();
            _ref = result.List;
            _results = [];
            for (_i = 0, _len = _ref.length; _i < _len; _i++) {
              row = _ref[_i];
              _results.push(_this.table.append(_this.rowTemplate.tmpl(row)));
            }
            return _results;
          });
        };
      })(this));
    }
  };

  exports.AppointRewardController = Spine.Controller.create(ops);

}).call(this);
