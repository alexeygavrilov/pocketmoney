exports = @
$ = jQuery
ops =
    elements: 
        '#appointsRowTemplate': 'rowTemplate'
        '#tableGoals > tbody': 'table'
        '#RewardRadioPoints': 'RewardRadioPoints'
        '#RewardRadioGift': 'RewardRadioGift'
        '#RewardLabelPoints': 'RewardLabelPoints'
        '#RewardPoints': 'RewardPoints'
        '#RewardLabelGift': 'RewardLabelGift'
        '#RewardGift': 'RewardGift'
        '#areaList': 'areaList'
        '#setRewardList': 'setRewardList'
        '#actionPost': 'postBtn'
        '#Description': 'Description'
        '#Name': 'Name'
        

    events:
        'click .js-action-setGift': 'setGift'
        'click .js-action-remove': 'deleteReward'
        'click #RewardRadioPoints': 'selectPoints'
        'click #RewardRadioGift': 'selectGift'
        'click #actionPost': 'appointReward'
        'click .actionBack': 'hide'

    init: ->
        @loadData()

    loadData: ->
        $.get @settings.GetAttainmentsUrl + "?_=" + new Date().getTime(), (result) =>
            $.getResult result, =>
                @table.empty()
                @table.append @rowTemplate.tmpl(row) for row in result.List

    selectPoints: ->
        @RewardLabelGift.hide()
        @RewardGift.hide()
        @RewardLabelPoints.show()
        @RewardPoints.show()
        @RewardGift.val ""
        @RewardPoints.val "0"

    selectGift: ->
        @RewardLabelPoints.hide()
        @RewardPoints.hide()
        @RewardLabelGift.show()
        @RewardGift.show()
        @RewardPoints.val "0" 

    setGift:(eventObject)->
        @loadAttainment($(eventObject.target).data("attainment-id"))
        @areaList.hide 'slide', { direction: 'left' }, 400, =>
           @setRewardList.show 'slide', { direction: 'right' }, 400
        @postBtn.data "attainment-id", ($(eventObject.target).data("attainment-id"))

    hide:->
        @setRewardList.hide 'slide', { direction: 'right' }, 400, =>
            @areaList.show 'slide', { direction: 'left' }, 400 
        
    loadAttainment:(id)->
        $.get @settings.GetAttainmentUrl + "?_=" + new Date().getTime(),
            attainmentId: id
            (result)=>
                $.getResult result, =>
                    @Description.val result.Data.Text
                    @Name.text "������ ���� �� " + result.Data.UserName + ":"

    appointReward:(eventObject) ->
        $.post @settings.PostRewardUrl + "?_=" + new Date().getTime(),
            Gift: @RewardGift.val(),
            Points: @RewardPoints.val(),
            Id: $(eventObject.target).data("attainment-id")
            (result)=>
                $.getResult result, =>
                    @hide()
                    @loadData()

    deleteReward: (eventObject) -> 
        if confirm "�� ����� ������ ������� ��������?"
            $.post @settings.DeleteReward + "?_=" + new Date().getTime(),
                attainmentId: $(eventObject.target).data "attainment-id"
                (result)=>
                    $.getResult result, =>
                         @loadData()

        
exports.AppointRewardController = Spine.Controller.create(ops);
