(function() {
  var $, exports, ops;

  exports = this;

  $ = jQuery;

  ops = {
    elements: {
      '#tableGoals > tbody': 'table',
      '#goalsRowTemplate': 'rowTemplate',
      '#addGoalBtn': 'addGoalBtn',
      '#areaList': 'areaList',
      '#GoalView': 'goalView',
      '#AssignedToChooseBox': 'AssignedToChooseBox',
      '#Reward': 'Reward',
      '#RewardRadioPoints': 'RewardRadioPoints',
      '#RewardRadioGift': 'RewardRadioGift',
      '#RewardLabelPoints': 'RewardLabelPoints',
      '#RewardPoints': 'RewardPoints',
      '#RewardLabelGift': 'RewardLabelGift',
      '#RewardGift': 'RewardGift',
      '#ReminderTime': 'ReminderTime',
      '#checkboxUsersTemplate': 'checkboxUsersTemplate',
      '#Description': 'Description',
      'div.validation-summary-errors > ul': 'validationSummary',
      '#actionPost': 'postGoalBtn',
      '#template': 'template'
    },
    events: {
      'click .js-actionRemove': 'deleteGoal',
      'click .js-actionEdit': 'editGoal',
      'click #RewardRadioPoints': 'selectPoints',
      'click #RewardRadioGift': 'selectGift',
      'click #actionPost': 'postGoal',
      'click .actionBack': 'hideGoal',
      'click #addGoalBtn': 'addGoal',
      'change #template': 'useTemplate'
    },
    init: function() {
      this.loadData();
      return this.loadChooseBox();
    },
    loadData: function() {
      return $.get(this.settings.GetGoalsUrl + "?_=" + new Date().getTime(), (function(_this) {
        return function(result) {
          return $.getResult(result, function() {
            var row, _i, _len, _ref, _results;
            _this.table.empty();
            _ref = result.List;
            _results = [];
            for (_i = 0, _len = _ref.length; _i < _len; _i++) {
              row = _ref[_i];
              _results.push(_this.table.append(_this.rowTemplate.tmpl(row)));
            }
            return _results;
          });
        };
      })(this));
    },
    useTemplate: function() {
      return this.Description.val(this.template.val());
    },
    hideGoal: function() {
      return this.goalView.hide('slide', {
        direction: 'right'
      }, 400, (function(_this) {
        return function() {
          return _this.areaList.show('slide', {
            direction: 'left'
          }, 400);
        };
      })(this));
    },
    deleteGoal: function(eventObject) {
      if (confirm("вы точно хотите удалить достижение?")) {
        return $.post(this.settings.DeleteGoalUrl + "?_=" + new Date().getTime(), {
          goalId: $(eventObject.target).data("goal-id")
        }, (function(_this) {
          return function(result) {
            return $.getResult(result, function() {
              return _this.loadData();
            });
          };
        })(this));
      }
    },
    loadChooseBox: function() {
      return $.get(this.settings.GetFamilyMembersUrl + "?_=" + new Date().getTime(), (function(_this) {
        return function(result) {
          return $.getResult(result, function() {
            var leftBox, rightBox, user, _i, _len, _ref;
            _this.AssignedToChooseBox.empty();
            _ref = result.List;
            for (_i = 0, _len = _ref.length; _i < _len; _i++) {
              user = _ref[_i];
              _this.AssignedToChooseBox.append(_this.checkboxUsersTemplate.tmpl(user));
            }
            rightBox = _this.AssignedToChooseBox.find('label').filter(':odd');
            leftBox = _this.AssignedToChooseBox.find('label').filter(':even');
            rightBox.wrapAll("<div class='right-part' />");
            leftBox.wrapAll("<div class='left-part' />");
            return _this.AssignedToChooseBox.append('<div style="clear:both;"></div>');
          });
        };
      })(this));
    },
    showGoal: function() {
      this.areaList.hide('slide', {
        direction: 'left'
      }, 400, (function(_this) {
        return function() {};
      })(this));
      return this.goalView.show("slide", {
        direction: "right"
      }, 400);
    },
    editGoal: function(eventObject) {
      this.showGoal();
      return this.loadGoal($(eventObject.target).data("goal-id"));
    },
    addGoal: function() {
      this.clear();
      return this.showGoal();
    },
    clear: function() {
      this.Description.val("");
      this.RewardPoints.val("");
      this.RewardGift.val("");
      $("input[data-user-id]", this.AssignedToChooseBox).prop("checked", false);
      this.postGoalBtn.data("goal-id", "");
      this.RewardGift.val("");
      return this.RewardPoints.val("0");
    },
    selectPoints: function() {
      this.RewardLabelGift.hide();
      this.RewardGift.hide();
      this.RewardLabelPoints.show();
      return this.RewardPoints.show();
    },
    selectGift: function() {
      this.RewardLabelPoints.hide();
      this.RewardPoints.hide();
      this.RewardLabelGift.show();
      return this.RewardGift.show();
    },
    loadGoal: function(goalId) {
      $.get(this.settings.GetGoalUrl + "?_=" + new Date().getTime(), {
        goalId: goalId
      }, (function(_this) {
        return function(result) {
          return $.getResult(result, function() {
            var user, _i, _len, _ref, _results;
            _this.Description.val(result.Data.Text);
            _this.RewardPoints.val(result.Data.Points);
            _this.RewardGift.val(result.Data.Gift);
            _ref = result.Data.AssignedTo;
            _results = [];
            for (_i = 0, _len = _ref.length; _i < _len; _i++) {
              user = _ref[_i];
              _results.push($("input[data-user-id=" + user.UserId + "]", _this.AssignedToChooseBox).prop("checked", true));
            }
            return _results;
          });
        };
      })(this));
      return this.postGoalBtn.data("goal-id", goalId);
    },
    getSelectedUsersId: function() {
      var checkBox, result, _i, _len, _ref;
      result = [];
      _ref = this.AssignedToChooseBox.find("input[data-user-id]:checked");
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        checkBox = _ref[_i];
        result.push($(checkBox).data("user-id"));
      }
      return result;
    },
    validate: function() {
      this.validationSummary.empty();
      this.Description.removeClass('input-validation-error');
      if (this.Description.val() === '') {
        this.validationSummary.append('<li>Цель - это обязательное поле</li>');
        this.Name.addClass('input-validation-error');
      }
      if (this.RewardPoints.val() <= 0 && this.RewardGift.val() === "") {
        return this.validationSummary.append('<li>Вы должны выбрать хотябы одну награду</li>');
      }
    },
    postGoal: function(eventObject) {
      var goalId, url;
      if (!this.validate()) {
        false;
      }
      goalId = this.postGoalBtn.data("goal-id");
      url = goalId && goalId.length > 0 ? this.postGoalBtn.data("action-url-update") : this.postGoalBtn.data("action-url-add");
      return $.ajax({
        type: "POST",
        url: url + "?_=" + new Date().getTime(),
        datatype: "json",
        traditional: true,
        data: {
          Id: goalId,
          Text: this.Description.val(),
          AssignedTo: this.getSelectedUsersId(),
          Points: this.RewardPoints.val(),
          Gift: this.RewardGift.val()
        },
        success: (function(_this) {
          return function(result) {
            return $.getResult(result, function() {
              _this.hideGoal();
              return _this.loadData();
            });
          };
        })(this)
      });
    }
  };

  exports.GoalController = Spine.Controller.create(ops);

}).call(this);
