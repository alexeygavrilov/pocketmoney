exports = @
$ = jQuery
ops =
    elements: 
        '#tableGoals > tbody': 'table'
        '#goalsRowTemplate': 'rowTemplate'
        '#addGoalBtn': 'addGoalBtn'
        '#areaList': 'areaList' 
        '#GoalView': 'goalView'
        '#AssignedToChooseBox': 'AssignedToChooseBox'
        '#Reward': 'Reward'
        '#RewardRadioPoints': 'RewardRadioPoints'
        '#RewardRadioGift': 'RewardRadioGift'
        '#RewardLabelPoints': 'RewardLabelPoints'
        '#RewardPoints': 'RewardPoints'
        '#RewardLabelGift': 'RewardLabelGift'
        '#RewardGift': 'RewardGift'
        '#ReminderTime': 'ReminderTime'
        '#checkboxUsersTemplate': 'checkboxUsersTemplate'
        '#Description': 'Description'
        'div.validation-summary-errors > ul': 'validationSummary'
        '#actionPost': 'postGoalBtn'
        '#template': 'template'

    events:
        'click .js-actionRemove': 'deleteGoal'
        'click .js-actionEdit': 'editGoal'
        'click #RewardRadioPoints': 'selectPoints'
        'click #RewardRadioGift': 'selectGift'
        'click #actionPost': 'postGoal'
        'click .actionBack': 'hideGoal'
        'click #addGoalBtn': 'addGoal' 
        'change #template': 'useTemplate'

    init: ->
        @loadData()
        @loadChooseBox()

    loadData: ->
        $.get @settings.GetGoalsUrl + "?_=" + new Date().getTime(), (result) =>
            $.getResult result, =>
                @table.empty()
                @table.append @rowTemplate.tmpl(row) for row in result.List

    useTemplate:->
        @Description.val @template.val()

    hideGoal: ->
        @goalView.hide 'slide', { direction: 'right' }, 400, =>
            @areaList.show 'slide', { direction: 'left' }, 400 

    deleteGoal:(eventObject)->
        if confirm "�� ����� ������ ������� ����������?"
            $.post @settings.DeleteGoalUrl + "?_=" + new Date().getTime(),
                goalId: $(eventObject.target).data "goal-id"
                (result)=>
                    $.getResult result, =>
                         @loadData()

    loadChooseBox: ->
        $.get @settings.GetFamilyMembersUrl + "?_=" + new Date().getTime(), (result) =>
            $.getResult result, =>
                @AssignedToChooseBox.empty()
                @AssignedToChooseBox.append @checkboxUsersTemplate.tmpl(user) for user in result.List
                rightBox = @AssignedToChooseBox.find('label').filter(':odd')
                leftBox = @AssignedToChooseBox.find('label').filter(':even')
                rightBox.wrapAll "<div class='right-part' />"
                leftBox.wrapAll "<div class='left-part' />"
                @AssignedToChooseBox.append '<div style="clear:both;"></div>'
    
    showGoal:->
        @areaList.hide 'slide', { direction: 'left' }, 400, =>
        @goalView.show "slide", { direction: "right" }, 400

    editGoal:(eventObject) ->
        @showGoal()
        @loadGoal ($(eventObject.target).data("goal-id"))
    
    addGoal:->
        @clear()
        @showGoal()

    clear:->
        @Description.val ""
        @RewardPoints.val ""
        @RewardGift.val ""
        $("input[data-user-id]",  @AssignedToChooseBox).prop "checked", false
        @postGoalBtn.data "goal-id", ""
        @RewardGift.val ""
        @RewardPoints.val "0"

    selectPoints: ->
        @RewardLabelGift.hide()
        @RewardGift.hide()
        @RewardLabelPoints.show()
        @RewardPoints.show()

    selectGift: ->
        @RewardLabelPoints.hide()
        @RewardPoints.hide()
        @RewardLabelGift.show()
        @RewardGift.show()

    loadGoal:(goalId) ->
        $.get @settings.GetGoalUrl + "?_=" + new Date().getTime(),
            goalId: goalId
            (result) =>
                $.getResult result, =>
                    @Description.val result.Data.Text
                    @RewardPoints.val result.Data.Points
                    @RewardGift.val result.Data.Gift
                    $("input[data-user-id=#{user.UserId}]",  @AssignedToChooseBox).prop "checked", true for user in result.Data.AssignedTo
        @postGoalBtn.data "goal-id", goalId

    getSelectedUsersId: ->
        result = []
        result.push $(checkBox).data("user-id") for checkBox in @AssignedToChooseBox.find("input[data-user-id]:checked")
        result

    validate: ->
        @validationSummary.empty()
        @Description.removeClass 'input-validation-error'
        if  @Description.val() is ''
            @validationSummary.append '<li>���� - ��� ������������ ����</li>'
            @Name.addClass 'input-validation-error'
        if  @RewardPoints.val() <= 0 and @RewardGift.val() is ""
            @validationSummary.append '<li>�� ������ ������� ������ ���� �������</li>'
        
    
    postGoal:(eventObject) ->
        false if !@validate()

        goalId = @postGoalBtn.data "goal-id"

        url = if goalId and goalId.length > 0 then @postGoalBtn.data "action-url-update" else @postGoalBtn.data "action-url-add"

        $.ajax 
            type: "POST"
            url: url + "?_=" + new Date().getTime()
            datatype: "json"
            traditional: true
            data:
                Id: goalId
                Text: @Description.val()
                AssignedTo: @getSelectedUsersId()
                Points: @RewardPoints.val()
                Gift: @RewardGift.val()
            success: (result) =>
                  $.getResult result, =>
                      @hideGoal()
                      @loadData()
                
exports.GoalController = Spine.Controller.create(ops);
