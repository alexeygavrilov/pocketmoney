(function() {
  var $, exports, ops;

  exports = this;

  $ = jQuery;

  ops = {
    elements: {
      '#appointsRowTemplate': 'rowTemplate',
      '#tableGoals > tbody': 'table',
      '#RewardRadioPoints': 'RewardRadioPoints',
      '#RewardRadioGift': 'RewardRadioGift',
      '#RewardLabelPoints': 'RewardLabelPoints',
      '#RewardPoints': 'RewardPoints',
      '#RewardLabelGift': 'RewardLabelGift',
      '#RewardGift': 'RewardGift',
      '#areaList': 'areaList',
      '#setRewardList': 'setRewardList',
      '#actionPost': 'postBtn',
      '#Description': 'Description',
      '#Name': 'Name'
    },
    events: {
      'click .js-action-setGift': 'setGift',
      'click .js-action-remove': 'deleteReward',
      'click #RewardRadioPoints': 'selectPoints',
      'click #RewardRadioGift': 'selectGift',
      'click #actionPost': 'appointReward',
      'click .actionBack': 'hide'
    },
    init: function() {
      return this.loadData();
    },
    loadData: function() {
      return $.get(this.settings.GetAttainmentsUrl + "?_=" + new Date().getTime(), (function(_this) {
        return function(result) {
          return $.getResult(result, function() {
            var row, _i, _len, _ref, _results;
            _this.table.empty();
            _ref = result.List;
            _results = [];
            for (_i = 0, _len = _ref.length; _i < _len; _i++) {
              row = _ref[_i];
              _results.push(_this.table.append(_this.rowTemplate.tmpl(row)));
            }
            return _results;
          });
        };
      })(this));
    },
    selectPoints: function() {
      this.RewardLabelGift.hide();
      this.RewardGift.hide();
      this.RewardLabelPoints.show();
      this.RewardPoints.show();
      this.RewardGift.val("");
      return this.RewardPoints.val("0");
    },
    selectGift: function() {
      this.RewardLabelPoints.hide();
      this.RewardPoints.hide();
      this.RewardLabelGift.show();
      this.RewardGift.show();
      return this.RewardPoints.val("0");
    },
    setGift: function(eventObject) {
      this.loadAttainment($(eventObject.target).data("attainment-id"));
      this.areaList.hide('slide', {
        direction: 'left'
      }, 400, (function(_this) {
        return function() {
          return _this.setRewardList.show('slide', {
            direction: 'right'
          }, 400);
        };
      })(this));
      return this.postBtn.data("attainment-id", $(eventObject.target).data("attainment-id"));
    },
    hide: function() {
      return this.setRewardList.hide('slide', {
        direction: 'right'
      }, 400, (function(_this) {
        return function() {
          return _this.areaList.show('slide', {
            direction: 'left'
          }, 400);
        };
      })(this));
    },
    loadAttainment: function(id) {
      return $.get(this.settings.GetAttainmentUrl + "?_=" + new Date().getTime(), {
        attainmentId: id
      }, (function(_this) {
        return function(result) {
          return $.getResult(result, function() {
            _this.Description.val(result.Data.Text);
            return _this.Name.text("Доброе дело от " + result.Data.UserName + ":");
          });
        };
      })(this));
    },
    appointReward: function(eventObject) {
      return $.post(this.settings.PostRewardUrl + "?_=" + new Date().getTime(), {
        Gift: this.RewardGift.val(),
        Points: this.RewardPoints.val(),
        Id: $(eventObject.target).data("attainment-id")
      }, (function(_this) {
        return function(result) {
          return $.getResult(result, function() {
            _this.hide();
            return _this.loadData();
          });
        };
      })(this));
    },
    deleteReward: function(eventObject) {
      if (confirm("вы точно хотите удалить поступок?")) {
        return $.post(this.settings.DeleteReward + "?_=" + new Date().getTime(), {
          attainmentId: $(eventObject.target).data("attainment-id")
        }, (function(_this) {
          return function(result) {
            return $.getResult(result, function() {
              return _this.loadData();
            });
          };
        })(this));
      }
    }
  };

  exports.AppointRewardController = Spine.Controller.create(ops);

}).call(this);
