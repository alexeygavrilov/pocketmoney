(function() {
  var $, exports, ops;

  exports = this;

  $ = jQuery;

  ops = {
    elements: {
      '#areaList': 'areaList',
      '#areaAddUser': 'areaAddUser',
      '#areaEditUser': 'areaEditUser',
      '#tableUsers > tbody': 'table',
      '#userRowTemplate': 'rowTemplate',
      'input:radio[name=RoleType]': 'roleType',
      '#editEmail': 'textEditEmail',
      '#editUserName': 'textEditUserName',
      '#editPhoto': 'editImgPhoto',
      '#editUploadPhoto': 'editUploadPhoto',
      '#addEmail > input': 'textAddEmail',
      '#addUserName > input': 'textAddUserName',
      '#addPhoto': 'addImgPhoto',
      '#addUploadPhoto': 'addUploadPhoto',
      '#SendNotification': 'checkNotification',
      'div.validation-summary-errors > ul': 'validationSummary',
      '#actionEditUser': 'btnEditUser',
      '#editUserAdditionalName': 'editAdditionalName',
      '#editUserPhone': 'editPhone',
      '#addUserPhone': 'addPhone',
      '#showUserRole': 'showRole',
      '#showUserLoginDate': 'showLoginDate',
      '#showPoints': 'showPoints',
      '#showCompletedTasksCount': 'showCompletedTasksCount',
      '#showGrabbedTasksCount': 'showGrabbedTasksCount',
      '#showCompletedGoalsCount': 'showCompletedGoalsCount',
      '#showGoodWorksCount': 'showGoodWorksCount',
      '#areaWithdrawPoints': 'areaWithdrawPoints',
      '#userName': 'labelUserName',
      '#actionWithdraw': 'withdrawBtn',
      '#withdrawInput': 'withdrawInput',
      '#historyLog': 'historyLog',
      '#areaHistory': 'areaHistory',
      '#vkId': 'vkId',
      '#vkName': 'vkName',
      '#textVkId': 'textVkId',
      '#textVkUrl': 'textVkUrl',
      '#textVkName': 'textVkName',
      '#addUserName': 'addUserName',
      '#addEmail': 'addEmail',
      '#adult': 'adult',
      '#editVkId': 'editVkId',
      '#editTextVkId': 'editTextVkId',
      '#editTextVkUrl': 'editTextVkUrl',
      '#editUserPhoneField': 'editUserPhoneField',
      '#textEditEmailField': 'textEditEmailField',
      '#actionCreateUser': 'btnCreateUser'
    },
    events: {
      'click #actionAdd': 'showAdd',
      'click #actionCancelAdd': 'hideAdd',
      'click .js-actionEdit': 'showEdit',
      'click #actionCancelEdit': 'hideEdit',
      'click #ChildRole': 'showAddChild',
      'click .selector-by-click': 'selectInputByClick',
      'click #actionCreateUser': 'addUser',
      'click .js-actionRemove': 'removeUser',
      'click #actionEditUser': 'editUser',
      'click .js-actionExchange': 'withdraw',
      'click #actionCancelWithdraw': 'cancelWithdraw',
      'click #actionWithdraw': 'commitWithdraw',
      'click .js-actionView': 'showHistory',
      'click #actionBack': 'hideHistory',
      'click #adult': 'selectAdult',
      'click #child': 'selectChild',
      'click #SearchVkId': 'getUser',
      'change #vkName > input': 'clearVkId'
    },
    init: function() {
      return this.loadData();
    },
    loadData: function() {
      return $.get(this.settings.GetUsersUrl + "?_=" + new Date().getTime(), (function(_this) {
        return function(result) {
          return $.getResult(result, function() {
            var row, _i, _len, _ref, _results;
            _this.table.empty();
            _ref = result.List;
            _results = [];
            for (_i = 0, _len = _ref.length; _i < _len; _i++) {
              row = _ref[_i];
              _results.push(_this.table.append(_this.rowTemplate.tmpl(row)));
            }
            return _results;
          });
        };
      })(this));
    },
    clearAddForm: function() {
      this.textAddUserName.val('');
      this.textAddUserName.removeClass('input-validation-error');
      this.textAddEmail.val('');
      this.textAddEmail.removeClass('input-validation-error');
      this.addImgPhoto.attr('src', '#');
      this.addUploadPhoto.empty();
      this.validationSummary.empty();
      this.roleType.filter('[value=1]').prop('checked', true);
      this.checkNotification.prop('checked', true);
      this.adult.click();
      this.textVkId.val("");
      this.textVkUrl.val("");
      return this.textVkName.val("");
    },
    clearEditForm: function() {
      this.textEditUserName.val('');
      this.textEditUserName.removeClass('input-validation-error');
      this.textEditEmail.val('');
      this.textEditEmail.removeClass('input-validation-error');
      this.editImgPhoto.attr('src', '#');
      this.editUploadPhoto.empty();
      this.validationSummary.empty();
      this.editTextVkId.val("");
      this.editTextVkUrl.val("");
      return this.btnEditUser.removeAttr("data-connection");
    },
    showAdd: function() {
      this.clearAddForm();
      this.textAddUserName.blur((function(_this) {
        return function() {
          return _this.validateAdd();
        };
      })(this));
      this.textAddEmail.blur((function(_this) {
        return function() {
          return _this.validateAdd();
        };
      })(this));
      return this.areaList.hide('slide', {
        direction: 'left'
      }, 400, (function(_this) {
        return function() {
          return _this.areaAddUser.show('slide', {
            direction: 'right'
          }, 400);
        };
      })(this));
    },
    hideAdd: function() {
      return this.areaAddUser.hide('slide', {
        direction: 'right'
      }, 400, (function(_this) {
        return function() {
          return _this.areaList.show('slide', {
            direction: 'left'
          }, 400);
        };
      })(this));
    },
    showEdit: function(eventObject) {
      var userId;
      this.clearEditForm();
      this.editAdditionalName.blur((function(_this) {
        return function() {
          return _this.validateEdit();
        };
      })(this));
      this.textEditUserName.blur((function(_this) {
        return function() {
          return _this.validateEdit();
        };
      })(this));
      this.textEditEmail.blur((function(_this) {
        return function() {
          return _this.validateEdit();
        };
      })(this));
      this.editPhone.blur((function(_this) {
        return function() {
          return _this.validateEdit();
        };
      })(this));
      this.areaList.hide('slide', {
        direction: 'left'
      }, 400, (function(_this) {
        return function() {
          return _this.areaEditUser.show('slide', {
            direction: 'right'
          }, 400);
        };
      })(this));
      userId = $(eventObject.target).siblings("input").val();
      this.btnEditUser.data('user-id', userId);
      return $.get(this.settings.GetUserUrl + "?_=" + new Date().getTime(), {
        userId: userId
      }, (function(_this) {
        return function(result) {
          return $.getResult(result, function() {
            var connection, _i, _len, _ref, _results;
            _this.btnEditUser.removeData("connection");
            _this.textEditEmail.show();
            _this.editUserPhoneField.show();
            _this.textEditEmailField.show();
            _this.editVkId.hide();
            _this.textEditUserName.val(result.Data.UserName);
            _this.textEditEmail.val(result.Data.Email);
            _this.editAdditionalName.val(result.Data.AdditionalName);
            _this.editPhone.val(result.Data.Phone);
            _this.showRole.val(result.Data.RoleName);
            _this.showLoginDate.val(result.Data.LastLoginDate);
            _this.showPoints.val(result.Data.Points);
            _this.showCompletedTasksCount.val(result.Data.CompletedTaskCount);
            _this.showGrabbedTasksCount.val(result.Data.GrabbedTaskCount);
            _this.showCompletedGoalsCount.val(result.Data.GoalsCount);
            _this.showGoodWorksCount.val(result.Data.GoodDeedCount);
            _this.editImgPhoto.attr('src', '#');
            _ref = result.Data.Connections;
            _results = [];
            for (_i = 0, _len = _ref.length; _i < _len; _i++) {
              connection = _ref[_i];
              console.log(connection.Key);
              if (connection.Key === "1") {
                _this.editVkId.show();
                _this.textEditEmailField.hide();
                _this.editUserPhoneField.hide();
                _this.editTextVkId.val(connection.Value);
                _this.editTextVkUrl.val("https://vk.com/id" + connection.Value);
                _this.btnEditUser.data("connection", connection.Key);
                _results.push(false);
              } else {
                _results.push(void 0);
              }
            }
            return _results;
          });
        };
      })(this));
    },
    hideEdit: function() {
      return this.areaEditUser.hide('slide', {
        direction: 'right'
      }, 400, (function(_this) {
        return function() {
          return _this.areaList.show('slide', {
            direction: 'left'
          }, 400);
        };
      })(this));
    },
    validateAdd: function() {
      if (this.adult.prop("checked") === true) {
        this.validationSummary.empty();
        this.textAddUserName.removeClass('input-validation-error');
        this.textAddEmail.removeClass('input-validation-error');
        this.addPhone.removeClass('input-validation-error');
        if (this.textAddUserName.val() === '') {
          this.validationSummary.append('<li>Имя - это обязательное поле</li>');
          this.textAddUserName.addClass('input-validation-error');
          return false;
        }
        if (this.textAddEmail.val() !== '' && !/^[\w-]+(?:\.[\w-]+)*@(?:[\w-]+\.)+[a-zA-Z]{2,7}$/.test(this.textAddEmail.val())) {
          this.validationSummary.append('<li>Некорректный формат Email</li>');
          this.textAddEmail.addClass('input-validation-error');
          return false;
        }
      } else {
        this.validationSummary.empty();
        if (this.textVkName.val() === "") {
          this.validationSummary.append('<li>Пользователь не найден - это обязательное поле</li>');
          return false;
        }
      }
      return true;
    },
    validateEdit: function() {
      this.validationSummary.empty();
      this.textEditUserName.removeClass('input-validation-error');
      this.textEditEmail.removeClass('input-validation-error');
      if (this.textEditUserName.val() === '') {
        this.validationSummary.append('<li>Имя - это обязательное поле</li>');
        this.textEditUserName.addClass('input-validation-error');
      }
      if (this.textEditEmail.val() !== '' && !/^[\w-]+(?:\.[\w-]+)*@(?:[\w-]+\.)+[a-zA-Z]{2,7}$/.test(this.textEditEmail.val())) {
        this.validationSummary.append('<li>Некорректный формат Email</li>');
        this.textEditEmail.addClass('input-validation-error');
      }
      if (this.editPhone.val() !== '' && !/^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/.test(this.editPhone.val())) {
        this.validationSummary.append('<li>Некорректный формат телефона</li>');
        return this.editPhone.addClass('input-validation-error');
      }
    },
    addUser: function() {
      if (!this.validateAdd()) {
        return false;
      }
      if (this.adult.prop("checked") === true) {
        return $.post(this.settings.AddUserUrl + "?_=" + new Date().getTime(), {
          roleId: this.roleType.val(),
          username: this.textAddUserName.val(),
          email: this.textAddEmail.val(),
          send: this.checkNotification.val() === "on"
        }, (function(_this) {
          return function(result) {
            return $.getResult(result, function() {
              _this.loadData();
              return _this.hideAdd();
            });
          };
        })(this));
      } else {
        return $.post(this.settings.AddClientUrl + "?_=" + new Date().getTime(), {
          ClientType: 1,
          UserName: this.textVkName.val(),
          Identity: this.textVkId.val()
        }, (function(_this) {
          return function(result) {
            return $.getResult(result, function() {
              _this.loadData();
              return _this.hideAdd();
            });
          };
        })(this));
      }
    },
    cutUrl: function(url) {
      var splitted;
      splitted = url.split('/');
      return splitted[splitted.length - 1];
    },
    editUser: function() {
      if (!this.validateEdit()) {
        false;
      }
      if (!this.btnEditUser.data("connection")) {
        return $.post(this.settings.EditUserUrl + "?_=" + new Date().getTime(), {
          UserId: this.btnEditUser.data('user-id'),
          UserName: this.textEditUserName.val(),
          Email: this.textEditEmail.val(),
          Phone: this.editPhone.val(),
          AdditionalName: this.editAdditionalName.val()
        }, (function(_this) {
          return function(result) {
            return $.getResult(result, function() {
              _this.loadData();
              return _this.hideEdit();
            });
          };
        })(this));
      } else {
        if (this.btnEditUser.data("connection") === "1") {
          return $.post(this.settings.UpdateClientUrl + "?_=" + new Date().getTime(), {
            UserId: this.btnEditUser.data('user-id'),
            UserName: this.textEditUserName.val(),
            Identity: this.editTextVkId.val(),
            AdditionalName: this.editAdditionalName.val(),
            ClientType: "1"
          }, (function(_this) {
            return function(result) {
              return $.getResult(result, function() {
                _this.loadData();
                return _this.hideEdit();
              });
            };
          })(this));
        }
      }
    },
    removeUser: function(eventObject) {
      if (confirm("вы уверены?")) {
        return $.post(this.settings.RemoveUserUrl + "?_=" + new Date().getTime(), {
          userId: $(eventObject.target).siblings("input").val()
        }, (function(_this) {
          return function(result) {
            return $.getResult(result, function() {
              return _this.loadData();
            });
          };
        })(this));
      }
    },
    selectInputByClick: function(eventObject) {
      var control;
      control = $('input', $(eventObject.target));
      switch (control.attr('type')) {
        case 'radio':
          return control.prop('checked', true);
        case 'checkbox':
          return control.prop('checked', !control.prop('checked'));
      }
    },
    withdraw: function(eventObject) {
      this.labelUserName.text(this.labelUserName.data("text") + $(eventObject.target).data("username") + "?");
      this.withdrawBtn.data("id", $(eventObject.target).data("userid"));
      this.withdrawInput.prop("max", $(eventObject.target).data("points"));
      return this.areaList.hide('slide', {
        direction: 'left'
      }, 400, (function(_this) {
        return function() {
          return _this.areaWithdrawPoints.show('slide', {
            direction: 'right'
          }, 400, function() {
            return _this.withdrawInput.focus();
          });
        };
      })(this));
    },
    cancelWithdraw: function() {
      return this.areaWithdrawPoints.hide('slide', {
        direction: 'right'
      }, 400, (function(_this) {
        return function() {
          return _this.areaList.show('slide', {
            direction: 'left'
          }, 400);
        };
      })(this));
    },
    commitWithdraw: function(eventObject) {
      if (this.withdrawInput.val() > this.withdrawInput.prop("max")) {
        this.validationSummary.empty();
        return this.validationSummary.append('<li>Списываемые баллы не могут быть больше текущих </li>');
      } else {
        return $.post(this.settings.WithdrawPointsUrl + "?_=" + new Date().getTime(), {
          Points: this.withdrawInput.val(),
          UserId: $(eventObject.target).data("id")
        }, (function(_this) {
          return function(result) {
            return $.getResult(result, function() {
              _this.cancelWithdraw();
              return _this.loadData();
            });
          };
        })(this));
      }
    },
    showHistory: function(eventObject) {
      var userId;
      userId = $(eventObject.target).data("userid");
      return $.get(this.settings.GetHistoryUrl + "?_=" + new Date().getTime(), {
        userId: userId
      }, (function(_this) {
        return function(result) {
          return $.getResult(result, function() {
            _this.historyLog.html(result.Data);
            return _this.areaList.hide('slide', {
              direction: 'left'
            }, 400, function() {
              return _this.areaHistory.show('slide', {
                direction: 'right'
              }, 400);
            });
          });
        };
      })(this));
    },
    hideHistory: function() {
      return this.areaHistory.hide('slide', {
        direction: 'right'
      }, 400, (function(_this) {
        return function() {
          return _this.areaList.show('slide', {
            direction: 'left'
          }, 400);
        };
      })(this));
    },
    selectAdult: function() {
      this.vkId.hide();
      this.vkName.hide();
      this.addUserName.show();
      this.addEmail.show();
      return this.btnCreateUser.removeAttr('disabled');
    },
    selectChild: function() {
      this.addUserName.hide();
      this.addEmail.hide();
      this.vkId.show();
      this.vkName.show();
      return this.btnCreateUser.attr('disabled', 'disabled');
    },
    getUser: function() {
      var id;
      id = this.cutUrl(this.textVkUrl.val());
      return $.ajax({
        url: "https://api.vk.com/method/users.get?user_ids=" + id,
        type: "GET",
        dataType: "jsonp",
        crossDomain: true,
        success: (function(_this) {
          return function(result) {
            if (result && result.response && result.response.length > 0) {
              _this.textVkName.val("" + result.response[0].first_name + " " + result.response[0].last_name);
              _this.textVkId.val(result.response[0].uid);
              return _this.btnCreateUser.removeAttr('disabled');
            } else {
              _this.textVkName.val("");
              _this.textVkId.val("");
              return _this.btnCreateUser.attr('disabled', 'disabled');
            }
          };
        })(this)
      });
    },
    clearVkId: function() {
      this.textVkId.clear();
      this.textVkUrl.clear();
      return this.btnCreateUser.attr('disabled', 'disabled');
    }
  };

  exports.FamilyController = Spine.Controller.create(ops);

}).call(this);
