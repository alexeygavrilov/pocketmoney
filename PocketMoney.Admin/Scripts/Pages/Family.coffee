﻿exports = @
$ = jQuery
ops =
    elements: 
        '#areaList': 'areaList'
        '#areaAddUser': 'areaAddUser'
        '#areaEditUser': 'areaEditUser'
        '#tableUsers > tbody': 'table'
        '#userRowTemplate': 'rowTemplate'
        'input:radio[name=RoleType]': 'roleType'
        '#editEmail': 'textEditEmail'
        '#editUserName': 'textEditUserName'
        '#editPhoto': 'editImgPhoto'
        '#editUploadPhoto': 'editUploadPhoto'
        '#addEmail > input': 'textAddEmail'
        '#addUserName > input': 'textAddUserName'
        '#addPhoto': 'addImgPhoto'
        '#addUploadPhoto': 'addUploadPhoto'
        '#SendNotification': 'checkNotification'
        'div.validation-summary-errors > ul': 'validationSummary'
        '#actionEditUser': 'btnEditUser'
        '#editUserAdditionalName': 'editAdditionalName'
        '#editUserPhone': 'editPhone'
        '#addUserPhone': 'addPhone'
        '#showUserRole': 'showRole'
        '#showUserLoginDate': 'showLoginDate'
        '#showPoints': 'showPoints'
        '#showCompletedTasksCount': 'showCompletedTasksCount'
        '#showGrabbedTasksCount': 'showGrabbedTasksCount'
        '#showCompletedGoalsCount': 'showCompletedGoalsCount'
        '#showGoodWorksCount': 'showGoodWorksCount'
        '#areaWithdrawPoints': 'areaWithdrawPoints'
        '#userName': 'labelUserName'
        '#actionWithdraw': 'withdrawBtn'
        '#withdrawInput': 'withdrawInput'
        '#historyLog': 'historyLog'
        '#areaHistory': 'areaHistory'
        '#vkId': 'vkId'
        '#vkName': 'vkName'
        '#textVkId': 'textVkId'
        '#textVkUrl': 'textVkUrl'
        '#textVkName': 'textVkName'
        '#addUserName': 'addUserName'
        '#addEmail': 'addEmail'
        '#adult': 'adult'
        '#editVkId': 'editVkId'
        '#editTextVkId' :'editTextVkId'
        '#editTextVkUrl': 'editTextVkUrl'
        '#editUserPhoneField': 'editUserPhoneField'
        '#textEditEmailField': 'textEditEmailField'
        '#actionCreateUser': 'btnCreateUser'

    events:
        'click #actionAdd': 'showAdd'
        'click #actionCancelAdd': 'hideAdd'
        'click .js-actionEdit' : 'showEdit'
        'click #actionCancelEdit' : 'hideEdit'
        'click #ChildRole': 'showAddChild'
        'click .selector-by-click': 'selectInputByClick'
        'click #actionCreateUser': 'addUser'
        'click .js-actionRemove': 'removeUser'
        'click #actionEditUser': 'editUser'
        'click .js-actionExchange': 'withdraw'
        'click #actionCancelWithdraw': 'cancelWithdraw'
        'click #actionWithdraw': 'commitWithdraw'
        'click .js-actionView': 'showHistory'
        'click #actionBack': 'hideHistory'
        'click #adult': 'selectAdult'
        'click #child': 'selectChild'
        'click #SearchVkId': 'getUser'
        'change #vkName > input': 'clearVkId'

    init: ->
        @loadData()

    loadData: ->
        $.get @settings.GetUsersUrl + "?_=" + new Date().getTime(), (result) =>
            $.getResult result, =>
                @table.empty()
                @table.append @rowTemplate.tmpl(row) for row in result.List

    clearAddForm: ->
        @textAddUserName.val ''
        @textAddUserName.removeClass 'input-validation-error'
        @textAddEmail.val ''
        @textAddEmail.removeClass 'input-validation-error'
        @addImgPhoto.attr 'src', '#'
        @addUploadPhoto.empty()
        @validationSummary.empty()
        @roleType.filter('[value=1]').prop 'checked', true
        @checkNotification.prop 'checked', true
        @adult.click()
        @textVkId.val ""
        @textVkUrl.val ""
        @textVkName.val ""
        #@btnCreateUser.attr 'disabled', 'disabled'

    clearEditForm: ->
        @textEditUserName.val ''
        @textEditUserName.removeClass 'input-validation-error'
        @textEditEmail.val ''
        @textEditEmail.removeClass 'input-validation-error'
        @editImgPhoto.attr 'src', '#'
        @editUploadPhoto.empty()
        @validationSummary.empty()
        @editTextVkId.val ""
        @editTextVkUrl.val ""
        @btnEditUser.removeAttr "data-connection"

    showAdd: ->
        @clearAddForm()
        
        @textAddUserName.blur =>
            @validateAdd()

        @textAddEmail.blur =>
            @validateAdd()

        @areaList.hide 'slide', { direction: 'left' }, 400, =>
            @areaAddUser.show 'slide', { direction: 'right' }, 400

    hideAdd: ->
        @areaAddUser.hide 'slide', { direction: 'right' }, 400, =>
            @areaList.show 'slide', { direction: 'left' }, 400
    
    showEdit: (eventObject) ->
        @clearEditForm()

        @editAdditionalName.blur =>
            @validateEdit()

        @textEditUserName.blur =>
            @validateEdit()

        @textEditEmail.blur =>
            @validateEdit()

        @editPhone.blur =>
            @validateEdit()

        @areaList.hide 'slide', { direction: 'left' }, 400, =>
            @areaEditUser.show 'slide', { direction: 'right' }, 400

        userId = $(eventObject.target).siblings("input").val()

        @btnEditUser.data 'user-id', userId

        $.get @settings.GetUserUrl + "?_=" + new Date().getTime(), userId: userId, (result) =>
            $.getResult result, =>
                @btnEditUser.removeData "connection"
                @textEditEmail.show()
                @editUserPhoneField.show()
                @textEditEmailField.show()
                @editVkId.hide()
                @textEditUserName.val result.Data.UserName
                @textEditEmail.val  result.Data.Email
                @editAdditionalName.val result.Data.AdditionalName
                @editPhone.val result.Data.Phone
                @showRole.val result.Data.RoleName
                @showLoginDate.val result.Data.LastLoginDate
                @showPoints.val result.Data.Points
                @showCompletedTasksCount.val result.Data.CompletedTaskCount
                @showGrabbedTasksCount.val result.Data.GrabbedTaskCount
                @showCompletedGoalsCount.val result.Data.GoalsCount
                @showGoodWorksCount.val result.Data.GoodDeedCount
                @editImgPhoto.attr 'src', '#'
                for connection in result.Data.Connections
                    console.log connection.Key
                    if connection.Key is "1" 
                        @editVkId.show()
                        @textEditEmailField.hide()
                        @editUserPhoneField.hide()
                        @editTextVkId.val connection.Value
                        @editTextVkUrl.val "https://vk.com/id#{connection.Value}"
                        @btnEditUser.data "connection", connection.Key
                        false

    hideEdit: ->
        @areaEditUser.hide 'slide', { direction: 'right' }, 400, =>
            @areaList.show 'slide', { direction: 'left' }, 400

    validateAdd: ->
        if @adult.prop("checked") is true
            @validationSummary.empty()
            @textAddUserName.removeClass 'input-validation-error'
            @textAddEmail.removeClass 'input-validation-error'
            @addPhone.removeClass 'input-validation-error'
            if @textAddUserName.val() is ''
                @validationSummary.append '<li>Имя - это обязательное поле</li>'
                @textAddUserName.addClass 'input-validation-error'
                return false

            if @textAddEmail.val() isnt '' and not /^[\w-]+(?:\.[\w-]+)*@(?:[\w-]+\.)+[a-zA-Z]{2,7}$/.test @textAddEmail.val()
                @validationSummary.append '<li>Некорректный формат Email</li>'
                @textAddEmail.addClass 'input-validation-error'
                return false
        else
            @validationSummary.empty()
            if @textVkName.val() is ""
                 @validationSummary.append '<li>Пользователь не найден - это обязательное поле</li>'
                 return false
        return true

    validateEdit: ->
        @validationSummary.empty()
        @textEditUserName.removeClass 'input-validation-error'
        @textEditEmail.removeClass 'input-validation-error'
        
        if @textEditUserName.val() is ''
            @validationSummary.append '<li>Имя - это обязательное поле</li>'
            @textEditUserName.addClass 'input-validation-error'

        if @textEditEmail.val() isnt '' and not /^[\w-]+(?:\.[\w-]+)*@(?:[\w-]+\.)+[a-zA-Z]{2,7}$/.test @textEditEmail.val()
            @validationSummary.append '<li>Некорректный формат Email</li>'
            @textEditEmail.addClass 'input-validation-error'

        if @editPhone.val() isnt '' and not /^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/.test @editPhone.val()
            @validationSummary.append '<li>Некорректный формат телефона</li>'
            @editPhone.addClass 'input-validation-error'

    addUser: ->
        return false if !@validateAdd()

        if @adult.prop("checked") is true
            $.post @settings.AddUserUrl + "?_=" + new Date().getTime(),
                roleId: @roleType.val()
                username: @textAddUserName.val()
                email: @textAddEmail.val()
                send: @checkNotification.val() == "on",
                (result) =>
                    $.getResult result, =>
                        @loadData()
                        @hideAdd()
        else
            $.post @settings.AddClientUrl + "?_=" + new Date().getTime(),
                ClientType: 1
                UserName: @textVkName.val()
                Identity: @textVkId.val()
                (result) =>
                    $.getResult result, =>
                        @loadData()
                        @hideAdd()

    cutUrl:(url)->
        splitted = url.split('/');
        splitted[splitted.length-1]

    editUser: ->
        false if !@validateEdit()
        if not @btnEditUser.data "connection"
            $.post @settings.EditUserUrl + "?_=" + new Date().getTime(),
                UserId: @btnEditUser.data 'user-id'
                UserName: @textEditUserName.val()
                Email: @textEditEmail.val()
                Phone: @editPhone.val()
                AdditionalName: @editAdditionalName.val(),
                (result) =>
                      $.getResult result, =>
                          @loadData()
                          @hideEdit()
        else
            if  @btnEditUser.data("connection") is "1"
                $.post @settings.UpdateClientUrl + "?_=" + new Date().getTime(),
                    UserId: @btnEditUser.data 'user-id'
                    UserName: @textEditUserName.val()
                    Identity: @editTextVkId.val()
                    AdditionalName: @editAdditionalName.val()
                    ClientType: "1",
                     (result) =>
                      $.getResult result, =>
                          @loadData()
                          @hideEdit()

    removeUser: (eventObject)->
        if confirm "вы уверены?"
             $.post @settings.RemoveUserUrl + "?_=" + new Date().getTime(), userId: $(eventObject.target).siblings("input").val(), (result) =>
                    $.getResult result, =>
                        @loadData()

    selectInputByClick: (eventObject)->
        control = $('input', $(eventObject.target))
        switch control.attr('type') 
            when 'radio' then control.prop 'checked', true
            when 'checkbox' then control.prop 'checked', !control.prop('checked')

    withdraw:(eventObject)->
        @labelUserName.text @labelUserName.data("text") + $(eventObject.target).data("username") + "?"
        @withdrawBtn.data "id", $(eventObject.target).data("userid")
        @withdrawInput.prop "max", $(eventObject.target).data("points")
        @areaList.hide 'slide', { direction: 'left' }, 400, =>
            @areaWithdrawPoints.show 'slide', { direction: 'right' }, 400, =>
                @withdrawInput.focus()
        
    cancelWithdraw:->
        @areaWithdrawPoints.hide 'slide', { direction: 'right' }, 400, =>
            @areaList.show 'slide', { direction: 'left' }, 400

    commitWithdraw:(eventObject)->
        if  @withdrawInput.val() > @withdrawInput.prop("max")
            @validationSummary.empty()
            @validationSummary.append '<li>Списываемые баллы не могут быть больше текущих </li>'
        else
            $.post @settings.WithdrawPointsUrl + "?_=" + new Date().getTime(),
                Points: @withdrawInput.val()
                UserId: $(eventObject.target).data("id"),
                (result) =>
                    $.getResult result, =>
                         @cancelWithdraw()
                         @loadData()   
           
    showHistory:(eventObject)->
        userId = $(eventObject.target).data "userid"
        $.get @settings.GetHistoryUrl + "?_=" + new Date().getTime(),
            userId: userId,
            (result) =>
                $.getResult result, =>
                    @historyLog.html result.Data
                    @areaList.hide 'slide', { direction: 'left' }, 400, =>
                        @areaHistory.show 'slide', { direction: 'right' }, 400

    hideHistory:->
        @areaHistory.hide 'slide', { direction: 'right' }, 400, =>
            @areaList.show 'slide', { direction: 'left' }, 400

    selectAdult:->
        @vkId.hide()
        @vkName.hide()
        @addUserName.show()
        @addEmail.show()
        @btnCreateUser.removeAttr 'disabled'

    selectChild:->
        @addUserName.hide()
        @addEmail.hide()
        @vkId.show()
        @vkName.show()
        @btnCreateUser.attr 'disabled', 'disabled'

    getUser:->
        id = @cutUrl(@textVkUrl.val())
        $.ajax 
            url: "https://api.vk.com/method/users.get?user_ids=#{id}"
            type: "GET"
            dataType: "jsonp"
            crossDomain: true
            success: (result) =>
                if result and result.response and result.response.length > 0
                    @textVkName.val "#{result.response[0].first_name} #{result.response[0].last_name}"
                    @textVkId.val result.response[0].uid
                    @btnCreateUser.removeAttr 'disabled'
                else
                    @textVkName.val ""
                    @textVkId.val ""
                    @btnCreateUser.attr 'disabled', 'disabled'
    clearVkId:->
        @textVkId.clear()
        @textVkUrl.clear()
        @btnCreateUser.attr 'disabled', 'disabled'

exports.FamilyController = Spine.Controller.create(ops);
