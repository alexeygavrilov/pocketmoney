﻿$.showErrorMessage = (message) ->
    box = $ '#error'
    box.text "Ошибка: #{message}"
    box.fadeIn 700
    setTimeout ( -> 
        box.fadeOut 700
        ), 10000

$.getResult = (result, success) ->
    if result.Success is false && result.Message
        $.showErrorMessage result.Message
    else
       success result 

$.getDate  = (date) ->
        day = ("0" + date.getDate()).slice -2 
        month = ("0" + (date.getMonth() + 1)).slice -2 
        
        (day) + "." + (month) + "." + date.getFullYear()

$.getFormatDate  = (Data) ->
    if Data
        $.getDate new Date parseInt(Data.substr(6))
        

$.getFormatTime  = (Data) ->
    if Data
        hours = ("0" + Data.Hours).slice -2
        minutes = ("0" + Data.Minutes).slice -2
        hours + ":" + minutes
