﻿using System.Web;
using System.Web.Optimization;

namespace PocketMoney.Admin
{
    public class BundleConfig
    {
        // Дополнительные сведения о Bundling см. по адресу http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            //BundleTable.EnableOptimizations = true;
            //bundles.UseCdn = true;
            
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
                        "~/Scripts/jquery-ui-{version}.js",
                        "~/Scripts/datepicker-ru.js",
                        "~/Scripts/jquery.blockUI.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.unobtrusive*",
                        "~/Scripts/jquery.validate*"));

            // Используйте версию Modernizr для разработчиков, чтобы учиться работать. Когда вы будете готовы перейти к работе,
            // используйте средство построения на сайте http://modernizr.com, чтобы выбрать только нужные тесты.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new StyleBundle("~/Content/css").Include("~/Content/site.css"));

            bundles.Add(new StyleBundle("~/Content/table").Include("~/Content/grid.css"));

            bundles.Add(new StyleBundle("~/Content/themes/base/css").Include(
                        "~/Content/themes/base/jquery.ui.core.css",
                        "~/Content/themes/base/jquery.ui.resizable.css",
                        "~/Content/themes/base/jquery.ui.selectable.css",
                        "~/Content/themes/base/jquery.ui.accordion.css",
                        "~/Content/themes/base/jquery.ui.autocomplete.css",
                        "~/Content/themes/base/jquery.ui.button.css",
                        "~/Content/themes/base/jquery.ui.dialog.css",
                        "~/Content/themes/base/jquery.ui.slider.css",
                        "~/Content/themes/base/jquery.ui.tabs.css",
                        "~/Content/themes/base/jquery.ui.datepicker.css",
                        "~/Content/themes/base/jquery.ui.progressbar.css",
                        "~/Content/themes/base/jquery.ui.theme.css"));

            bundles.Add(new ScriptBundle("~/bundles/application").Include(
                    "~/Scripts/spine.js",
                    "~/Scripts/jquery.tmpl.js",
                    "~/Scripts/ErrorMessage.js",
                    "~/Scripts/ErrorHandler.js"));

            bundles.Add(new ScriptBundle("~/bundles/appointReward").Include(
                   "~/Scripts/Pages/appointReward.js"));
            
            bundles.Add(new ScriptBundle("~/bundles/family").Include(
                    "~/Scripts/Pages/Family.js"));

            bundles.Add(new ScriptBundle("~/bundles/grid").Include(
                    "~/Scripts/grid.js"));

            bundles.Add(new ScriptBundle("~/bundles/task").Include(
                    "~/Scripts/Pages/Task/Task.js",
                    "~/Scripts/Pages/Task/OneTimeTask.js",
                    "~/Scripts/Pages/Task/CleanTask.js",
                    "~/Scripts/Pages/Task/HomeworkTask.js",
                    "~/Scripts/Pages/Task/RepeatTask.js",
                    "~/Scripts/Pages/Task/ShoppingTask.js"));

            bundles.Add(new ScriptBundle("~/bundles/goal").Include(
                    "~/Scripts/Pages/Goal.js"));
                    
        }
    }
}