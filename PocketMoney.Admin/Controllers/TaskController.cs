﻿using Newtonsoft.Json;
using PocketMoney.Data;
using PocketMoney.Data.Security;
using PocketMoney.FileSystem;
using PocketMoney.Model.External;
using PocketMoney.Model.External.Requests;
using PocketMoney.Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Web.Mvc;


namespace PocketMoney.Admin.Controllers
{
    [Authorize]
    public class TaskController : BaseController
    {
        private readonly ITaskService _taskService;
        private readonly IFamilyService _familyService;
        private readonly ISettingService _settingService;
        private IConnector _connector;
        private readonly JsonSerializerSettings jsonSettings;

        public TaskController(
            IFamilyService familyService,
            ITaskService taskService,
            IConnector connector,
            ICurrentUserProvider currentUserProvider,
            IFileService fileService,
            IAuthorization authorization,
            ISettingService settingService)
            : base(authorization, fileService, currentUserProvider)
        {
            _familyService = familyService;
            _taskService = taskService;
            _connector = connector;
            _settingService = settingService;

            jsonSettings = new JsonSerializerSettings { Culture = CultureInfo.CurrentCulture, DateParseHandling = DateParseHandling.DateTime };
        }

        [HttpGet]
        public ActionResult Index()
        {
            return View(_currentUserProvider.GetCurrentUser());
        }

        [HttpGet]
        public JsonResult GetTasks()
        {
            return Json(_taskService.AllTasks(PocketMoney.Data.Request.Empty), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetOneTimeTask(Guid taskId)
        {
            return Json(_taskService.GetOneTimeTask(new GuidRequest(taskId)), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult AddOneTimeTask(AddOneTimeTaskRequest model)
        {
            return Json(_taskService.AddOneTimeTask(model));
        }

        [HttpPost]
        public JsonResult UpdateOneTimeTask(UpdateOneTimeTaskRequest model)
        {
            return Json(_taskService.UpdateOneTimeTask(model));
        }

        [HttpGet]
        public JsonResult GetHomeworkTask(Guid taskId)
        {
            return Json(_taskService.GetHomeworkTask(new GuidRequest(taskId)), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult AddHomeworkTask(AddHomeworkTaskRequest model)
        {
            try
            {
                model.Form = JsonConvert.DeserializeObject<HomeworkForm>(this.Request.Params["form"], jsonSettings);

                return Json(_taskService.AddHomeworkTask(model));
            }
            catch (Exception ex)
            {
                return Json(Result.Unsuccessfully(ex.ToString()));
            }
        }

        [HttpPost]
        public JsonResult UpdateHomeworkTask(UpdateHomeworkTaskRequest model)
        {
            try
            {
                model.Form = JsonConvert.DeserializeObject<HomeworkForm>(this.Request.Params["form"], jsonSettings);
                return Json(_taskService.UpdateHomeworkTask(model));
            }
            catch (Exception ex)
            {
                return Json(Result.Unsuccessfully(ex.ToString()));
            }
        }

        [HttpGet]
        public JsonResult GetCleanTask(Guid taskId)
        {
            return Json(_taskService.GetCleanTask(new GuidRequest(taskId)), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult AddCleanTask(AddCleanTaskRequest model)
        {
            try
            {
                return Json(_taskService.AddCleanTask(model));
            }
            catch (Exception ex)
            {
                return Json(Result.Unsuccessfully(ex.ToString()));
            }
        }

        [HttpPost]
        public JsonResult UpdateCleanTask(UpdateCleanTaskRequest model)
        {
            try
            {
                return Json(_taskService.UpdateCleanTask(model));
            }
            catch (Exception ex)
            {
                return Json(Result.Unsuccessfully(ex.ToString()));
            }

        }

        [HttpGet]
        public JsonResult GetShoppingTask(Guid taskId)
        {
            try
            {
                var result = _taskService.GetShoppingTask(new GuidRequest(taskId));
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(Result.Unsuccessfully(ex.ToString()), JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public JsonResult AddShoppingTask(AddShoppingTaskRequest model)
        {
            try
            {
                model.ShoppingList = JsonConvert.DeserializeObject<List<ShopItem>>(this.Request.Params["ShoppingList"], jsonSettings);

                return Json(_taskService.AddShoppingTask(model));
            }
            catch (Exception ex)
            {
                return Json(Result.Unsuccessfully(ex.ToString()));
            }
        }

        [HttpPost]
        public JsonResult UpdateShoppingTask(UpdateShoppingTaskRequest model)
        {
            try
            {
                model.ShoppingList = JsonConvert.DeserializeObject<List<ShopItem>>(this.Request.Params["ShoppingList"], jsonSettings);

                return Json(_taskService.UpdateShoppingTask(model));
            }
            catch (Exception ex)
            {
                return Json(Result.Unsuccessfully(ex.ToString()));
            }
        }

        [HttpGet]
        public JsonResult GetRepeatTask(Guid taskId)
        {
            try
            {
                var result = _taskService.GetRepeatTask(new GuidRequest(taskId));
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(Result.Unsuccessfully(ex.ToString()), JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public JsonResult AddRepeatTask(AddRepeatTaskRequest model)
        {
            try
            {
                model.Form = JsonConvert.DeserializeObject<RepeatForm>(this.Request.Params["Form"], jsonSettings);

                return Json(_taskService.AddRepeatTask(model));
            }
            catch (Exception ex)
            {
                return Json(Result.Unsuccessfully(ex.ToString()));
            }
        }

        [HttpPost]
        public JsonResult UpdateRepeatTask(UpdateRepeatTaskRequest model)
        {
            try
            {
                model.Form = JsonConvert.DeserializeObject<RepeatForm>(this.Request.Params["form"], jsonSettings);
                return Json(_taskService.UpdateRepeatTask(model));
            }
            catch (Exception ex)
            {
                return Json(Result.Unsuccessfully(ex.ToString()));
            }
        }

        [HttpGet]
        public JsonResult GetFamilyMembers()
        {
            try
            {
                var familyMembers = _familyService.GetUsers(PocketMoney.Data.Request.Empty);
                return Json(familyMembers, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(Result.Unsuccessfully(ex.ToString()), JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult DeleteTask(Guid taskId)
        {
            return Json(_taskService.DeleteTask(new GuidRequest(taskId)));
        }

        [HttpGet]
        public JsonResult GetLessons()
        {
            var lessons = _settingService.GetLessons(PocketMoney.Data.Request.Empty);
            if (lessons.Success)
            {
                return Json(lessons.List, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(lessons.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public JsonResult GetRooms()
        {
            var lessons = _settingService.GetRooms(PocketMoney.Data.Request.Empty);
            if (lessons.Success)
            {
                return Json(lessons.List, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(lessons.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public JsonResult GetTaskStatus(Guid taskId)
        {
            var result = _taskService.GetTaskStatus(new GuidRequest(taskId));

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult CancelTaskAction(Guid taskActionId)
        {
            return Json(_taskService.CancelTaskAction(new GuidRequest(taskActionId)));
        }
    }
}
