﻿using PocketMoney.Data;
using PocketMoney.Data.Security;
using PocketMoney.FileSystem;
using PocketMoney.Model.External.Requests;
using PocketMoney.Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PocketMoney.Admin.Controllers
{
    [Authorize]
    public class GoalController : BaseController
    {

        private readonly ITaskService _taskService;
        private readonly IGoalService _goalService;
        private readonly ISettingService _settingService;
        private readonly IFamilyService _familyService;
        private IConnector _connector;

        public GoalController(
            IFamilyService familyService ,
            ITaskService taskService,
            IConnector connector ,
            IGoalService goalService,
            ICurrentUserProvider currentUserProvider ,
            IFileService fileService ,
            IAuthorization authorization ,
            ISettingService settingService)
            : base(authorization , fileService , currentUserProvider)
        {
            _familyService = familyService;
            _goalService = goalService;
            _connector = connector;
            _settingService = settingService;
            _taskService = taskService;
        }

        [HttpGet]
        public ActionResult Index()
        {
            return View(_currentUserProvider.GetCurrentUser());
        }

        [HttpGet]
        public JsonResult GetGoals()
        {
            return Json(_goalService.AllGoals(PocketMoney.Data.Request.Empty) , JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult DeleteGoal(Guid goalId)
        {
            return Json(_taskService.DeleteTask(new GuidRequest(goalId)));
        }

        [HttpGet]
        public JsonResult GetGoal(Guid goalId)
        {
            return Json(_goalService.GetGoal(new GuidRequest(goalId)) , JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetFamilyMembers()
        {
            var familyMembers = _familyService.GetUsers(PocketMoney.Data.Request.Empty);
            return Json(familyMembers , JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult UpdateGoal(UpdateGoalRequest model)
        {
            return Json(_goalService.UpdateGoal(model));
        }

        [HttpPost]
        public JsonResult AddGoal(AddGoalRequest model)
        {
            return Json(_goalService.AddGoal(model));
        }
    }
}
