﻿using System.Web.Mvc;
using PocketMoney.Data;
using PocketMoney.Model.External.Requests;
using PocketMoney.Service.Interfaces;
using System;
using PocketMoney.Model;
using System.Net.Http;
using System.Net;
using System.IO;
using System.Text;
using Newtonsoft.Json.Linq;

namespace PocketMoney.Admin.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        #region Members & Ctors

        private IFamilyService _familyService;
        private ICurrentUserProvider _currentUserProvider;

        public AccountController(
            IFamilyService familyService,
            ICurrentUserProvider currentUserProvider)
        {
            _familyService = familyService;
            _currentUserProvider = currentUserProvider;
        }

        #endregion

        #region Membership
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginRequest model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                var result = _familyService.Login(model);
                if (result.Success)
                {
                    _currentUserProvider.AddCurrentUser(result.Data, model.RememberMe);

                    return RedirectToLocal(returnUrl);
                }
                else
                {
                    ModelState.AddModelError("", result.Message);
                }
            }
            else
            {
                ModelState.AddModelError("", "Имя пользователя или пароль указаны неверно.");
            }
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            _currentUserProvider.RemoveCurrentUser();

            return RedirectToAction("Index", "Home");
        }

        [AllowAnonymous]
        public ActionResult Register()
        {
            var model = new RegisterUserRequest();

            var strType = Request.QueryString["client_type"];
            var identity = Request.QueryString["identity"];

            eClientType clientType = eClientType.VK;
            if (!string.IsNullOrWhiteSpace(strType) && !string.IsNullOrWhiteSpace(identity) && Enum.TryParse<eClientType>(strType, true, out clientType))
            {
                if (clientType == eClientType.VK)
                {
                    var jsonUserInfo = GetVKInfo(identity);

                    if (!string.IsNullOrEmpty(jsonUserInfo))
                    {
                        var userInfo = JObject.Parse(jsonUserInfo);
                        var response = userInfo.SelectToken("response").ToObject<JArray>();
                        if (response != null && response.Count > 0)
                        {
                            var uid = response[0]["uid"];
                            if (uid != null)
                            {
                                model.Identity = uid.Value<string>();
                            }
                            var first_name = response[0]["first_name"];
                            var last_name = response[0]["last_name"];
                            if (first_name != null && last_name != null)
                            {
                                model.ClientFullName = first_name.Value<string>() + " " + last_name.Value<string>();
                            }
                            model.ClientType = clientType;
                            model.RegisterClient = !string.IsNullOrEmpty(model.Identity);
                        }
                    }
                }
            }

            return View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Register(RegisterUserRequest model)
        {
            if (ModelState.IsValid)
            {
                if (model.RegisterClient && string.IsNullOrEmpty(model.ClientFullName))
                {
                    ModelState.AddModelError("ClientFullName", "Имя отдельного члена семьи - это обязательное поле");
                }
                else
                {
                    var result = _familyService.RegisterUser(model);
                    if (result.Success)
                    {
                        _currentUserProvider.AddCurrentUser(result.Data, false);

                        return RedirectToAction("Index", "Family");
                        //return RedirectToAction("Confirm", "Account");
                    }
                    else
                    {
                        ModelState.AddModelError("", result.Message);
                    }
                }
            }

            return View(model);
        }

        [AllowAnonymous]
        public ActionResult Confirm(string code)
        {
            if (!string.IsNullOrEmpty(code))
            {
                var result = _familyService.ConfirmUser(new ConfirmUserRequest { ConfirmCode = code });
                if (result.Success)
                {
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    ModelState.AddModelError("", result.Message);
                }
            }
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Confirm(ConfirmUserRequest model)
        {

            if (ModelState.IsValid)
            {
                var result = _familyService.ConfirmUser(model);
                if (result.Success)
                {
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    ModelState.AddModelError("", result.Message);
                }
            }

            return View(model);
        }

        #endregion

        #region Private Methods

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        private string GetVKInfo(string identity)
        {
            var content = string.Empty;
            if (!string.IsNullOrEmpty(identity))
            {
                var request = WebRequest.Create("https://api.vk.com/method/users.get?user_ids=" + identity);

                var response = (HttpWebResponse)request.GetResponse();

                if (response.StatusCode == HttpStatusCode.OK)
                {

                    using (var stream = response.GetResponseStream())
                    {
                        using (var reader = new StreamReader(stream, Encoding.UTF8))
                        {
                            content = reader.ReadToEnd();
                        }
                    }
                }
            }
            return content;

        }
        #endregion
    }
}
