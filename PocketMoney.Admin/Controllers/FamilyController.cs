﻿using System;
using System.Web.Mvc;
using PocketMoney.Data;
using PocketMoney.Data.Security;
using PocketMoney.FileSystem;
using PocketMoney.Model;
using PocketMoney.Model.External.Requests;
using PocketMoney.Model.Network;
using PocketMoney.Service.Interfaces;

namespace PocketMoney.Admin.Controllers
{
    [Authorize]
    public class FamilyController : BaseController
    {
        #region Members & Ctors

        private IFamilyService _familyService;
        private IConnector _connector;

        public FamilyController(
            IFamilyService familyService,
            IConnector connector,
            ICurrentUserProvider currentUserProvider,
            IFileService fileService,
            IAuthorization authorization)
            : base(authorization, fileService, currentUserProvider)
        {
            _familyService = familyService;
            _connector = connector;
        }

        #endregion

        #region Methods

        [HttpGet]
        public ActionResult Index()
        {
            return View(_currentUserProvider.GetCurrentUser());
        }

        [HttpGet]
        public JsonResult GetUsers()
        {
            return Json(
                _familyService.GetUsers(PocketMoney.Data.Request.Empty),
                JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetUser(Guid userId)
        {
            return Json(
                _familyService.GetUser(new GuidRequest(userId)),
                JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetUserVK(string id)
        {
            return Json(
                _connector.GetAccount(new StringNetworkRequest { Data = id, Type = Model.eNetworkType.VK }),
                JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult AddUser(byte roleId, string username, string email, bool send, Guid? fileId)
        {
            var result = _familyService.AddUser(new AddUserRequest
            {
                UserName = username,
                Password = "12345",
                ConfirmPassword = "12345",
                SendNotification = false,
                Email = email,
                Phone = null,
                RoleId = roleId
            });
            return Json(result);
        }

        [HttpPost]
        public JsonResult DeleteUser(Guid userId)
        {
            var result = _familyService.Deactive(new GuidRequest(userId));
            return Json(result);
        }

        [HttpPost]
        public JsonResult EditUser(UpdateUserRequest model)
        {
            return Json(_familyService.UpdateUser(model));
        }

        [HttpPost]
        public JsonResult WithdrawPoints(WithdrawRequest model)
        {
            return Json(_familyService.Withdraw(model));
        }

        [HttpGet]
        public JsonResult GetHistory(Guid userId)
        {
            return Json(_familyService.GetUserHistory(new GuidRequest(userId)),
                JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult AddClient(AddClientRequest model)
        {
            return Json(_familyService.AddClient(model));
        }

        [HttpPost]
        public JsonResult UpdateClient(UpdateClientRequest model)
        {
            return Json(_familyService.UpdateClient(model));
        }
        #endregion

    }
}
