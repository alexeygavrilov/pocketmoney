﻿using PocketMoney.Data;
using PocketMoney.Data.Security;
using PocketMoney.FileSystem;
using PocketMoney.Model.External.Requests;
using PocketMoney.Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PocketMoney.Admin.Controllers
{
    [Authorize]
    public class AppointRewardController : BaseController
    {
        protected IGoalService _goalService = null;

        public AppointRewardController(
            IGoalService goalService,
            ICurrentUserProvider currentUserProvider,
            IFileService fileService,
            IAuthorization authorization,
            ISettingService settingService)
            : base(authorization, fileService, currentUserProvider)
        {
            _goalService = goalService;
        }

        [HttpGet]
        public ActionResult Index()
        {
            return View(_currentUserProvider.GetCurrentUser());
        }

        [HttpGet]
        public JsonResult GetAttainments()
        {
            return Json(_goalService.AllAttainments(PocketMoney.Data.Request.Empty), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetAttainment(Guid attainmentId)
        {
            return Json(_goalService.GetAttainment(new GuidRequest(attainmentId)), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult PostReward(AppointRewardRequest model)
        {
            return Json(_goalService.AppointReward(model));
        }

        [HttpPost]
        public JsonResult DeleteReward(Guid attainmentId)
        {
            return Json(_goalService.DeleteReward(new GuidRequest(attainmentId)));
        }
    }
}
