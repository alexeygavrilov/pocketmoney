﻿using PocketMoney.Data;
using PocketMoney.Data.Wrappers;
using PocketMoney.Model.External.Requests;
using PocketMoney.Model.External.Results;
using PocketMoney.Service.Behaviors;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using Clients = PocketMoney.Model.External.Results.Clients;

namespace PocketMoney.Service.Interfaces
{
    [ServiceContract(Namespace = "http://api.pocketmoney.ru/WCF", SessionMode = SessionMode.Allowed)]
    [ErrorPolicyBehavior]
    [ServiceKnownType(typeof(WrapperUser))]
    [ServiceKnownType(typeof(WrapperFamily))]
    [ServiceKnownType(typeof(WrapperFile))]
    [ServiceKnownType(typeof(Role))]
    public interface IClientService
    {
        [Process, OperationContract, WebGet]
        StringResult Authorizate(AuthorizateRequest model);

        [Process, OperationContract, WebGet]
        UserResult GetCurrentUser(Request model);

        [Process, OperationContract, WebGet]
        Clients.TaskListResult GetTaskList(Request model);

        [Process, OperationContract, WebGet]
        Clients.TaskListResult GetFloatingTaskList(Request model);

        [Process, OperationContract, WebInvoke(Method = "GET")]
        Clients.GoalListResult GetGoalList(Request model);

        [Process, OperationContract, WebInvoke(Method = "GET")]
        Clients.AttainmentListResult GetGoodDeedList(Request model);

        [Process, OperationContract, WebGet(RequestFormat = WebMessageFormat.Json)]
        ShopItemListResult GetShoppingList(GuidRequest taskId);

        [Process, OperationContract, WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json)]
        Result DoneTask(DoneTaskRequest model);

        [Process, OperationContract, WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json)]
        Result GrabbTask(ProcessRequest model);

        [Process, OperationContract(), WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json)]
        Result AchieveGoal(ProcessRequest model);

        [Process, OperationContract, WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json)]
        Result AddGoodDeed(AddAttainmentRequest model);

        [Process, OperationContract, WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json)]
        Result CheckShopItem(CheckShopItemRequest model);


    }
}
